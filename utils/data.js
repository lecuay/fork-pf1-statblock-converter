import fs from "fs";

/**
 * Reads a file (typically plain text) and returns its content.
 *
 * WARNING: Due to the dependency of 'fs' this is meant to be used on a node
 * environment only (in this case, tests).
 *
 * @param {string} file - The path to the file where statblock is.
 *
 * @example
 * // Reads Goblin from statblocks folder
 * const input = loadStatBlock("tests/statblocks/Goblin.txt")
 *
 * @returns {string} The content of the statblock file.
 */
export const loadStatBlock = (file) => {
  const bufferFile = fs.readFileSync(file);
  return bufferFile.toString();
};
