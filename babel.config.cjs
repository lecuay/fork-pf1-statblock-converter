/**
 * Due to some features missing from ESM Jest support we create a babel file
 * in order to transform ESM into CJS.
 * Refer to https://github.com/jestjs/jest/issues/9430
 */

/**
 *
 * @param {import("@babel/core").ConfigAPI} api
 * @returns {import("@babel/core").TransformOptions}
 */
module.exports = (api) => {
  const isTest = api.env("test");
  if (isTest) {
    return {
      presets: [["@babel/preset-env", { targets: { node: "current" } }]],
    };
  }
  return {};
};
