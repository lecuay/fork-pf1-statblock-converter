import { jest } from "@jest/globals";

global.console = {
  ...console,
  // Suppress terminal output
  log: jest.fn(),
  debug: jest.fn(),
  info: jest.fn(),
};
