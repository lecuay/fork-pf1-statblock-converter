import { NotesParser } from "../../../scripts/Parsers/Universal/notes-parser.js";
import * as sbcDataModule from "../../../scripts/sbcData.js";
import * as sbcParserModule from "../../../scripts/sbcParser.js";
import { sbcUtils } from "../../../scripts/sbcUtils.js";

describe("NotesParser Suite", () => {
  test("empty constructor fails", () => {
    // Due to the nature of the testing framework constructors need to be wrapped
    // withing anonymous functions
    expect(() => new NotesParser()).toThrow("The 'targetFields' argument is mandatory.");
  });
});

describe("NotesParser.parse Suite", () => {
  const parseValueToDocPathSpy = jest.spyOn(sbcParserModule, "parseValueToDocPath");
  const sbcErrorSpy = jest.spyOn(sbcDataModule, "sbcError");

  afterAll(() => {
    parseValueToDocPathSpy.mockRestore();
    sbcErrorSpy.mockRestore();
  });

  beforeEach(async () => {
    await sbcUtils.resetCharacterData();
    // Reset mock calls before each test start
    parseValueToDocPathSpy.mockReset();
    sbcErrorSpy.mockReset();
  });

  // Dummy parser, needs to be adjusted per test
  let parser = new NotesParser(["base.mr"]);

  test("undefined arguments throw error", () => {
    expect(parser.parse()).rejects.toThrow("The arguments 'value' and 'line' are mandatory.");
    expect(parser.parse("valueArg")).rejects.toThrow("The arguments 'value' and 'line' are mandatory.");
  });

  test("empty value returns true", async () => {
    const parseValueToPathMock = jest.spyOn(sbcParserModule, "parseValueToPath");

    expect(await parser.parse("3", 2)).toBeTruthy();
    expect(parseValueToPathMock).toHaveBeenCalledTimes(1);
    // FIXME: `app.processData.notes` is overwritten and needs to return to its original state
    // eslint-disable-next-line no-undef
    expect(parseValueToPathMock).toHaveBeenCalledWith(app.processData.notes, "base.mr", "3");
  });

  test("error during parsing returns false", async () => {
    jest.spyOn(sbcParserModule, "parseValueToPath").mockImplementationOnce(() => {
      throw Error("Dummy forced error");
    });
    // Suppress terminal output for readability
    jest.spyOn(console, "error").mockImplementationOnce(() => {});

    expect(await parser.parse("3", 2)).toBeFalsy();
  });

  test("error during parsing calls sbcError and adds error to array of errors", async () => {
    jest.spyOn(sbcParserModule, "parseValueToPath").mockImplementationOnce(() => {
      throw Error("Dummy forced error");
    });
    // Suppress terminal output for readability
    jest.spyOn(console, "error").mockImplementationOnce(() => {});

    let [value, line] = ["3", 0];
    await parser.parse(value, line);

    expect(sbcErrorSpy).toHaveBeenCalledTimes(1);
    expect(sbcErrorSpy).toHaveBeenCalledWith(2, "Parse", `Failed to parse ${value} into notes.base.mr`, line);
  });
});
