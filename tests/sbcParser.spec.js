/* eslint-disable no-undef */
/* eslint-disable no-unused-vars */
import { jest } from "@jest/globals";

import { loadStatBlock } from "../utils/data.js";

import { sbcData } from "../scripts/sbcData.js";
import { parserMapping } from "../scripts/Parsers/parser-mapping.js";
import { sbcUtils } from "../scripts/sbcUtils.js";

// Import what we are testing
import { sbcParser, parseInteger, parseValueToDocPath, generateNotesSection } from "../scripts/sbcParser.js";
import { sbcConfig } from "../scripts/sbcConfig.js";

beforeEach(() => {
  // Populating the mapper
  parserMapping.initMapping();
});

/**
 * NOTES:
 * - `app.processData.input.text` is an string which contains the Statblock.
 */

describe("prepareInput Suite", () => {
  // Mocks are declared outside `beforeAll` in order to be accessible later
  // All this functions aren't needed to be executed since they are out of the
  // scope of this test
  const parseInputMock = jest.spyOn(sbcParser, "parseInput").mockImplementation(async () => null);

  afterEach(() => {
    // Reset the sbcData
    app.processData.input.text = "";
    app.processData.input.prepared = {};
    // We want to reset the mock calls
    sbcParser.parseInput.mockClear();
  });

  afterAll(() => {
    // Return the function to its original state
    sbcParser.parseInput.mockRestore();
  });

  describe("empty input Suite", () => {
    beforeEach(() => {
      app.processData.input.text = "";
    });

    test("input raises error", async () => {
      await expect(sbcParser.prepareInput()).rejects.toEqual("Given input is empty.");
      expect(processData.errors.length).toBe(1);
      expect(processData.errors[0]).toMatchObject({
        keyword: "Prepare",
        level: 0,
        line: -1,
        message: "Given input is empty.",
      });
    });

    test("parseInput is not called", async () => {
      // We already know this raises an error
      await sbcParser.prepareInput().catch(() => null);

      expect(parseInputMock).toHaveBeenCalledTimes(0);
    });

    test("resetErrorLog is not called", async () => {
      // We already know this raises an error
      await sbcParser.prepareInput().catch(() => null);

      expect(resetErrorLogMock).toHaveBeenCalledTimes(0);
    });

    test("resetHighlights is not called", async () => {
      // We already know this raises an error
      await sbcParser.prepareInput().catch(() => null);

      expect(resetHighlightsMock).toHaveBeenCalledTimes(0);
    });
  });

  describe("minimal input Suite", () => {
    beforeEach(() => {
      app.processData.input.text = loadStatBlock("tests/statblocks/Minimal.txt");
    });

    test("data length equals linebreaks of statblock", async () => {
      await sbcParser.prepareInput();

      expect(app.processData.input.prepared.data.length).toBe(9);
    });

    test("parseInput is called once", async () => {
      await sbcParser.prepareInput();

      expect(parseInputMock).toHaveBeenCalledTimes(1);
    });

    test("resetErrorLog is called once", async () => {
      // We already know this raises an error
      await sbcParser.prepareInput().catch(() => null);

      expect(resetErrorLogMock).toHaveBeenCalledTimes(1);
    });

    test("resetHighlights is called once", async () => {
      // We already know this raises an error
      await sbcParser.prepareInput().catch(() => null);

      expect(resetHighlightsMock).toHaveBeenCalledTimes(1);
    });
  });

  describe("simple monsters Suite", () => {
    beforeEach(() => {
      app.processData.input.text = loadStatBlock("tests/statblocks/Goblin.txt");
    });

    test("data length equals linebreaks of statblock", async () => {
      await sbcParser.prepareInput();

      expect(app.processData.input.prepared.data.length).toBe(33);
    });

    test("parseInput is called once", async () => {
      await sbcParser.prepareInput();

      expect(parseInputMock).toHaveBeenCalledTimes(1);
    });

    test("resetErrorLog is called once", async () => {
      // We already know this raises an error
      await sbcParser.prepareInput().catch(() => null);

      expect(resetErrorLogMock).toHaveBeenCalledTimes(1);
    });

    test("resetHighlights is called once", async () => {
      // We already know this raises an error
      await sbcParser.prepareInput().catch(() => null);

      expect(resetHighlightsMock).toHaveBeenCalledTimes(1);
    });
  });

  describe("Transformations", () => {
    test.each([
      ["–", "-"],
      ["—", "-"],
      ["−", "-"],
      ["×", "x"],
      [",", ","],
      [",", ","],
      ["½", "1/2"],
      ["⅓", "1/3"],
      ["¼", "1/4"],
      ["⅕", "1/5"],
      ["⅙", "1/6"],
      ["⅛", "1/8"],
      // TODO: Missing Source Superscript
      // TODO: Missing Bestiary Source Superscript
      ["ﬂ", "fl"],
      ["ﬁ", "fi"],
      ["ﬀ", "ff"],
      ["ﬃ", "ffi"],
      ["ﬄ", "ffl"],
      ["ﬆ", "st"],
    ])("Transformation of '%s' into '%s'", async (original, transformed) => {
      app.processData.input.text = original;
      await sbcParser.prepareInput();

      expect(app.processData.input.prepared.data.length).toBe(1);
      expect(app.processData.input.prepared.data[0]).toBe(transformed);
    });
  });
});

describe("parseInteger Suite", () => {
  test("string number transform into a number", () => {
    let result = parseInteger("12");

    expect(result).toBeNumber();
    expect(result).toBe(12);
  });

  describe("non numerics return 0", () => {
    test.each([NaN, undefined, null, "randomWord"])("%s transforms into 0", (input) => {
      let result = parseInteger(input);

      expect(result).toBeNumber();
      expect(result).toBe(0);
    });
  });
});

describe("parseValueToDocPath Suite", () => {
  /** @type {Actor} */
  let actor;

  test("actor is correctly updated", async () => {
    actor = await Actor.create(
      {
        name: "sbc | Actor Template",
        type: "npc",
        folder: sbcData.customWIPFolderId,
      },
      { temporary: false }
    );

    let fieldsToChange = ["name", "prototypeToken.name"];
    let valueForFields = "Goblin";
    let updatedActor = await parseValueToDocPath(actor, fieldsToChange, valueForFields);

    expect(updatedActor.name).toBe(valueForFields);
    expect(updatedActor.prototypeToken.name).toBe(valueForFields);
  });
});

describe("generateNotesSection Suite", () => {
  let updateMock;

  beforeEach(async () => {
    sbcUtils.resetData();
    await sbcUtils.resetCharacterData();
    /**
     * IMPORTANT: Since FoundryVTT doesn't update the Document
     * when calling `update` and neither do our mocks in order
     * to make as close to reality as possible we can't check
     * if the actor has the attributes (unless the function)
     * returns it. So instead we check the `update` method has
     * been called with expected values.
     */
    updateMock = sbcData.characterData.actorData.update;
    // Cleaning mocks used in this suite
    updateMock.mockClear();
    renderTemplate.mockClear();
  });

  test("calls Actor.update with correct params", async () => {
    await generateNotesSection();

    expect(updateMock).toHaveBeenCalledOnce();
    // What this mean is get the last (and only in this case) call
    // from that get the first (and only in this case) argument
    // Since the first argument is an object with quite long HTML we split that to
    // assert individually
    const key = Object.keys(updateMock.mock.lastCall[0]).pop();
    expect(key).toBe("system.details.notes.value");
  });

  test("today date is correctly parsed", async () => {
    jest.useFakeTimers().setSystemTime(new Date("2020-01-01"));
    await generateNotesSection();
    jest.useRealTimers();

    expect(updateMock).toHaveBeenCalledOnce();
    // What this mean is get the last (and only in this case) call
    // from that get the first (and only in this case) argument
    // Since the first argument is an object with quite long HTML we split that to
    // assert individually
    const value = Object.values(updateMock.mock.lastCall[0]).pop();
    expect(value).toInclude("January 1, 2020");
    //expect(value).toInclude("December 31, 2019");
  });

  test("renderTemplate is called", async () => {
    await generateNotesSection();

    expect(renderTemplate).toHaveBeenCalledOnce();
    expect(renderTemplate).toHaveBeenCalledWith("modules/pf1-statblock-converter/templates/sbcPreview.hbs", {
      actor: {
        // Inside of actor there are many functions declarations
        // we can't really replicate, so instead we just hardcode what
        // we know are static values
        ...sbcData.characterData.actorData,
        folder: "",
        name: "sbc | Actor Template",
        type: "npc",
        id: "DummyActor",
      },
      flags: processData.flags,
      notes: {},
    });
  });

  test("input is in notes", async () => {
    const expectedInput = "lorem ipsum dolmen random 123";
    app.processData.input.text = expectedInput;

    await generateNotesSection();

    expect(updateMock).toHaveBeenCalledOnce();
    // What this mean is get the last (and only in this case) call
    // from that get the first (and only in this case) argument
    // Since the first argument is an object with quite long HTML we split that to
    // assert individually
    const value = Object.values(updateMock.mock.lastCall[0]).pop();
    expect(value).toInclude(expectedInput);
  });
});
