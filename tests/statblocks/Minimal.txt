Minimal
This is a minimal input for testing purposes.
Minimal CR ⅓
Defense
AC 16, touch 13, flat-footed 14 (+2 armor, +2 Dex, +1 shield, +1 size)
Offense
Melee short sword +2 (1d4/19–20)
Statistics
Str 11, Dex 15, Con 12, Int 10, Wis 9, Cha 6