import { sbcUtils } from "./sbcUtils.js";

// eslint-disable-next-line no-unused-vars
import * as sbcTypes from "./sbcTypes.js";

const tokenBarAttributes = [
  "NONE",
  "attributes.hp",
  "spells.spellbooks.primary.spellPoints",
  "spells.spellbooks.secondary.spellPoints",
  "spells.spellbooks.tertiary.spellPoints",
  "spells.spellbooks.spelllike.spellPoints",
  "details.xp",
  "attributes.ac.normal.total",
  "attributes.ac.flatFooted.total",
  "attributes.ac.touch.total",
  "attributes.cmd.total",
  "attributes.speed.land.total",
  "attributes.speed.land.fly.total",
  "attributes.speed.land.swim.total",
  "attributes.speed.land.climb.total",
  "attributes.speed.land.burrow.total",
  "attributes.sr.total",
];

export class sbcSettings {
  // Updates the array of custom compendia, which is used to find items, feats, etc. with a higher priority then the pf1.compendia
  static async updateCustomCompendiums(isInitializing = false) {
    /** @type {sbcTypes.SBC} */
    let sbcInstance = window.SBC;
    await sbcInstance.compendiumSearch.registerDefaultCompendia();

    sbcUtils.log("Updating custom compendiums");

    /** @type {string[]} */
    let customCompendiums = game.settings
      .get(sbcInstance.config.modData.mod, "customCompendiums")
      .replace(/\s/g, "")
      .split(/[,;]/g)
      .filter((comp) => !!comp);

    if (!customCompendiums.length) {
      return;
    }
    // Replace semicolons with commas

    // Get the names of all available compendiums
    let packKeys = Array.from(game.packs.keys());

    const invalidCompendiums = [];
    const validCompendiums = customCompendiums.filter((comp) => {
      if (sbcInstance.compendiumSearch.isKnownCompendium(comp)) {
        return false;
      }

      // ... and check if it's available
      if (!packKeys.includes(comp)) {
        // save invalid compendiums for the error message
        invalidCompendiums.push(comp);
        return false;
      }

      // save valid compendiums to overwrite the settings
      return true;
    });

    if (!isInitializing) {
      // If there are invalid compendiums, let the user know
      if (invalidCompendiums.length > 0) {
        let error =
          "sbc-pf1 | Failed to add the following compendiums to sbc, please check for typos (" +
          invalidCompendiums.toString() +
          ")";
        ui.notifications.error(error);
        sbcUtils.log(error);
      }

      if (!validCompendiums.length) {
        return;
      }

      const info = "sbc-pf1 | Added the following compendiums to sbc (" + validCompendiums.toString() + ")";
      ui.notifications.info(info);
      sbcUtils.log(info);
    }

    await sbcInstance.compendiumSearch.processCompendiums(validCompendiums);
  }

  // Update the default folder into which statblocks get imported
  static async updateImportFolder() {
    sbcUtils.log("Updating custom import folder");

    // Get the custom folder name from the settings
    let customFolderName = sbcSettings.getSetting("importFolder");

    if (customFolderName !== "") {
      let searchForExistingFolder = null;

      try {
        searchForExistingFolder = await game.folders.find(
          (entry) => entry.data.name === customFolderName && entry.data.type === "Actor"
        );
      } catch (err) {
        let info = "sbc-pf1 | Something went wrong while searching for an existing import folder.";
        ui.notifications.info(info);
        sbcUtils.log(info);
      }

      if (searchForExistingFolder === null) {
        // No existing folder found
        let newFolder = await Folder.create({
          name: customFolderName,
          type: "Actor",
          color: "#e76f51",
          parent: null,
        });

        let info = "Created a custom folder for imported statblocks.";
        ui.notifications.info(info);
        sbcUtils.log(info);
        return newFolder.id;
      } else {
        // Existing folder found
        return searchForExistingFolder.id;
      }
    } else {
      // No custom import folder defined
    }
  }

  /**
   * Get a setting from the module settings
   *
   * @param {string} key  The key of the setting to get
   * @returns {any}       The value of the setting
   */
  static getSetting(key) {
    // Special handling of the attribute bars
    if (key.includes("attributeBar")) {
      let number = parseInt(key.replace("attributeBar", ""));
      if (isNaN(number)) {
        return {};
      }

      let attributeKey = game.settings.get(window.SBC.config.modData.mod, number === 1 ? "bar1" : "bar2");
      if (attributeKey === "0") {
        return {};
      } else {
        return { attribute: tokenBarAttributes[attributeKey] };
      }
    }

    return game.settings.get(window.SBC.config.modData.mod, key);
  }
}

const settings = {
  debug: {
    default: false,
    type: Boolean,
  },
  defaultActorType: {
    default: 0,
    type: String,
    choices: { 0: "NPC", 1: "PC" },
  },
  inputDelay: {
    default: 750,
    type: Number,
    range: { min: 250, max: 2000, step: 10 },
  },
  importFolder: {
    default: "sbc | Imported Statblocks",
    type: String,
    onChange: () => sbcSettings.updateImportFolder(),
  },
  customCompendiums: {
    default: "",
    type: String,
    onChange: async () => await sbcSettings.updateCustomCompendiums(),
  },
  createBuff: {
    default: true,
    type: Boolean,
  },
  createAttacks: {
    default: false,
    type: Boolean,
  },
  createAssociations: {
    default: false,
    type: Boolean,
  },
  rollBonusesIntegration: {
    default: false,
    type: Boolean,
  },
  pcsight: {
    default: true,
    type: Boolean,
  },
  npcsight: {
    default: true,
    type: Boolean,
  },
  disposition: {
    default: -1,
    type: String,
    choices: { "-1": "Hostile", 0: "Neutral", 1: "Friendly" },
  },
  displayName: {
    default: 20,
    type: String,
    choices: {
      0: "None",
      10: "Control",
      20: "Owner Hover",
      30: "Hover",
      40: "Owner",
      50: "Always",
    },
  },
  displayBars: {
    default: 20,
    type: String,
    choices: {
      0: "None",
      10: "Control",
      20: "Owner Hover",
      30: "Hover",
      40: "Owner",
      50: "Always",
    },
  },
  bar1: {
    default: "24",
    type: String,
    choices: tokenBarAttributes,
  },
  bar2: {
    default: "0",
    type: String,
    choices: tokenBarAttributes,
  },
};

export const registerSettings = function () {
  /** @type {sbcTypes.ConfigData} */
  let config = window.SBC.config;

  Object.entries(settings).forEach(([key, value]) => {
    game.settings.register(
      config.modData.mod,
      key,
      Object.assign(
        {
          name: sbcUtils.translate(`settings.${key}.name`),
          hint: sbcUtils.translate(`settings.${key}.hint`),
          scope: "world",
          config: true,
        },
        value
      )
    );
  });
};
