// eslint-disable-next-line no-unused-vars
// import { ItemAction } from "../pf1/module/components/action.mjs";
// eslint-disable-next-line no-unused-vars
import { CompendiumOps } from "./sbcCompendium.js";
// eslint-disable-next-line no-unused-vars
import { sbcSettings } from "./sbcSettings.js";

// ---------------- GENERAL TYPES ----------------
/**
 * AppData
 *
 * @typedef {object} AppData                                    Data that's relevant for the SBC
 * @property {string} folderId                                  The folder ID for the actor
 * @property {string} wipFolderId                               The folder ID for the WIP actor
 */
export const AppData = {};

/**
 * Input Dialog - an extension of Foundry's Application
 *
 * @typedef {object} InputDialog
 * @property {ProcessData} processData                          The processed data for the statblock
 * @augments {Application}
 */
export const InputDialog = {};

/**
 * SBC window data
 *
 * @typedef {object} SBC
 * @property {InputDialog} app                                  The current instance of the input dialog
 * @property {import("../pf1/module/documents/actor/actor-pf.mjs").ActorPF} actor The actor that's being created
 * @property {number} actorType                                 The type of actor that's being created
 * @property {string} folderId                                  The folder ID for the actor
 * @property {string} wipFolderId                               The folder ID for the WIP actor
 * @property {ConfigData} config                                The configuration object for the SBC
 * @property {boolean} configInitialized                        Whether the configuration has been initialized
 * @property {CompendiumOps} compendiumSearch                   The compendium searching construct
 * @property {boolean} isImporting                              Whether the SBC is currently importing
 * @property {sbcSettings} settings                             The settings for the SBC
 */
export const SBC = {};

// ---------------- PARSING TYPES ----------------
/**
 * ChangeData
 *
 * @typedef {{[key: string]: ChangeDataTarget}} ChangeData      A collection of PF1 change data, organized by target and type to track the highest amount of that type for that target.
 */
export const ChangeData = {};

/**
 * ChangeDataTarget
 *
 * @typedef {{[key: string]: number}} ChangeDataTarget          A change data target, with the bonus type and amount
 */
export const ChangeDataTarget = {};

/**
 * CharacterData
 *
 * @typedef CharacterData                                       Character data that's relevant from the statblock
 * @property {ConversionValidationData} conversionValidation    Data that's relevant for the conversion validation
 * @property {string[]} weaponFocus                             Weapon Focus feats
 * @property {string[]} weaponSpecialization                    Weapon Specialization feats
 * @property {string[]} weaponFocusGreater                      Greater Weapon Focus feats
 * @property {string[]} weaponSpecializationGreater             Greater Weapon Specialization feats
 * @property {any} weaponGroups                                 Weapon groups
 * @property {string[]} classes                                 The classes of the character
 * @property {string[]} archetypes                              The archetypes of the character
 * @property {string} race                                      The tag of the race of the character
 * @property {boolean} hdMaximized                              Whether the first HD of a base class has been maximized
 * @property {Map<string, string>} prestigeClassCasting         The casting data of the prestige class offsets
 */
export const CharacterData = {};

/**
 * ConversionValidationData
 *
 * @typedef {object} ConversionValidationData                   Data that's relevant for the conversion validation
 * @property {object} context                                   The context notes of the conversion
 * @property {object} attributes                                The attributes of the conversion
 * @property {object} skills                                    The skills of the conversion
 * @property {object} spellBooks                                The spellbooks of the conversion
 */
export const ConversionValidationData = {};

/**
 * ErrorData
 *
 * @typedef {object} ErrorData                                  Data that's relevant for the errors
 * @property {symbol} level                                     The level of the error
 * @property {string} keyword                                   The keyword of the error
 * @property {string} message                                   The message of the error
 * @property {number} line                                      The line number of the error, defaults to -1
 * @property {string} value                                     The value of the error, for highlighting purposes
 * @property {Function} errorLabel                              Function to get the error label in human-readable format
 */
export const ErrorData = {};

/**
 * ErrorHighlightData
 *
 * @typedef {object} ErrorHighlightData                         Data that's relevant for highlighting errors
 * @property {string} value                                     The value of the error
 * @property {string} messageBefore                             The message of the error
 * @property {string} messageAfter                              The message of the error
 * @property {number} line                                      The line number of the error
 * @property {boolean} marked                                   Whether the error has been marked
 */
export const ErrorHighlightData = {};

/**
 * FlagData
 *
 * @typedef {object} FlagData                                   Flags for SBC to use while parsing the statblock
 * @property {boolean} hasNoCMB                                 Whether the creature has no CMB
 * @property {boolean} hasNoCMD                                 Whether the creature has no CMD
 * @property {boolean} noStr                                    Whether the creature has a STR score
 * @property {boolean} noDex                                    Whether the creature has a DEX score
 * @property {boolean} noCon                                    Whether the creature has a CON score
 * @property {boolean} noInt                                    Whether the creature has an INT score
 * @property {boolean} noWis                                    Whether the creature has a WIS score
 * @property {boolean} noCha                                    Whether the creature has a CHA score
 * @property {boolean} isUndead                                 Is the creature undead?
 * @property {boolean} isConstruct                              Is the creature a construct?
 * @property {boolean} hasWeaponFinesse                         Has the weapon finesse feat
 */
export const FlagData = {};

/**
 * InputData
 *
 * @typedef {object} InputData                                  Data that's relevant for the input
 * @property {string[]} data                                    The data for the statblock
 * @property {string} text                                      The text input for the statblock
 * @property {string} html                                      The HTML input for the statblock
 * @property {object} prepared                                  The prepared input for the statblock
 * @property {string[]} prepared.data                           The prepared data for the statblock
 * @property {boolean} prepared.success                         Whether the prepared input was successful
 * @property {boolean} parsedSuccess                            Whether the input was successfully parsed
 */
export const InputData = {};

/**
 * NotesData
 *
 * @typedef {object} NotesData                                  Notes data that's relevant for the statblock
 *
 * @property {object} base                                      The base notes for the statblock
 * @property {string} base.cr                                   The Challenge Rating of the statblock
 * @property {string} base.mr                                   The Mythic Rating of the statblock
 * @property {string} base.shortDescription                     The short description of the statblock
 * @property {string} base.source                               The source of the statblock
 * @property {string} base.xp                                   The XP of the statblock
 * @property {string} base.template                             The template of the statblock
 * @property {string} base.race                                 The race listed in the statblock
 * @property {string[]} base.classes                            The classes listed in the statblock
 * @property {string} base.alignment                            The alignment of the statblock
 * @property {string} base.size                                 The size of the statblock
 * @property {string} base.creatureType                         The creature type of the statblock
 * @property {string[]} base.senses                             The senses of the statblock
 * @property {string} base.perception                           The perception of the statblock
 * @property {object[]} base.aura                               The aura of the statblock
 * @property {string} base.aura.name                            The name of the aura
 * @property {string} base.aura.dc                              The DC of the aura
 * @property {string} base.aura.dcNotes                         The DC notes of the aura
 * @property {string} base.aura.saveType                        The save of the aura
 * @property {string} base.aura.range                           The range of the aura
 * @property {string} base.aura.effect                          The description of the aura
 *
 * @property {object} defense                                   The defense notes for the statblock
 * @property {string} defense.acNormal                          The AC of the statblock
 * @property {string} defense.acTouch                           The touch AC of the statblock
 * @property {string} defense.acFlatFooted                      The flat-footed AC of the statblock
 * @property {object[]} defense.acTypes                         The AC types of the statblock
 * @property {string} defense.acTypes.type                      The type of the AC
 * @property {string} defense.acTypes.value                     The value of the AC
 * @property {string} defense.hpTotal                           The total HP of the statblock
 * @property {string} defense.hdTotal                           The total HD of the statblock
 * @property {string} defense.hdPool                            The HD pool of the statblock
 * @property {string} defense.hdAbilities                       The HD abilities of the statblock
 * @property {string} defense.fortSave                          The fortitude save of the statblock
 * @property {string} defense.refSave                           The reflex save of the statblock
 * @property {string} defense.willSave                          The will save of the statblock
 * @property {string} defense.saveNotes                         The save notes of the statblock
 * @property {object} defense.defensiveAbilities                The defensive abilities of the statblock
 * @property {string} defense.defensiveAbilities.name           The name of the defensive ability
 * @property {string[]} defense.dr                              The damage reduction of the statblock
 * @property {string[]} defense.immune                          The immunities of the statblock
 * @property {string[]} defense.resist                          The resistances of the statblock
 * @property {string} defense.sr                                The spell resistance of the statblock
 * @property {string} defense.srNotes                           The spell resistance notes of the statblock
 * @property {string} defense.weakness                          The weaknesses of the statblock
 *
 * @property {object} offense                                   The offense notes for the statblock
 * @property {string[]} offense.speed                           The speed of the statblock
 * @property {string} offense.melee                             The melee attacks of the statblock
 * @property {string} offense.ranged                            The ranged attacks of the statblock
 * @property {string} offense.space                             The space of the statblock
 * @property {string} offense.reach                             The reach of the statblock
 * @property {string} offense.reachContext                      The reach context of the statblock
 * @property {object} offense.specialAttacks                    The special attacks of the statblock
 * @property {string} offense.specialAttacks.name               The name of the special attack
 * @property {object[]} offense.spellBooks                      The spellbooks of the statblock
 * @property {string} offense.spellBooks.name                   The name of the spellbook
 * @property {string} offense.spellBooks.cl                     The caster level of the spellbook
 * @property {string} offense.spellBooks.concentration          The concentration bonus of the spellbook
 * @property {string} offense.spellBooks.asf                    The arcane spell failure of the spellbook
 * @property {object[]} offense.spellBooks.spellRows            The spells of the spellbook
 * @property {object} offense.spellBooks.spellRows.frequency    The frequency of the spell row
 * @property {string} offense.spellBooks.spellRows.frequency.amount The amount of the frequency
 * @property {string} offense.spellBooks.spellRows.frequency.type  The type of the frequency
 * @property {string} offense.spellBooks.spellRows.level        The level of the spell row
 * @property {object[]} offense.spellBooks.spellRows.spells     The spells of the spellbook row
 * @property {string} offense.spellBooks.spellRows.spells.name  The name of the spell
 * @property {boolean} offense.spellBooks.spellRows.spells.domain Whether the spell is a domain spell
 * @property {string} offense.spellBooks.spellRows.spells.prepCount The prepared count of the spell
 * @property {string} offense.spellBooks.spellRows.spells.dc     The DC of the spell
 *
 * @property {object} tactics                                   The tactics notes for the statblock
 * @property {string} tactics.beforeCombat                      The tactics before combat
 * @property {string} tactics.duringCombat                      The tactics during combat
 * @property {string} tactics.morale                            The morale of the statblock
 * @property {string} tactics.baseStatistics                    The base statistics of the statblock
 *
 * @property {object} statistics                                The statistics notes for the statblock
 * @property {string} statistics.str                            The STR score of the statblock
 * @property {string} statistics.dex                            The DEX score of the statblock
 * @property {string} statistics.con                            The CON score of the statblock
 * @property {string} statistics.int                            The INT score of the statblock
 * @property {string} statistics.wis                            The WIS score of the statblock
 * @property {string} statistics.cha                            The CHA score of the statblock
 * @property {string} statistics.cmbContext                     The CMB context of the statblock
 * @property {string} statistics.cmdContext                     The CMD context of the statblock
 * @property {object[]} statistics.feats                        The feats of the statblock
 * @property {string} statistics.feats.name                     The name of the feat
 * @property {object[]} statistics.skills                       The skills of the statblock
 * @property {string} statistics.skills.name                    The name of the skill
 * @property {string} statistics.skills.prefixedValue           The prefixed value of the skill
 * @property {object[]} statistics.racialSkillModifiers         The racial skill modifiers of the statblock
 * @property {string} statistics.racialSkillModifiers.name      The name of the racial skill modifier
 * @property {string} statistics.racialSkillModifiers.prefixedValue The value of the racial skill modifier
 * @property {string} statistics.racialSkillModifiers.context   The context of the racial skill modifier
 * @property {string[]} statistics.languages                    The languages of the statblock
 * @property {object[]} statistics.sq                           The special qualities of the statblock
 * @property {string} statistics.sq.name                        The name of the special quality
 * @property {string} statistics.combatGear                     The combat gear of the statblock
 * @property {string} statistics.otherGear                      The other gear of the statblock
 * @property {string} statistics.gearNotes                      The gear notes of the statblock
 *
 * @property {object} ecology                                   The ecology notes for the statblock
 * @property {string} ecology.environment                       The environment of the statblock
 * @property {string} ecology.organization                      The organization of the statblock
 * @property {string} ecology.treasure                          The treasure of the statblock
 *
 * @property {object} specialAbilities                          The special abilities of the statblock
 * @property {string} specialAbilities.parsedSpecialAbilities   The parsed special abilities of the statblock
 *
 * @property {object} description                               The description notes for the statblock
 * @property {string} description.long                          The description of the statblock
 */
export const NotesData = {};

/**
 * SpellbookNotesData
 *
 * @typedef {object} SpellbookNotesData                         The spellbook notes for the statblock
 * @property {string} key                                      The key of the spellbook
 * @property {string} concentration                            The concentration notes of the spellbook
 * @property {string} casterLevel                              The caster level notes of the spellbook
 */
export const SpellbookNotesData = {};

/**
 * MiscNotesData
 *
 * @typedef {object} MiscNotesData                  Miscellaneous notes for the statblock (for any context note inputs on the actor)
 * @property {string[]} acNotes                     The AC notes for the statblock
 * @property {string} saveNotes                     The saving throw notes for the statblock
 * @property {string} cmdNotes                      The CMD notes for the statblock
 * @property {string} srNotes                       The spell resistance notes for the statblock
 * @property {number} naturalAC                     The natural AC of the statblock
 * @property {number} sr                            The spell resistance of the statblock
 * @property {SpellbookNotesData[]} spellbookNotes  The spellbook notes for the statblock
 */
export const MiscNotesData = {};

/**
 * TreasureParsingData
 *
 * @typedef {object} TreasureParsingData                        Data that's relevant for parsing the treasure
 * @property {string} treasureToParse                           The treasure to parse
 * @property {number} lineToRemove                              The line to remove
 * @property {number} statisticsStartLine                       The line where the statistics start
 */
export const TreasureParsingData = {};

/**
 * ProgressBarData
 *
 * @typedef {object} ProgressBarData                            Data that's relevant for the progress bar
 * @property {string} class                                     A css class for the progress bar for additional styling
 * @property {string} text                                      The task for the progress bar
 * @property {number} width                                     The total width of the progress bar as a percentage
 */
export const ProgressBarData = {};

/**
 * ProcessData
 *
 * @typedef {object} ProcessData
 * @property {boolean} actorReady                               Whether the actor is ready
 * @property {AppData} appData                                  Data that's relevant for SBC
 * @property {CharacterData} characterData                      Character data that's relevant from the statblock
 * @property {ChangeData} changes                               A collection of PF1 change data, organized by target and type to track the highest amount of that type for that target
 * @property {ErrorData[]} errors                               Errors that occurred during parsing
 * @property {FlagData} flags                                   Flags for SBC to use while parsing the statblock
 * @property {number} foundCategories                           The number of categories found
 * @property {number} parsedCategories                          The number of categories parsed
 * @property {boolean} imported                                 Whether the statblock has been imported
 * @property {InputData} input                                  Input data for the statblock
 * @property {MiscNotesData} miscNotes                          Miscellaneous notes for the statblock (for any context note inputs on the actor)
 * @property {NotesData} notes                                  Notes for the statblock
 * @property {TreasureParsingData} treasureParsing              Data for parsing the treasure
 * @property {ProgressBarData} progressBar                      Data for the progress bar
 */
export const ProcessData = {};

/**
 * RawSpellBookData
 *
 * @typedef {object} RawSpellBookData
 *
 * @property {string} firstLine                                 The first line of the spellbook
 * @property {string[]} spells                                  The lines of spells in the spellbook from the statblock
 * @property {string} spellCastingType                          The spell-casting type of the spellbook
 * @property {string} spellCastingClass                         The spell-casting class of the spellbook
 * @property {number} casterLevel                               The caster level of the spellbook
 * @property {number} concentrationBonus                        The concentration bonus of the spellbook
 * @property {string} spellBookType                             The type of spellbook
 * @property {boolean} isSpellLike                              Whether the spellbook is spell-like
 * @property {boolean} isAlchemist                              Whether the spellbook is for an alchemist
 * @property {string} spellBookName                             The name of the spellbook
 */
export const RawSpellBookData = {};

/**
 * @typedef {object} SpellRowData
 *
 * The spell data for the given row in the spellbook
 *
 * @property {number} spellLevel The spell level
 * @property {string} spellsPerX The spells per X (day, week, month, year)
 * @property {number} spellsPerXTotal The total number of spells per X (day, week, month, year)
 * @property {boolean} isAtWill Whether the spells are at will
 * @property {string} frequency The frequency of the spells
 * @property {boolean} isCantrip Whether the spells are cantrips
 * @property {boolean} isSpellRow Whether the row is a row of spells or information
 * @property {boolean} spellRowIsInitialized Whether the spell row is initialized
 * @property {string} wizardSchool The wizard school of the spell book itself
 * @property {boolean} isSpellLike Whether the spell belongs to a spell-like book
 */
export const SpellRowData = {};

/**
 * DamageReductionEntry
 *
 * @typedef {object} DamageReductionEntry
 * @property {number} amount                                    The amount of the damage reduction
 * @property {string} type1                                     The first type of the damage reduction
 * @property {string} type2                                     The second type of the damage reduction
 * @property {boolean} operator                                 The operator for the damage reduction (or/and, respectively)
 */
export const DamageReductionEntry = {};

// ---------------- CONFIG TYPES ----------------
/**
 * ModData
 *
 * @typedef {object} ModData                                    The metadata for the module
 * @property {string} version                                   The version of the module
 * @property {string} mod                                       The name of the module
 * @property {string} modName                                   The name of the module
 */
export const ModData = {};

/**
 * TokenSettings
 *
 * @typedef {object} TokenSettings                              The token settings
 * @property {number} displayName                               The display name
 * @property {object} pcsight                                   The player character sight
 * @property {boolean} pcsight.enabled                          Whether the player character sight is enabled
 * @property {object} npcsight                                  The non-player character sight
 * @property {boolean} npcsight.enabled                         Whether the non-player character sight is enabled
 * @property {number} disposition                               The disposition
 * @property {number} displayBars                               The display bars
 * @property {object} bar1                                      The first bar
 * @property {object} bar2                                      The second bar
 */
export const TokenSettings = {};

/**
 * ConfigOptions
 *
 * @typedef {object} ConfigOptions                              The options for the PF1 Statblock Converter module
 * @property {boolean} debug                                    Whether to enable debug mode
 * @property {number} inputDelay                                The delay for the input
 * @property {number} defaultActorType                          The default actor type
 * @property {boolean} createBuff                               Whether to create a buff
 * @property {boolean} createAttacks                            Whether to create attacks
 * @property {TokenSettings} tokenSettings                      The token settings
 */
export const ConfigOptions = {};

/**
 * ConfigConstData
 *
 * @typedef {object} ConfigConstData                            The constant values used in the module
 * @property {number} lineHeight                                The line height
 * @property {{[key: string]: number}} crFractions              The CR fractions
 * @property {{[key: number]: string}} actorType                The actor types
 * @property {string[]} tokenBarAttributes                      The possible token bar attributes to assign when creating an actor
 * @property {{[key: string]: string}} suffixMultiples          The suffixes for multiple attacks in a sequence
 */
export const ConfigConstData = {};

/**
 * MagicAbility
 *
 * @property {string} [name]                                    The name of the magical ability
 * @property {number} staticCost                                The static cost of the magical ability
 * @property {number} cl                                        The caster level of the magical ability
 * @property {number} bonus                                     The enhancement bonus of the magical ability
 * @property {string} aura                                      The aura of the magical ability
 * @property {object} available                                 Availability of the magic item property
 * @property {boolean} hasSpecialTerm                           Whether the magical ability has a special term
 * @property {string} [specialTerm]                             The special term of the magical ability
 */
export const MagicAbility = {};

/**
 * ConfigData
 *
 * @typedef {object} ConfigData                                 The configuration data for the PF1 Statblock Converter module
 * @property {ModData} modData                                  Contains the version and name of the module
 * @property {ConfigConstData} const                            Contains constant values used in the module
 * @property {ConfigOptions} options                            Contains the options for the module
 * @property {string[]} sources                                 Contains the abbreviations for the sources used in the module
 * @property {string[]} lineCategories                          Contains the categories for the lines in the statblock
 * @property {string[]} lineStarts                              Contains the starting words for the lines in the statblock
 * @property {string[]} techColors                              Contains the colors for the tech items
 * @property {string[]} techTiers                               Contains the tiers for the tech items
 * @property {object} magicalAbilities                          Contains the magical abilities for the items
 * @property {{[key: string]: string}} magicalAbilities.Auras   The value of the magical abilities
 * @property {string[]} magicalAbilities.Prefixes               Common prefix terms for magical abilities that could appear before or after the ability name
 * @property {{[key: string]: MagicAbility}} magicalAbilities.Abilities The magical abilities for the items
 * @property {{[key: string]: number}} weaponGroups             Contains the weapon groups for the items
 * @property {string[]} compendiumsToBeIndexed                  Contains the compendiums to be indexed
 * @property {string[]} metamagic                               Contains the metamagic abilities for the items
 * @property {{[key: string]: string}} sillyGear                Contains a map of all silly gear names Paizo made up
 * @property {string[]} races                                   Contains the races for race parsing
 * @property {string[]} classes                                 Contains the classes for class parsing
 * @property {string[]} mythicPaths                             Contains the mythic classes for class parsing
 * @property {string[]} racialClasses                           Contains the racial classes for class parsing
 * @property {string[]} prestigeClasses                         Contains the prestige class names class parsing
 * @property {string[]} naturalAttacks                          Contains the natural attacks from the ssytem
 * @property {string[]} templates                               Contains the templates from the system
 * @property {Function} initializeConfig                        Initializes the config by loading some necessary compendia data
 */
export const ConfigData = {};

// ---------------- COMPENDIUM TYPES ------------
/**
 * SearchEntityData
 * The data structure for the search entity data, to be used when attempting to
 * find an entity in a compendium.
 *
 * Order of precedence when searching:
 * 1. name
 * 2. shortName
 * 3. altName
 * 4. altName2
 *
 * @typedef {object} SearchEntityData
 * @property {string} name                                      The name of the entity to search for, as it appears in the statblock
 * @property {string} shortName                                 The short name of the entity to search for, without anything from parentheses
 * @property {string} altName                                   The first alternative name of the entity to search for
 * @property {string} altName2                                  The second alternative name of the entity to search for
 */
export const SearchEntityData = {};

/**
 * PlaceholderEntityData
 *
 * A placeholder entity is created whenever SBC fails to find an entity in the compendiums.
 * The only guaranteed fields are the name and type of the entity. The img field is optional with a default value.
 *
 * The rest of the fields are optional and depend on the entity type:
 * - Creature-related: creatureType, subTypes
 * - Gear-related: subtext, currency, enhancement, mwk
 * - Class-related: wizardClass, suffix, archetype, level
 * - Ability-related: specialAbilityType, desc
 *
 * @typedef {object} PlaceholderEntityData
 * @property {string} name                                      The name of the entity
 * @property {string} type                                      The type of the entity
 * @property {string} img                                       The image of the entity
 * @property {string} creatureType                              The creature type of the race/creature
 * @property {string[]} subTypes                                The subtypes of the race/creature type
 * @property {number} quantity                                  The quantity of the gear
 * @property {string} subtext                                   The subtext of the gear (the part in parentheses)
 * @property {object} currency                                  The currency of the gear
 * @property {number} currency.pp                               The amount of platinum pieces
 * @property {number} currency.gp                               The amount of gold pieces
 * @property {number} currency.sp                               The amount of silver pieces
 * @property {number} currency.cp                               The amount of copper pieces
 * @property {string} enhancement                               The enhancement bonus of the gear
 * @property {string} mwk                                       The masterwork status of the gear
 * @property {string} wizardClass                               The wizard type of the class, if any
 * @property {string} suffix                                    The suffix of the class (anything after the class name)
 * @property {string} archetype                                 The archetype of the class (anything in the parentheses after the class name)
 * @property {string} level                                     The level of the class
 * @property {string} specialAbilityType                        The special ability type of the special ability (Na, Ex, Sp, Su)
 * @property {string} desc                                      The description of the special ability
 * @property {Array} changes                                    The changes to the entity
 * @property {string} size                                      The size of the creature
 * @property {number} price                                     The price of the gear
 */
export const PlaceholderEntityData = {};

// ---------------- OFFENSE TYPES ----------------

/**
 * AttackData
 *
 * @typedef {object} AttackData
 * @property {string} attackName                                The name of the attack
 * @property {string} formattedAttackName                       The formatted name of the attack
 * @property {ItemAction[]} actions                             The actions for the attack
 * @property {string} img                                       The image for the attack
 * @property {string} subType                                   The subtype of the attack
 * @property {string} attackNotes                               The attack notes
 * @property {string[]} effectNotes                             The effect notes
 * @property {string} specialEffects                            The special effects
 * @property {boolean} isMasterwork                             Whether the attack is masterwork
 * @property {number} enhancementBonus                          The enhancement bonus of the attack
 * @property {boolean} isPrimaryAttack                          Whether the attack is the primary attack
 * @property {string} held                                      The held status of the attack
 * @property {boolean} isTouch                                  Whether the attack is a touch attack
 * @property {boolean} isNonlethal                              Whether the attack is nonlethal
 * @property {boolean} isNatural                                Whether the attack is natural
 * @property {boolean} isBroken                                 Whether the attack is broken
 * @property {string[]} baseTypes                               The base types of the attack
 * @property {object} weaponGroups                              The weapon groups of the attack
 * @property {string[]} weaponGroups.value                      The value of the weapon groups
 * @property {string} weaponGroups.custom                       The custom weapon group
 * @property {string} baseMaterial                              The base material of the attack
 * @property {string} material                                  The material of the attack
 * @property {{[key: string]: string}} addons                   The addons for the attack
 * @property {boolean} isFlurryOfBlows                          True/False, does the creature have Flurry of Blows?
 */
export const AttackData = {};

/**
 * ActionData
 *
 * @typedef {object} ActionData
 * @property {number} numberOfAttacks                           The number of attacks
 * @property {number} numberOfIterativeAttacks                  The number of iterative attacks
 * @property {string} iterativeFormula                          The iterative formula
 * @property {ItemAction[]} attackParts                         The attack parts
 * @property {string} formulaicAttacksCountFormula              The formulaic attacks count formula
 * @property {string} formulaicAttacksBonusFormula              The formulaic attacks bonus formula
 * @property {number} inputAttackModifier                       The input attack modifier
 * @property {number} calculatedAttackBonus                     The calculated attack bonus
 * @property {string} attackAbilityType                         The attack ability type
 * @property {number} attackAbilityModifier                     The attack ability modifier
 * @property {string} damage                                    The damage
 * @property {string} damageAbilityType                         The damage ability type
 * @property {number} numberOfDamageDice                        The number of damage dice
 * @property {number} damageDie                                 The damage die
 * @property {number} damageBonus                               The damage bonus
 * @property {number} damageModifier                            The damage modifier
 * @property {Array} damageParts                                The damage parts
 * @property {Array} nonCritParts                               The non-critical parts
 * @property {string} defaultDamageType                         The default damage type
 * @property {Set<string>} damageTypes                          The damage types
 * @property {string} specialDamageType                         The special damage type
 * @property {boolean} hasSpecialDamageType                     Whether the attack has a special damage type
 * @property {string} weaponSpecial                             The weapon special
 * @property {number} critRange                                 The critical range
 * @property {number} critMult                                  The critical multiplier
 * @property {number} damageMult                                The damage multiplier
 * @property {string} attackRangeUnits                          The attack range units
 * @property {string} attackRangeIncrement                      The attack range increment
 * @property {boolean} useOriginalDamage                        Whether to use the original damage
 * @property {number} featAttackBonus                           The feat attack bonus
 * @property {number} featDamageBonus                           The feat damage bonus
 * @property {string} featAttackBonusString                     The feat attack bonus string
 * @property {string} featDamageBonusString                     The feat damage bonus string
 * @property {boolean} hasMultiattack                           True/False, does the creature have the multiattack feat?
 * @property {import ("../pf1/module/documents/item/item-class.mjs").ItemClassPF} flurryClass                          The class of the flurry of blows feature
 * @property {boolean} isUnchainedMonk                          True/False, is the creature an Unchained Monk?
 * @property {boolean} flurryOfBlowsType                        The type of flurry of blows, if applicable
 */
export const ActionData = {};

/**
 * WeaponAttackDetails
 *
 * @typedef {object} WeaponAttackDetails
 * @property {string} attackName                                The name of the attack
 * @property {object} weaponGroups                              The weapon groups
 * @property {string[]} weaponGroups.value                      The value of the weapon groups
 * @property {string} weaponGroups.custom                       The custom weapon group
 * @property {boolean} isMasterwork                             Whether the attack is masterwork
 * @property {string} attackAbilityType                         The attack ability type
 * @property {number} attackAbilityModifier                     The attack ability modifier
 * @property {string} damageAbilityType                         The damage ability type
 * @property {number} damageBonus                               The damage bonus
 * @property {number} damageMult                                The damage multiplier
 * @property {number} critRange                                 The critical range
 * @property {number} critMult                                  The critical multiplier
 * @property {string} iterativeFormula                          The iterative formula
 * @property {string} damageFormula                             The damage formula
 * @property {string} originalDamageFormula                     The original damage formula
 * @property {number} featAttackBonus                           The feat attack bonus
 * @property {number} featDamageBonus                           The feat damage bonus
 * @property {string} featAttackBonusString                     The feat attack bonus string
 * @property {string} featDamageBonusString                     The feat damage bonus string
 * @property {string} material                                  The material
 * @property {Array} addons                                     The addons
 * @property {boolean} isBroken                                 Whether the weapon used to attack is broken
 */
export const WeaponAttackDetails = {};

/**
 * AttackComparison
 *
 * @typedef {object} AttackComparison
 * @property {string} statblockIterativeFormula                 The iterative formula from the statblock
 * @property {string} statblockDamageFormula                    The damage formula from the statblock
 * @property {number} statblockDamageBonus                      The damage bonus from the statblock
 * @property {string} statblockFullFormula                      The full formula from the statblock
 * @property {number} statblockEnhancementBonus                 The enhancement bonus from the statblock
 * @property {boolean} itemExists                               Whether the item exists
 * @property {string} itemIterativeFormula                      The iterative formula from the item
 * @property {string} itemDamageFormula                         The damage formula from the item
 * @property {number} itemDamageBonus                           The damage bonus from the item
 * @property {string} itemFullFormula                           The full formula from the item
 * @property {number} itemEnhancementBonus                      The enhancement bonus from the item
 */
export const AttackComparison = {};

// ---------------- DEFENSE TYPES ----------------
/**
 * The settings for the HP calculation
 *
 * @typedef {object} hpSettings
 * @property {boolean} continuous       False = discrete, True = continuous, default False
 * @property {number} maximized         How many HD to maximize for the character, default 1
 * @property {string} rounding          How to round the HP, default "up"
 * @property {number} maximizedProgress How many HD have been maximized so far
 */
export const hpSettings = {};

/**
 * @typedef {object} hdSettings The settings for the HD of a class type (ignored for Mythic)
 *
 * @property {boolean} auto     Whether to automatically calculate the HP for this class type
 * @property {number} rate      The rate of HP increase (to multiply HD size to get the average)
 * @property {number} maximized Is this class type possible to maximize?
 */
export const hdSettings = {};

/**
 * Processed Formula Result
 *
 * @typedef {object} ProcessedFormulaResult
 * @property {string} formula The processed formula
 * @property {number} total The processed total
 */
export const ProcessedFormulaResult = {};

export const sbcTypes = {};
