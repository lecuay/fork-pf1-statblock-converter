import { sbcUtils } from "../sbcUtils.js";
import { sbcError } from "../sbcError.js";
import { parserMapping } from "../Parsers/parser-mapping.js";
// eslint-disable-next-line no-unused-vars
import * as sbcTypes from "../sbcTypes.js";
import { createItem } from "../sbcParser.js";

/* ------------------------------------ */
/* Parser for statistics data           */
/* ------------------------------------ */

let gearParsed = false;
let hasGear = false;
let skills = "";
let skillsLine = -1;

/**
 * Parses the statistics data of the statblock.
 *
 * @param {sbcTypes.ProcessData} processData  Process data object.
 * @param {Array.<string>} data               Array of separated data.
 * @param {number} startLine                  Index from where base part starts.
 * @returns {Promise<boolean>}               `false` if any error is caught.
 */
export async function parseStatistics(processData, data, startLine) {
  window.SBC.settings.getSetting("debug") &&
    console.groupCollapsed(
      "sbc-pf1 | " + processData.parsedCategories + "/" + processData.foundCategories + " >> PARSING STATISTICS DATA"
    );

  let parsedSubCategories = [];
  processData.notes["statistics"] = {};
  processData.treasureParsing.statisticsStartLine = startLine;
  let abilities = [];
  let abilitiesLine = 0;
  let sqs = "";
  let sqsLine = 0;
  gearParsed = false;
  hasGear = data.some((line) => /\b((?:Combat |Other |^)Gear)\b/i.test(line));
  skills = "";
  skillsLine = -1;

  // Empty actor inventory to remove any class-granted items, such as spellbooks
  for (let itemType of ["consumable", "equipment", "loot", "weapon", "container"]) {
    await Promise.all(window.SBC.actor.itemTypes[itemType]?.map(async (item) => await item.delete()));
  }

  // Loop through the lines
  for (let line = 0; line < data.length; line++) {
    try {
      let lineContent = data[line];

      // Parse Base Attack
      if (!parsedSubCategories["bab"] && lineContent.match(/^Base Atk\b/i)) {
        let bab = lineContent.match(/Base Atk\b\s*([+-]?\d+)/gi)[0].replace(/Base Atk\b\s*/i, "");

        processData.characterData.conversionValidation.attributes["bab"] = +bab;
      }

      // Parse CMB
      if (!parsedSubCategories["cmb"] && lineContent.match(/\bCMB\b/i)) {
        parsedSubCategories["cmb"] = await parseCMB(processData, lineContent, startLine + line);
      }

      // Parse CMD
      if (!parsedSubCategories["cmd"] && lineContent.match(/\bCMD\b/i)) {
        parsedSubCategories["cmd"] = await parseCMD(processData, lineContent, startLine + line);
      }

      // Parse Feats
      if (!parsedSubCategories["feats"] && lineContent.match(/^Feats\b/i)) {
        parsedSubCategories["feats"] = await parseFeats(processData, lineContent, startLine + line);
      }

      // Parse Languages
      if (!parsedSubCategories["languages"] && lineContent.match(/^Languages\b/i)) {
        parsedSubCategories["languages"] = await parseLanguages(processData, lineContent, startLine + line);
      }

      // Parse Gear
      if (/((?:Combat |Other |^)Gear)\b/i.test(lineContent)) {
        parsedSubCategories["gear"] = await parseGear(processData, lineContent, startLine + line);
        gearParsed = true;
      }

      // Parse Abilities
      // This gets checked every loop by its nature, as the first pass will cache the abilities
      // and the second pass will parse them after the gear has been parsed.
      // At least it exits early if we're not checking abilities and the gear isn't parsed.
      if (!parsedSubCategories["abilities"]) {
        if (/(Str|Dex|Con|Int|Wis|Cha)\s*(\d+|-)/i.test(lineContent) && !abilities.length) {
          // Cache the abilities so that we can process them after the gear is parsed.
          abilities = lineContent.match(/((Str|Dex|Con|Int|Wis|Cha)\s*(\d+|-))/gi);
          abilitiesLine = line;
        }

        // gearIsParsed(line, data.length) &&
        if (gearIsParsed(line, data.length) && abilities.length > 0) {
          for (let i = 0; i < abilities.length; i++) {
            await parseAbility(abilities[i], processData, abilitiesLine);
          }

          parsedSubCategories["abilities"] = true;
        }
      }

      // Parse Skills
      // This gets checked every loop by its nature, as the first pass will cache the skills
      // and the second pass will parse them after the gear has been parsed.
      // At least it exits early if we're not checking skills and the gear and abilities aren't parsed.
      if (!parsedSubCategories["skills"]) {
        if (lineContent.match(/^Skills\b/i)) {
          skills = lineContent
            .match(/Skills\b\s*(.*)/i)[1]
            .replace(/\s*[,;]+/g, ",")
            .trim();
          skillsLine = startLine + line;
          parsedSubCategories["skills"] = true;
        }
      }

      // Parse SQ
      if (!parsedSubCategories["sq"]) {
        if (lineContent.match(/^SQ\b/i)) {
          sqs = lineContent;
          sqsLine = line;
        }

        if (sqs && parsedSubCategories["abilities"]) {
          parsedSubCategories["sq"] = await parseSQ(processData, sqs, startLine + sqsLine);
        }
      }
    } catch (err) {
      window.SBC.settings.getSetting("debug") && console.error(err);

      let errorMessage = "Parsing the statistics data failed at the highlighted line";
      let error = new sbcError(sbcError.ERRORLEVELS.ERROR, "Parse/Statistics", errorMessage, startLine + line);
      processData.errors.push(error);
      processData.input.prepared.success = false;

      window.SBC.settings.getSetting("debug") && console.groupEnd();
      return false;
    }
  }

  sbcUtils.log("RESULT OF PARSING STATISTICS DATA (TRUE = PARSED SUCCESSFULLY)");
  sbcUtils.log(parsedSubCategories);
  window.SBC.settings.getSetting("debug") && console.groupEnd();

  return true;
}

/**
 * Parses the CMB of the statblock.
 *
 * @param {sbcTypes.ProcessData} processData  Process data object.
 * @param {string} lineContent                Line content to parse.
 * @param {number} line                       Line number.
 * @returns {Promise<boolean>}               `false` if any error is caught.
 */
async function parseCMB(processData, lineContent, line) {
  let parserCmb = parserMapping.map.statistics.cmb;
  let cmbRaw = lineContent
    .match(/CMB\s+([+-]?(\d+)?\s*(\(.*\))?;?)/i)[1]
    .trim()
    .replace(";", "");

  let cmb = cmbRaw.match(/^([+-](\d+)?)/)?.[0] ?? null;
  processData.flags.hasNoCMB = !cmb;
  cmb = cmb ?? 0;

  let cmbContext = sbcUtils.parseSubtext(cmbRaw)[1];

  if (!processData.flags.hasNoCMB) processData.characterData.conversionValidation.attributes.cmb = +cmb;
  if (cmbContext) {
    const originalCmbContext = cmbContext;
    let match;
    const regex = /\+?(-?\d+)/g;
    while ((match = regex.exec(cmbContext)) !== null) {
      const oldValue = +match[1];
      const newValue = sbcUtils.prefixNumber(oldValue - +cmb);
      const matchLength = match[0].length;

      cmbContext = cmbContext.substring(0, match.index) + newValue + cmbContext.substring(match.index + matchLength);

      regex.lastIndex += newValue.length;
    }

    //processData.characterData.conversionValidation.context.cmb = cmbContext;
    processData.notes.statistics.cmbContext = " (" + originalCmbContext + ")";
    processData.miscNotes.cmbNotes = cmbContext ?? "";
  }

  return await parserCmb.parse(+cmb, line);
}

/**
 * Parses the CMD of the statblock.
 *
 * @param {sbcTypes.ProcessData} processData  Process data object.
 * @param {string} lineContent                Line content to parse.
 * @param {number} line                       Line number.
 * @returns {Promise<boolean>}               `false` if any error is caught.
 */
async function parseCMD(processData, lineContent, line) {
  let parserCmd = parserMapping.map.statistics.cmd;
  let cmdRaw = lineContent
    .match(/CMD\s+([+-]?(\d+)?\s*(\(.*\))?;?)/i)[1]
    .trim()
    .replace(";", "");

  // Check if CMD is "-"
  let cmd = cmdRaw.match(/^[+-]?(\d+)?/)?.[0] ?? null;
  processData.flags.hasNoCMD = !cmd;
  cmd = cmd ?? 0;

  let cmdContext = sbcUtils.parseSubtext(cmdRaw)[1];

  if (!processData.flags.hasNoCMD) processData.characterData.conversionValidation.attributes.cmd = +cmd;
  if (cmdContext) {
    const originalCmdContext = cmdContext;
    let match;
    const regex = /\+?(-?\d+)/g;
    while ((match = regex.exec(cmdContext)) !== null) {
      const oldValue = +match[1];
      const newValue = sbcUtils.prefixNumber(oldValue - +cmd);
      const matchLength = match[0].length;

      cmdContext = cmdContext.substring(0, match.index) + newValue + cmdContext.substring(match.index + matchLength);

      regex.lastIndex += newValue.length;
    }

    //processData.characterData.conversionValidation.context.cmd = cmdContext;
    processData.notes.statistics.cmdContext = " (" + originalCmdContext + ")";
    processData.miscNotes.cmdNotes = cmdContext ?? "";
  }

  return await parserCmd.parse(+cmd, line);
}

/**
 * Parses the feats of the statblock.
 *
 * @param {sbcTypes.ProcessData} processData  Process data object.
 * @param {string} lineContent                Line content to parse.
 * @param {number} line                       Line number.
 * @returns {Promise<boolean>}               `false` if any error is caught.
 */
async function parseFeats(processData, lineContent, line) {
  /** @type {sbcTypes.SBC} */
  const sbcInstance = window.SBC;

  let parserFeats = parserMapping.map.statistics.feats;
  let feats = lineContent
    .match(/Feats\b\s*(.*)/i)[1]
    .replace(/\s*[,;]+/g, ",")
    .trim();

  const [result, entities] = await parserFeats.parse(feats, line, "feats", "feat", "feat");
  processData.notes.statistics.feats = entities;

  if (!sbcInstance.settings.getSetting("createAssociations")) {
    const compendiumOps = sbcInstance.compendiumSearch;
    const featCount = sbcInstance.actor.getFeatCount().excess;
    if (featCount > 0) {
      let searchEntity = {
        search: sbcUtils.createSearchSet(["Bonus Feat", "Bonus Feats", "Bonus Combat Feats"], true),
        item: "Bonus Feats",
      };

      let entity = await compendiumOps.findEntityInCompendia(searchEntity, {
        itemType: "feat",
        itemSubType: "classFeat",
        classes: processData.characterData.classes,
        race: processData.characterData.race,
      });

      if (entity) {
        await sbcUtils.addAssociatedClass(entity, sbcInstance.actor.itemTypes.class);

        await createItem(entity);
      }
    }
  }

  await processFeats(processData);
  return result;
}

/**
 * Parses the languages of the statblock.
 *
 * @param {sbcTypes.ProcessData} processData  Process data object.
 * @param {string} lineContent                Line content to parse.
 * @param {number} line                       Line number.
 * @returns {Promise<boolean>}               `false` if any error is caught.
 */
async function parseLanguages(processData, lineContent, line) {
  let parserLanguages = parserMapping.map.statistics.languages;
  let languages = lineContent
    .match(/Languages\b\s*(.*)/i)[1]
    .replace(/\s*[,;]+/g, ",")
    .trim();

  return await parserLanguages.parse(languages, line);
}

/**
 * Parses the special qualities of the statblock.
 *
 * @param {sbcTypes.ProcessData} processData  Process data object.
 * @param {string} lineContent                Line content to parse.
 * @param {number} line                       Line number.
 * @returns {Promise<boolean>}               `false` if any error is caught.
 */
async function parseSQ(processData, lineContent, line) {
  let parserSQ = parserMapping.map.statistics.sq;
  let sqs = lineContent
    .match(/SQ\b\s*(.*)/i)[1]
    .replace(/\s*[,;]+\s*/g, ", ")
    .trim();

  const [result, entities] = await parserSQ.parse(
    sqs,
    line,
    ["monster-abilities", "class-abilities"],
    ["equipment", "feat", "weapon"],
    null,
    false
  );

  processData.notes.statistics.sq = entities;

  return result;
}

/**
 * Parses the gear of the statblock.
 *
 * @param {sbcTypes.ProcessData} processData  Process data object.
 * @param {string} lineContent                Line content to parse.
 * @param {number} line                       Line number.
 * @returns {Promise<boolean>}               `false` if any error is caught.
 */
async function parseGear(processData, lineContent, line) {
  let parserGear = parserMapping.map.statistics.gear;
  // Combat Gear, Other Gear, Gear
  let gear = lineContent
    .replace(/(Combat Gear|Other Gear|Gear)/g, "")
    .replace(/[,;]+/g, ",")
    .replace(/[,;]$/, "")
    .trim();

  if (!processData.notes.statistics.gear) {
    processData.notes.statistics.gear = [];
  }

  return await parserGear.parse(gear, line);
}

/**
 * Parses the abilities of the statblock.
 *
 * @param {string} abilityKey                   Key of the ability to parse.
 * @param {sbcTypes.ProcessData} processData    Process data object.
 * @param {number} line                         Line number.
 * @returns {Promise<boolean>}                 `false` if any error is caught.
 */
async function parseAbility(abilityKey, processData, line) {
  let ability = abilityKey.match(/(\w+)/)[1];
  let valueInStatblock = abilityKey.match(/(\d+|-)/)[1];

  // FLAGS NEED TO BE WORKED ON AFTER PARSING IS FINISHED
  if (valueInStatblock === "-" || valueInStatblock === 0 || valueInStatblock === "0") {
    valueInStatblock = 0;
    let flagKey = "no" + ability.capitalize();
    processData.flags[flagKey] = true;
  }

  // CHECK CURRENT ITEMS FOR CHANGES IN ABILITIES (MAINLY RACES)
  // Check the current Items for changes to ablities
  const abilityChangesInItems = await sbcUtils.getTotalFromChanges(window.SBC.actor, ability);
  const abilityChangeFromAge = parserMapping.map.base.age.getAbilityChange(ability.toLowerCase());

  let correctedValue = +valueInStatblock - abilityChangesInItems - abilityChangeFromAge;

  processData.characterData.conversionValidation.attributes[ability] = +valueInStatblock;
  processData.notes.statistics[ability.toLowerCase()] = +valueInStatblock;

  let parser = parserMapping.map.statistics[ability.toLowerCase()];
  await parser.parse(+correctedValue, line);
}

/**
 * Checks if the gear is parsed or not.
 *
 * @param {number} line         Current line index.
 * @param {number} dataLength   Length of the data array.
 * @returns {boolean}           `true` if the gear is parsed.
 */
function gearIsParsed(line, dataLength) {
  if (!hasGear) return true;
  return gearParsed || line === dataLength - 1;
}

/**
 * Processes feats to determine weapon focus and weapon specialization.
 *
 * @param {sbcTypes.ProcessData} processData  Process data object.
 */
async function processFeats(processData) {
  let feats = window.SBC.actor.itemTypes.feat.filter((feat) => feat.system.subType === "feat");

  for (let feat of feats) {
    let featName = feat.name.toLowerCase();
    let featSubtext = sbcUtils.parseSubtext(featName)[1];
    if (!featSubtext) continue;

    if (/greater weapon focus/.test(featName)) {
      processData.characterData.weaponFocusGreater.push(featSubtext);
    } else if (/weapon focus/.test(featName)) {
      processData.characterData.weaponFocus.push(featSubtext);
    } else if (/greater weapon specialization/.test(featName)) {
      processData.characterData.weaponSpecializationGreater.push(featSubtext);
    } else if (/weapon specialization/.test(featName)) {
      processData.characterData.weaponSpecialization.push(featSubtext);
    }
  }

  sbcUtils.log("Weapon Focus", processData.characterData.weaponFocus);
  sbcUtils.log("Weapon Focus Greater", processData.characterData.weaponFocusGreater);
  sbcUtils.log("Weapon Specialization", processData.characterData.weaponSpecialization);
  sbcUtils.log("Weapon Specialization Greater", processData.characterData.weaponSpecializationGreater);
}

/**
 * Parses the skills of the statblock.
 * This is done indirectly by sbcParser after the Defense data is processed for HP.
 * This is because the racial classes might be removed during HP processing.
 *
 * @returns {Promise<void>}
 */
export async function parseDelayedSkills() {
  let parserSkills = parserMapping.map.statistics.skills;
  await parserSkills.parse(skills, skillsLine);
}
