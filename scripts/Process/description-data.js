import { sbcUtils } from "../sbcUtils.js";
import { sbcError } from "../sbcError.js";
import { parserMapping } from "../Parsers/parser-mapping.js";

// eslint-disable-next-line no-unused-vars
import * as sbcTypes from "../sbcTypes.js";

/* ------------------------------------ */
/* Parser for description data          */
/* ------------------------------------ */
/**
 * Parses the description data
 *
 * @param {sbcTypes.ProcessData} processData    The current process data
 * @param {Array<string>} data                  The data to parse
 * @param {number} startLine                    The line number where the data starts
 * @returns {Promise<boolean>}                  Returns true if parsing was successful, false otherwise
 */
export async function parseDescription(processData, data, startLine) {
  window.SBC.settings.getSetting("debug") &&
    console.groupCollapsed(
      "sbc-pf1 | " + processData.parsedCategories + "/" + processData.foundCategories + " >> PARSING DESCRIPTION DATA"
    );

  processData.notes["description"] = {};

  let description = "";

  // Loop through the lines
  for (let line = 0; line < data.length; line++) {
    try {
      let lineContent = data[line];
      switch (lineContent.toLowerCase()) {
        case "description":
          break;
        case "":
          description = description.concat("\n");
          break;
        default:
          description = description.concat(lineContent + "\n");
          break;
      }
    } catch (err) {
      window.SBC.settings.getSetting("debug") && console.error(err);
      let errorMessage = `Parsing the description data failed at line ${line + startLine}`;
      let error = new sbcError(sbcError.ERRORLEVELS.WARNING, "Parse/Description", errorMessage, line + startLine);
      processData.errors.push(error);
      processData.input.prepared.success = false;

      window.SBC.settings.getSetting("debug") && console.groupEnd();
      return false;
    }
  }

  processData.notes.description.long = description;

  let parserDescription = parserMapping.map.description;
  await parserDescription.parse(description, startLine);

  sbcUtils.log("RESULT OF PARSING DESCRIPTION DATA (TRUE = PARSED SUCCESSFULLY)");
  window.SBC.settings.getSetting("debug") && console.groupEnd();

  return true;
}
