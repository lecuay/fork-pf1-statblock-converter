import { parserMapping } from "../Parsers/parser-mapping.js";
import { OtherRaces } from "../Content/index.js";
import { sbcError } from "../sbcError.js";
import { sbcUtils } from "../sbcUtils.js";

// eslint-disable-next-line no-unused-vars
import * as sbcTypes from "../sbcTypes.js";
import { alignmentRegex, classesRegex, creatureTypeAndSizeRegex, genderRegex, sizeRegex } from "../sbcRegex.js";

/* ------------------------------------ */
/* Regexes for base data                 */
/* ------------------------------------ */
let aura = { content: "", line: 0 };

/* ------------------------------------ */
/* Parser for base data                 */
/* ------------------------------------ */
/**
 * Parses the base data of the statblock. This is everything up to the first category.
 *
 * @param {sbcTypes.ProcessData} processData  Process data object.
 * @param {Array.<string>} data               Array of separated data.
 * @param {number} startLine                  Index from where base part starts.
 * @returns {Promise.<boolean>}               `false` if any error is caught.
 */
export async function parseBase(processData, data, startLine) {
  window.SBC.settings.getSetting("debug") &&
    console.groupCollapsed(
      "sbc-pf1 | " + processData.parsedCategories + "/" + processData.foundCategories + " >> PARSING BASE DATA"
    );

  let parsedSubCategories = [];
  processData.notes["base"] = {};

  aura = { content: "", line: 0 };

  let mythicRankLine = null;
  let mythicRankLineContent = null;

  // Loop through the lines
  for (let line = 0; line < data.length; line++) {
    try {
      let lineContent = data[line].trim();

      // Skip empty lines
      if (lineContent === "") continue;

      // Check for the name
      if (!parsedSubCategories["name"]) {
        [parsedSubCategories["name"], lineContent] = await parseName(lineContent, line);
        if (lineContent === "") continue;
      }

      // Parse Challenge Rating
      if (!parsedSubCategories["cr"] && lineContent.match(/\bCR\b/i) !== null) {
        [parsedSubCategories["cr"], lineContent] = await parseCR(processData, lineContent, line);
        if (lineContent === "") continue;
      }

      // Parse Mythic Rank (uses NotesParser, as mr currently is not supported by FVTT PF1)
      if (!parsedSubCategories["mr"]) {
        if (lineContent.match(/\bMR\b/i) !== null) {
          mythicRankLine = line;
          mythicRankLineContent = lineContent;
        }

        if (parsedSubCategories["classes"] && mythicRankLineContent) {
          [parsedSubCategories["mr"], mythicRankLineContent] = await parseMR(
            processData,
            mythicRankLineContent,
            mythicRankLine
          );
        }
      }

      if (line !== 0) {
        // Parse Source (uses NotesParser, the source has no separate field in FVTT PF1)
        if (!parsedSubCategories["source"] && lineContent.toLowerCase().startsWith("source")) {
          [parsedSubCategories["source"], lineContent] = await parseSource(lineContent, line);
          if (lineContent === "") continue;
        }

        // Parse XP
        if (!parsedSubCategories["xp"] && lineContent.toLowerCase().startsWith("xp")) {
          [parsedSubCategories["xp"], lineContent] = await parseXP(processData, lineContent, line);
          if (lineContent === "") continue;
        }

        // Parse Gender
        if (!parsedSubCategories["gender"] && genderRegex().test(lineContent)) {
          [parsedSubCategories["gender"], lineContent] = await parseGender(lineContent, line);
          if (lineContent === "") continue;
        }

        // Parse Race
        if (!parsedSubCategories["race"]) {
          [parsedSubCategories["race"], lineContent] = await parseRace(lineContent, line);
          if (lineContent === "") continue;
        }

        // Parse Classes
        if (!parsedSubCategories["classes"]) {
          // Check for classes only in lines that do not start with an alignment
          // So as not to match the class "MEDIUM" when it's a Medium Humanoid for example
          const isAlignmentLine = lineContent.match(alignmentRegex());

          // Check for classes only in lines that do not start with "Source"
          // So as not to match the class "Witch" when it's included in "Source Pathfinder #72: The Witch Queen's Revenge pg. 86"
          const isSourceLine = lineContent.toLowerCase().startsWith("source");

          // let isAgeLine = lineContent.match(/(Old|Middle Age|Middle-aged|Venerable)/i);
          // if (isAgeLine && !parsedSubCategories["age"]) {
          //     let age = isAgeLine[1];
          //     let parserAge = parserMapping.map.base.age
          //     parsedSubCategories["age"] = await parserAge.parse(age, line);

          //     if (parsedSubCategories["age"] === true) lineContent = sbcUtils.cleanLine(age, lineContent);
          // }
          if (
            !parsedSubCategories["age"] &&
            sbcUtils.haystackRegexSearch(["Middle[- ]Aged?", "Old", "Venerable"], lineContent)
          ) {
            [parsedSubCategories["age"], lineContent] = await parseAge(lineContent, line);
            if (lineContent === "") continue;
          }

          if (!isAlignmentLine && !isSourceLine) {
            parsedSubCategories["classes"] = await parseClasses(lineContent, line);
            if (lineContent === "") continue;
          }
        }

        // Parse Alignment
        if (!parsedSubCategories["alignment"] && alignmentRegex().test(lineContent)) {
          [parsedSubCategories["alignment"], lineContent] = await parseAlignment(processData, lineContent, line);
          if (lineContent === "") continue;

          if (parsedSubCategories["alignment"] === true) {
            parsedSubCategories["classes"] = true;
          }
        }

        // Parse Size and Space / Token Size
        if (!parsedSubCategories["size"] && sizeRegex().test(lineContent)) {
          parsedSubCategories["size"] = await parseSizeAndSpace(processData, lineContent, line);
          if (lineContent === "") continue;
        }

        // Parse Creature Type and Subtype, but only when they are found after a size declaration
        if (
          !parsedSubCategories["creatureType"] &&
          creatureTypeAndSizeRegex().test(lineContent) &&
          sizeRegex().test(lineContent)
        ) {
          [parsedSubCategories["creatureType"], lineContent] = await parseCreatureType(lineContent, line);
          if (lineContent === "") continue;
        }

        // Parse Initiative
        if (!parsedSubCategories["init"] && lineContent.toLowerCase().match(/^init\b/i)) {
          [parsedSubCategories["init"], lineContent] = await parseInit(processData, lineContent, line);
          if (lineContent === "") continue;
        }

        // Parse Senses
        if (!parsedSubCategories["senses"] && lineContent.match(/\bSenses\b/i) !== null) {
          [parsedSubCategories["senses"], lineContent] = await parseSenses(lineContent, line);
          if (lineContent === "") continue;
        }

        // Parse Aura
        if (!parsedSubCategories["aura"] && lineContent.match(/\bAura\b/i) !== null) {
          aura = { content: lineContent, line: line };
          parsedSubCategories["aura"] = true;
          // [parsedSubCategories["aura"], lineContent] = await parseAura(lineContent, line);
          // if (lineContent === "") continue;
        }
      }
    } catch (err) {
      window.SBC.settings.getSetting("debug") && console.error(err);
      const errorMessage = "Parsing the base data failed at the highlighted line";
      const error = new sbcError(sbcError.ERRORLEVELS.ERROR, "Parse/Base", errorMessage, startLine + line);
      processData.errors.push(error);
      processData.input.prepared.success = false;

      window.SBC.settings.getSetting("debug") && console.groupEnd();
      return false;
    }
  }

  // Parse errors and warnings
  if (!parsedSubCategories["cr"] && window.SBC.actorType === 0) {
    const error = new sbcError(
      sbcError.ERRORLEVELS.WARNING,
      "Parse/Base",
      "No CR for this NPC detected, please check the highlighted line",
      0
    );
    processData.errors.push(error);
  }

  if (!parsedSubCategories["creatureType"]) {
    const error = new sbcError(sbcError.ERRORLEVELS.ERROR, "Parse/Base", "No creature type found!");
    processData.errors.push(error);
  }

  sbcUtils.log("RESULT OF PARSING BASE DATA (TRUE = PARSED SUCCESSFULLY)");
  sbcUtils.log(parsedSubCategories);
  window.SBC.settings.getSetting("debug") && console.groupEnd();

  return true;
}

/**
 * Parses the name of the statblock.
 *
 * @param {string} lineContent  Content of the line.
 * @param {number} line         Line number.
 * @returns {Promise.<[boolean, string]>}  Returns a tuple with the result of the parsing and the cleaned line content.
 */
async function parseName(lineContent, line) {
  const parserName = parserMapping.map.base.name;
  const name = lineContent
    .replace(/\bCR\b.*$/i, "")
    .replace(/\($/, "")
    .trim();
  const result = await parserName.parse(name.trim().capitalize(), line);

  if (result) {
    lineContent = sbcUtils.cleanLine(name, lineContent);
  }

  return [result, lineContent];
}

/**
 * Parses the Challenge Rating of the statblock.
 *
 * @param {sbcTypes.ProcessData} processData    Process data object.
 * @param {string} lineContent                  Content of the line.
 * @param {number} line                         Line number.
 * @returns {Promise<[boolean, string]>}        Returns a tuple with the result of the parsing and the cleaned line content.
 */
async function parseCR(processData, lineContent, line) {
  const parserCR = parserMapping.map.base.cr;
  let crMatch = lineContent.match(/\bCR\b\s(-|\d*\.\d+|\d+(?:\/\d+)?)/i);
  let cr = crMatch[1];

  if (cr.includes("/")) processData.notes.base.cr = cr;
  else {
    for (const [frac, dec] in Object.entries(window.SBC.config.const.crFractions)) {
      if (+cr === dec) {
        processData.notes.base.cr = frac;
        break;
      }
    }
  }
  const result = await parserCR.parse(parseFloat(window.SBC.config.const.crFractions[cr] || cr), line);
  if (result) {
    lineContent = sbcUtils.cleanLine(crMatch[0], lineContent);
  }

  return [result, lineContent];
}

/**
 * Parses the Mythic Rank of the statblock.
 *
 * @param {sbcTypes.ProcessData} processData    Process data object.
 * @param {string} lineContent                  Content of the line.
 * @param {number} line                         Line number.
 * @returns {Promise<[boolean, string]>}        Returns a tuple with the result of the parsing and the cleaned line content.
 */
async function parseMR(processData, lineContent, line) {
  const parserMR = parserMapping.map.base.mr;
  const mrMatch = lineContent.match(/\bMR\b\s(\d+)/i);
  const mr = mrMatch[1];

  processData.notes.base.mr = mr;
  const result = await parserMR.parse(mr, line);

  if (result) {
    lineContent = sbcUtils.cleanLine(mr, lineContent);
  }

  return [result, lineContent];
}

/**
 * Parses the Source of the statblock.
 *
 * @param {string} lineContent                  Content of the line.
 * @param {number} line                         Line number.
 * @returns {Promise<[boolean, string]>}        Returns a tuple with the result of the parsing and the cleaned line content.
 */
async function parseSource(lineContent, line) {
  const parserSource = parserMapping.map.base.source;
  const source = lineContent.match(/^Source\s+(.*)/)[1].trim();
  const result = await parserSource.parse(source, line);

  if (result) lineContent = sbcUtils.cleanLine(source, lineContent);

  return [result, lineContent];
}

/**
 * Parses the XP of the statblock.
 *
 * @param {sbcTypes.ProcessData} processData    Process data object.
 * @param {string} lineContent                  Content of the line.
 * @param {number} line                         Line number.
 * @returns {Promise<[boolean, string]>}        Returns a tuple with the result of the parsing and the cleaned line content.
 */
async function parseXP(processData, lineContent, line) {
  const parserXP = parserMapping.map.base.xp;
  const xp =
    lineContent
      .match(/^XP\s+([\d,.]*)/)?.[1]
      .replace(/\.,/g, "")
      .trim() ?? "0";
  processData.notes.base.xp = xp;

  // We just save the xp into our notes, as Foundry calculates them automatically
  const result = await parserXP.parse(xp, line);

  if (result === true) lineContent = sbcUtils.cleanLine(lineContent.match(/^(XP\s+[\d,.]*)/)[1], lineContent);

  return [result, lineContent];
}

/**
 * Parses the Gender of the statblock.
 *
 * @param {string} lineContent              Content of the line.
 * @param {number} line                     Line number.
 * @returns {Promise<[boolean, string]>}    Returns a tuple with the result of the parsing and the cleaned line content.
 */
async function parseGender(lineContent, line) {
  const gender = lineContent.match(genderRegex())[0];
  const parserGender = parserMapping.map.base.gender;
  const result = await parserGender.parse(gender, line);

  if (result) lineContent = sbcUtils.cleanLine(gender, lineContent);

  return [result, lineContent];
}

/**
 * Parses the Race of the statblock.
 *
 * @param {string} lineContent                  Content of the line.
 * @param {number} line                         Line number.
 * @returns {Promise<[boolean, string]>}        Returns a tuple with the result of the parsing and the cleaned line content.
 */
async function parseRace(lineContent, line) {
  const parserRace = parserMapping.map.base.race;

  // Check, if it's one of the supported or any other known races
  let race =
    sbcUtils
      .haystackRegexSearch(
        window.SBC.config.races.map((term) => (term += "\\b")),
        lineContent
      )
      ?.replace("\\b", "") ||
    sbcUtils
      .haystackRegexSearch(
        OtherRaces.map((term) => (term += "\\b")),
        lineContent
      )
      ?.replace("\\b", "") ||
    false;

  // Return false if the race wasn't found, or it was found inside parentheses
  if (!race) return [false, lineContent];
  if (
    new RegExp(`\\(${race.toLowerCase()}|,\\s*${race.toLowerCase()}\\s*,|${race.toLowerCase()}\\)`).test(
      lineContent.toLowerCase()
    )
  )
    return [false, lineContent];

  return [await parserRace.parse(race, line), sbcUtils.cleanLine(race, lineContent)];
}

/**
 * Parses the Age of the statblock.
 *
 * @param {string} lineContent                  Content of the line.
 * @param {number} line                         Line number.
 * @returns {Promise<[boolean, string]>}        Returns a tuple with the result of the parsing and the cleaned line content.
 */
async function parseAge(lineContent, line) {
  const age = sbcUtils.haystackRegexSearch(["Middle[- ]Aged?", "Old", "Venerable"], lineContent) || "";
  const parserAge = parserMapping.map.base.age;
  const result = await parserAge.parse(age, line);

  if (result) lineContent = sbcUtils.cleanLine(age, lineContent);

  return [result, lineContent];
}

/**
 * Parses the Classes of the statblock.
 *
 * @param {string} lineContent                  Content of the line.
 * @param {number} line                         Line number.
 * @returns {Promise.<boolean>}                 Returns the result of the parsing.
 */
async function parseClasses(lineContent, line) {
  const classMatch = lineContent.match(classesRegex());
  if (classMatch) {
    const classes = lineContent.slice(lineContent.indexOf(classMatch[0]));

    const parserClasses = parserMapping.map.base.classes;
    return await parserClasses.parse(classes, line);
  }
  return true;
}

/**
 * Parses the Alignment of the statblock.
 *
 * @param {sbcTypes.ProcessData} processData    Process data object.
 * @param {string} lineContent                  Content of the line.
 * @param {number} line                         Line number.
 * @returns {Promise<[boolean, string]>}        Returns a tuple with the result of the parsing and the cleaned line content.
 */
async function parseAlignment(processData, lineContent, line) {
  const parserAlignment = parserMapping.map.base.alignment;
  const alignment = lineContent.match(alignmentRegex())[1].trim();
  processData.notes.base.alignment = alignment;
  const result = await parserAlignment.parse(alignment.replace(/\bN\b/, "TN").toLowerCase(), line);

  if (result) lineContent = sbcUtils.cleanLine(alignment, lineContent);

  return [result, lineContent];
}

/**
 * Parses the Size and Space of the statblock.
 *
 * @param {sbcTypes.ProcessData} processData    Process data object.
 * @param {string} lineContent                  Content of the line.
 * @param {number} line                         Line number.
 * @returns {Promise.<object>}                  Returns an object with the parsed data.
 */
async function parseSizeAndSpace(processData, lineContent, line) {
  const parserSize = parserMapping.map.base.size;
  const size = lineContent.match(sizeRegex())[0].trim();
  processData.notes.base.size = size;
  const actorSize = sbcUtils.getKeyByValue(pf1.config.actorSizes, size);

  // Values derived from Size
  const parserSpace = parserMapping.map.base.space;
  const parserScale = parserMapping.map.base.scale;

  const space = pf1.config.tokenSizes[actorSize].w;
  const scale = pf1.config.tokenSizes[actorSize].scale;

  switch (actorSize) {
    case "dim":
    case "fine":
    case "tiny":
      await window.SBC.actor.update({
        "system.skills.swm.ability": "dex",
        "system.skills.clm.ability": "dex",
        "system.attributes.cmbAbility": "dex",
      });
      break;

    default:
      break;
  }

  return {
    size: await parserSize.parse(actorSize, line),
    space: await parserSpace.parse(space, line),
    scale: await parserScale.parse(scale, line),
  };
}

/**
 * Parses the Creature Type of the statblock.
 *
 * @param {string} lineContent                  Content of the line.
 * @param {number} line                         Line number.
 * @returns {Promise<[boolean, string]>}        Returns a tuple with the result of the parsing and the cleaned line content.
 */
async function parseCreatureType(lineContent, line) {
  const creatureType = lineContent.match(creatureTypeAndSizeRegex())[1];
  const parserCreatureType = parserMapping.map.base.creatureType;

  const result = await parserCreatureType.parse(creatureType, line);

  if (result) lineContent = sbcUtils.cleanLine(creatureType, lineContent);

  return [result, lineContent];
}

/**
 * Parses the Initiative of the statblock.
 *
 * @param {sbcTypes.ProcessData} processData    Process data object.
 * @param {string} lineContent                  Content of the line.
 * @param {number} line                         Line number.
 * @returns {Promise<[boolean, string]>}        Returns a tuple with the result of the parsing and the cleaned line content.
 */
async function parseInit(processData, lineContent, line) {
  const parserInit = parserMapping.map.base.init;
  const init = lineContent.match(/Init\s*([+-]?\d+)/)[1].trim();
  processData.characterData.conversionValidation.attributes["init"] = +init;
  const result = await parserInit.parse(+init, line);

  if (result) lineContent = sbcUtils.cleanLine(init, lineContent);

  return [result, lineContent];
}

/**
 * Parses the Senses of the statblock.
 *
 * @param {string} lineContent                  Content of the line.
 * @param {number} line                         Line number.
 * @returns {Promise<[boolean, string]>}        Returns a tuple with the result of the parsing and the cleaned line content.
 */
async function parseSenses(lineContent, line) {
  const parserSenses = parserMapping.map.base.senses;
  const senses = lineContent.match(/\bSenses\b\s*(.*?)(?:\n|$|\s*Aura)/gim)[0].replace(/\bSenses\b\s*|\s*Aura\b/g, "");
  const result = await parserSenses.parse(senses, line);

  if (result) lineContent = sbcUtils.cleanLine(senses, lineContent);

  return [result, lineContent];
}

/**
 * Parses the Aura of the statblock.
 *
 * @returns {Promise<boolean>}        Returns the result of the parsing.
 */
export async function parseAuras() {
  if (!aura.content) return true;

  const parserAura = parserMapping.map.base.aura;
  const auraValue = aura.content.match(/\bAura\b\s*(.*)/gim)[0].replace(/\s*Aura\b/g, "");
  const result = await parserAura.parse(auraValue, aura.line);

  return result;
}
