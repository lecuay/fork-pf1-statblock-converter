import { sbcUtils } from "./sbcUtils.js";
import { sbcError } from "./sbcError.js";
import { parserMapping } from "./Parsers/parser-mapping.js";
import { sbcValidation } from "./sbcValidation.js";
import { Metamagic } from "./Content/index.js";

// eslint-disable-next-line no-unused-vars
import * as sbcTypes from "./sbcTypes.js";

import { parseBase, parseAuras } from "./Process/base-data.js";
import { parseDefense } from "./Process/defense-data.js";
import { parseDescription } from "./Process/description-data.js";
import { parseEcology } from "./Process/ecology-data.js";
import { parseOffense } from "./Process/offense-data.js";
import { parseSpecialAbilities } from "./Process/special-ability-data.js";
import { parseStatistics, parseDelayedSkills } from "./Process/statistics-data.js";
import { parseTactics } from "./Process/tactics-data.js";
import { SimpleParser } from "./Parsers/Universal/index.js";

/* ------------------------------------ */
/* sbcParser    						            */
/* ------------------------------------ */

export class sbcParser {
  /* ------------------------------------ */
  /* Prepare the input                    */
  /* ------------------------------------ */

  /**
   * Modifies the Statblock input in order to have it as an array per line break.
   *
   * @param {sbcTypes.ProcessData} processData The processed data to be prepared.
   */
  static async prepareInput(processData) {
    /** @type {sbcTypes.SBC} */
    const sbcInstance = window.SBC;
    sbcInstance.isImporting = true;
    window.SBC.settings.getSetting("debug") && console.group("sbc-pf1 | PREPARING INPUT");
    if (processData.input.text === "") {
      processData.errors.push(new sbcError(sbcError.ERRORLEVELS.FATAL, "Prepare", "Given input is empty."));

      throw "Given input is empty.";
    }

    try {
      // Check if David's "Roll Bonuses PF1" module is installed and active
      if (game.modules.get("roll-bonuses-pf1")?.active) {
        sbcUtils.log("Roll Bonuses PF1 module is active");
        processData.flags["rollBonusesPF1"] = true;
      }

      // Initial Clean-up of input
      $("#sbcProgressBar").css("width", "5%");

      const sourceScrubbing = new RegExp("(" + sbcInstance.config.sources.join("\\b|") + ")", "gm");
      // Replace different dash-glyphs with the minus-glyph
      processData.input.prepared.data = processData.input.text
        .replace(/[–—−]/gm, "-")
        // Remove weird multiplication signs
        .replaceAll("×", "x")
        // Remove double commas
        .replace(/,+/gm, ",")
        // Replace real fractions with readable characters (½ ⅓ ¼ ⅕ ⅙ ⅛)
        .replaceAll("½", "1/2")
        .replaceAll("⅓", "1/3")
        .replaceAll("¼", "1/4")
        .replaceAll("⅕", "1/5")
        .replaceAll("⅙", "1/6")
        .replaceAll("⅛", "1/8")
        // Remove Source Superscript (e.g. ^APG, ^UE)
        .replace(sourceScrubbing, "")
        // Remove Bestiary Source Superscript (e.g. ^B)
        .replace(/([a-z])B\b/gm, "$1")
        // Remove Mythic and Mythic Adventures superscript ("M, MA")
        .replace(/([a-z])M\b,\s*MA/gm, "$1M")

        // Fix quotes
        // eslint-disable-next-line quotes
        .replaceAll("“", '"')
        // eslint-disable-next-line quotes
        .replaceAll("”", '"')
        .replaceAll("‘", "'")
        .replaceAll("’", "'")

        // Replace ligatures
        .replaceAll("ﬂ", "fl")
        .replaceAll("ﬁ", "fi")
        .replaceAll("ﬀ", "ff")
        .replaceAll("ﬃ", "ffi")
        .replaceAll("ﬄ", "ffl")
        .replaceAll("ﬆ", "st")

        // Remove lines that only consist of 20 dashes (Hero Lab Online)
        .replace(/^-{20}$/gm, "")

        // Fix miscellaneous formattings
        .replace(/\*, (D,?)/g, "$1")

        // Separate the input into separate lines and put them into an array,
        // so that we can place highlights on specific lines when for
        // example an error occurs

        .split(/\n/g);

      // Process each line for a line starter.
      // If a line starter isn't found, then it's a continuation of the previous line.
      // If a line starter is found, then it's a new line.
      sbcUtils.log("Before: ", processData.input.prepared.data);
      let result = [];
      const lineCategoryPattern = new RegExp("^(" + sbcInstance.config.lineCategories.join("|") + ")", "i");
      const lineStarterPattern = new RegExp("^(" + sbcInstance.config.lineStarts.join("|") + ")(?!\\))", "i");

      processData.input.prepared.data.forEach((line, index) => {
        // Capitalize the line if it's a category
        if (lineCategoryPattern.test(line)) {
          console.log(line, lineCategoryPattern);
          line = line.toLowerCase().capitalize();
        }

        // let caseNum = 0;
        if (line && sbcInstance.config.lineStarts.some((starter) => line.startsWith(starter))) {
          // The line starts with a string from the other array, push it as a new line
          // caseNum = 1;
          result.push(line);
        } else if (
          index > 0 &&
          lineStarterPattern.test(result[result.length - 1]) &&
          !lineStarterPattern.test(line) &&
          !/^Description/i.test(result[result.length - 1])
        ) {
          // The line doesn't start with a string from the other array, append it to the last line
          // caseNum = 2;
          result[result.length - 1] += " " + line;
        } else {
          // The first line doesn't start with a string from the other array, push it as a new line
          // caseNum = 3;
          result.push(line);
        }
      });

      // Move gear in treasure section to statistics
      let ecologyLine = 0;
      for (let i = 0; i < result.length; i++) {
        const currentLine = result[i];
        if (currentLine.toLowerCase().startsWith("ecology")) {
          ecologyLine = i;
          continue;
        }

        if (ecologyLine && currentLine.toLowerCase().startsWith("treasure npc gear")) {
          const newGearLine =
            "Gear " +
            currentLine
              .replace(/^Treasure\s+NPC gear\s*\((.*)\)\s*$/i, "$1")
              .replaceAll("[", "(")
              .replaceAll("]", ")");
          result.splice(i, 1);
          result.splice(ecologyLine, 0, newGearLine);
          break;
        }
      }

      // Filter out lines that start with an asterisk - These are comments to check books
      result = result.filter((line) => !line.startsWith("*"));

      processData.input.prepared.data = result;
      sbcUtils.log("After: ", processData.input.prepared.data);

      // Check if the input is prepared successfully and set the input field to the prepared data
      processData.input.prepared.success = true;
      processData.input.text = processData.input.prepared.data.join("\n");
    } catch (errorMessage) {
      let error = new sbcError(sbcError.ERRORLEVELS.FATAL, "Prepare", errorMessage);
      processData.errors.push(error);
      processData.input.prepared.success = false;
      throw errorMessage;
    }

    window.SBC.settings.getSetting("debug") && console.groupEnd();
  }

  /* ------------------------------------ */
  /* Parse the input                      */
  /* ------------------------------------ */
  /**
   * Parse the input data and create the actor data.
   *
   * @param {sbcTypes.ProcessData} processData  The process data object to use for preparing
   */
  static async parseInput(processData) {
    window.SBC.settings.getSetting("debug") && console.group("sbc-pf1 | PARSING INPUT");
    let characterData = processData.characterData;
    let text = processData.input.text;

    // Check if there is stuff to parse and a temporary actor to hold the data
    if (characterData == null || !text) {
      if (characterData == null)
        processData.errors.push(new sbcError(sbcError.ERRORLEVELS.FATAL, "Input", "No valid characterData found"));
      if (!processData.input.text)
        processData.errors.push(new sbcError(sbcError.ERRORLEVELS.FATAL, "Parse", "Not enough input found to parse"));

      processData.input.prepared.success = false;
    }

    if (processData.input.prepared.success) {
      const actorType = window.SBC.actor.type;
      // DO STUFF WITH THE PARSED INPUT
      // parse the different blocks of content
      try {
        sbcParser.updateProgressBar(processData, "Preparation", "Clean-up");
        // const availableCategories = Object.keys(parserMapping.map);
        const availableCategories = parserMapping.map.types[actorType];
        console.log(availableCategories);

        /* ------------------------------------ */
        /* The input was prepared and is        */
        /* currently in the form of an array    */
        /* which consists of one entry per line */
        /* ------------------------------------ */

        // Split the input data via the category names of our mapping
        // base (no keyword, so use everything up to the defense keyword)
        // defense
        // offense
        // tactics
        // statistics
        // ecology
        // special abilities
        // description

        // Get the index position of our keywords/categories
        // Bonus, filter for the categories found in the statblock
        let categoryIndexPositions = {};
        for (let i = 1; i < availableCategories.length; i++) {
          const category = availableCategories[i];
          const categoryPattern = new RegExp("^\\b" + category + "\\b\\s*(?:\\r?\\n|$)", "i");

          processData.input.prepared.data.filter(function (item, index) {
            if (categoryPattern.test(item) && categoryIndexPositions[category] == null) {
              categoryIndexPositions[category] = index;
            }
          });
        }

        // Check, if any categories could be found
        let foundCategories = Object.keys(categoryIndexPositions);
        let foundCategoriesString = foundCategories.join(", ").capitalize();
        foundCategories.unshift("base");
        let parsedCategories = {};

        processData.foundCategories = foundCategories.length;
        console.log("Found Categories: ", foundCategories, foundCategories.length);
        if (foundCategories.length !== 0) {
          // Check, if the needed categories are there
          let neededCategories = parserMapping.getNeededCategories(actorType);
          let foundAllNeededCategories = neededCategories.length
            ? neededCategories.every((i) => foundCategories.includes(i))
            : true;

          if (foundAllNeededCategories) {
            // Split the input into chunks for the found categories
            // via the index positions found earlier
            // and send these chunks off to the correct parser in sbcParsers.js

            /** @type {{string: Array<string>}} */
            let dataChunks = {
              base: [],
              description: [],
            };

            /** @type {{string: number}} */
            let startLines = {
              base: 0,
              description: 0,
            };

            if (["npc", "character"].includes(actorType)) {
              dataChunks = foundry.utils.mergeObject(dataChunks, {
                defense: [],
                offense: [],
                statistics: [],
                tactics: [],
                ecology: [],
                specialAbilities: [],
              });

              startLines = foundry.utils.mergeObject(startLines, {
                defense: 0,
                offense: 0,
                statistics: 0,
                tactics: 0,
                ecology: 0,
                specialAbilities: 0,
              });
            }

            let lastLine = 0;

            // put the found data in the dataChunks
            for (let i = 0; i < foundCategories.length; i++) {
              let category = foundCategories[i];

              let startLine = lastLine;
              startLines[category] = startLine;
              let stopLine = categoryIndexPositions[foundCategories[i + 1]];

              if (i === foundCategories.length - 1) {
                dataChunks[category] = processData.input.prepared.data.slice(startLine);
              } else {
                dataChunks[category] = processData.input.prepared.data.slice(startLine, stopLine);
              }

              lastLine = stopLine;
            }

            // Parse the found categories into a proper order for processing
            let orderedFoundCategories = foundCategories;

            // If we're dealing with NPCs or PCs, rearrange the foundCategories
            // so that statistics gets parsed before defense and offense, and
            // specialAbilities gets parsed before statistics, if it exists
            if (["npc", "character"].includes(actorType)) {
              orderedFoundCategories.splice(foundCategories.indexOf("statistics"), 1);
              orderedFoundCategories.splice(1, 0, "statistics");

              // Rearrange the foundCategories so that Special Abilities get parsed before base data
              if (foundCategories.includes("special abilities")) {
                orderedFoundCategories.splice(foundCategories.indexOf("special abilities"), 1);
                orderedFoundCategories.splice(0, 0, "special abilities");
              }
            }

            // Now we can parse each category one-by-one
            for (let i = 0; i < orderedFoundCategories.length; i++) {
              let category = orderedFoundCategories[i];
              sbcParser.updateProgressBar(processData, "Parsing", category, orderedFoundCategories.length, i + 1);
              parsedCategories[category] = await sbcParser.parseCategories(
                processData,
                category,
                dataChunks[category],
                startLines[category]
              );
            }

            // Do some necessary post-processing for NPCs and PCs
            if (["npc", "character"].includes(actorType)) {
              // After parsing all available subCategories, create embedded entities
              sbcParser.updateProgressBar(processData, "Buffs", "Looking for Auras and Buffs");
              await parseAuras();
              await createBuffs();

              // After parsing all available subCategories, check the flags set on the way
              sbcParser.updateProgressBar(processData, "Flags", "Checking if Special Flags were set");
              await checkFlags(processData);

              // After all that, process the misc notes before we might do validation
              await processMiscNotes(processData);

              await sbcValidation.conversionValidation(processData);
            }

            // Create the notes section composed of the statblock and the raw input
            sbcParser.updateProgressBar(processData, "Preview", "Generating Preview");
            await generateNotesSection(processData);

            // SET THIS TO TRUE WHEN ALL CATEGORIES ARE PARSED SUCCESSFULLY (MORE OR LESS)
            processData.input.prepared.success = true;
            sbcParser.updateProgressBar(processData, "Ready", "Actor is ready");
          } else {
            let errorMessage = `Failed to find enough keywords to parse the input.<br>
                                        Found Keywords: ${foundCategoriesString}<br>
                                        Needed Keywords: Defense, Offense, Statistics<br>
                                        Optional Keywords: Special Abilities, Ecology, Tactics, Description`;
            let error = new sbcError(sbcError.ERRORLEVELS.FATAL, "Parse", errorMessage);
            processData.errors.push(error);
            processData.input.prepared.success = false;
            sbcParser.updateProgressBar(processData, "Error", "An Error occurred during parsing");
          }
        } else {
          console.log("No categories found");
          let errorMessage = `Failed to find any keywords to parse the input.<br>
                                        Needed Keywords: Defense, Offense, Statistics<br>
                                        Optional Keywords: Special Abilities, Ecology, Tactics, Description`;
          let error = new sbcError(sbcError.ERRORLEVELS.FATAL, "Parse", errorMessage);
          processData.errors.push(error);
          processData.input.prepared.success = false;
          sbcParser.updateProgressBar(processData, "Error", "An Error occurred during parsing");
        }
      } catch (e) {
        let errorMessage = "parseInput() failed with an unspecified error. Sorry!";
        let error = new sbcError(sbcError.ERRORLEVELS.FATAL, "Parse", errorMessage);
        processData.errors.push(error);
        processData.input.prepared.success = false;
        sbcParser.updateProgressBar(processData, "Error", "An Error occurred during parsing");
        throw e;
      }
    } else {
      let errorMessage = "parseInput() failed as the input could not be prepared successfully";
      let error = new sbcError(sbcError.ERRORLEVELS.FATAL, "Parse", errorMessage);
      processData.errors.push(error);
      processData.input.prepared.success = false;
      sbcParser.updateProgressBar(processData, "Error", "An Error occurred during parsing");
    }

    window.SBC.settings.getSetting("debug") && console.groupEnd();
  }

  /**
   * Try to find a matching parser for a given category
   *
   * @param {sbcTypes.ProcessData} processData The processed data to be prepared.
   * @param {"base"|"defense"|"offense"|"statistics"|"ecology"|"special abilities"|"description"|"tactics"} category The part of the statblock we are parsing.
   * @param {Array.<string>} data Array of separated data contained.
   * @param {number} startLine Index from where this part of the statblock starts.
   */
  static async parseCategories(processData, category, data, startLine) {
    switch (category) {
      case "base":
        await parseBase(processData, data, startLine);
        break;

      case "defense":
        await parseDefense(processData, data, startLine);

        window.SBC.settings.getSetting("debug") && console.groupCollapsed("sbc-pf1 | >> PARSING SKILL DATA");
        await parseDelayedSkills();
        window.SBC.settings.getSetting("debug") && console.groupEnd();
        break;

      case "offense":
        await parseOffense(processData, data, startLine);
        break;

      case "statistics":
        await parseStatistics(processData, data, startLine);
        break;

      case "tactics":
        await parseTactics(processData, data, startLine);
        break;

      case "ecology":
        await parseEcology(processData, data, startLine);
        break;

      case "special abilities":
        await parseSpecialAbilities(processData, data, startLine);
        break;

      case "description":
        await parseDescription(processData, data, startLine);
        break;

      default:
        processData.errors.push(
          new sbcError(sbcError.ERRORLEVELS.ERROR, "Parse/Categories", `No Parser found for category: ${category}`)
        );
        processData.input.prepared.success = false;
        return;
    }

    processData.parsedCategories++;
  }

  /**
   * Update the progress bar with the current progress.
   *
   * @param {sbcTypes.ProcessData} processData    The processed data to be prepared.
   * @param {string} process                      The process that is currently running.
   * @param {string} subProcess                   The sub process that is currently running.
   * @param {number} total                        The total number of steps to be taken.
   * @param {number} step                         The current step that is being processed.
   * @returns {void}
   */
  static async updateProgressBar(processData, process = "", subProcess = "", total = 1, step = 1) {
    let newWidth = 0;
    let progressBarText = subProcess;
    const increment = 100 / total;
    processData.progressBar.class = process.toLowerCase();

    switch (process.toLowerCase()) {
      case "preparation":
        newWidth = 10;
        progressBarText = process + ": " + subProcess;
        break;

      case "parsing":
        newWidth = Math.floor(10 + 0.6 * increment * step);
        progressBarText = process + ": " + subProcess;
        break;

      case "buffs":
        newWidth = 90;
        break;

      case "flags":
        newWidth = 95;
        break;

      case "preview":
        newWidth = 99;
        break;

      case "actor":
        newWidth = 100;
        break;

      case "error":
        newWidth = 100;
        break;

      default:
        break;
    }

    processData.progressBar.width = newWidth;
    processData.progressBar.text = progressBarText;

    /** @type {sbcTypes.SBC} */
    const sbcInstance = window.SBC;
    await sbcInstance.app.render();
  }
}

/**
 * Convenience helper, tries to parse the number to integer, if it is NaN, will return 0 instead.
 *
 * @param {string} value The value to parse
 * @returns {number} The parsed integer
 */
export const parseInteger = (value) => {
  let p = parseInt(value);
  return isNaN(p) ? 0 : p;
};

/**
 * Convenience helper, returns an array with the base text and the sub text if found. Format: base text (sub text)
 *
 * @param {string} value The value to parse
 * @returns {Array<string>} The parsed text
 */
export const parseSubtext = (value) => {
  return sbcUtils.parseSubtext(value);
};

export const parseValueToPath = async (obj, path, value) => {
  const parts = path.split(".");
  let curr = obj;
  for (let i = 0; i < parts.length - 1; i++) {
    curr = curr[parts[i]] || {};
  }
  curr[parts[parts.length - 1]] = value;
};

/**
 * Wrapper for {@link Actor#update}.
 *
 * @param {Actor} obj The actor to be updated.
 * @param {string} path Where to perform the update.
 * @param {any} value Value to substitute.
 * @returns {Promise.<Actor>} The update document instance.
 */
export const parseValueToDocPath = async (obj, path, value) => {
  return obj.update({ [path]: value });
};

/* ------------------------------------ */
/* Check for flags and parse            */
/* ------------------------------------ */

// Check if some special flags were set during parsing
/**
 * Check if some special flags were set during parsing
 *
 * @param {sbcTypes.ProcessData} processData  The process data object.
 * @param {string} flagToProcess              The flag to process.
 * @returns {Promise<void>}
 * @async
 */
export async function checkFlags(processData, flagToProcess = "all") {
  sbcUtils.log("Flags set during the conversion process");
  sbcUtils.log(processData.flags);

  let parsedFlags = [];
  const flagsToParse = flagToProcess === "all" ? Object.keys(processData.flags) : [flagToProcess];

  for (const flag of flagsToParse) {
    // Fix for set abilities persisting even when flags are reset
    if (processData.flags[flag]) {
      let fields = [];
      let value = null;
      let supportedTypes = "string";
      let flagNeedsAdditionalParsing = false;

      switch (flag) {
        case "isUndead":
          // When its an undead, use Cha for HP and Save Calculation
          fields = ["system.attributes.hpAbility", "system.attributes.savingThrows.fort.ability"];
          if (processData.flags[flag] === true) {
            value = "cha";
          } else {
            value = "con";
          }
          flagNeedsAdditionalParsing = true;
          break;

        case "noStr":
          fields = ["system.abilities.str.base", "system.abilities.str.value", "system.abilities.str.total"];
          flagNeedsAdditionalParsing = true;
          value = "NaN";
          break;

        case "noDex":
          fields = ["system.abilities.dex.base", "system.abilities.dex.value", "system.abilities.dex.total"];
          flagNeedsAdditionalParsing = true;
          value = "NaN";
          break;

        case "noCon":
          fields = ["system.abilities.con.base", "system.abilities.con.value", "system.abilities.con.total"];
          flagNeedsAdditionalParsing = true;
          value = "NaN";
          break;

        case "noInt":
          fields = ["system.abilities.int.base", "system.abilities.int.value", "system.abilities.int.total"];
          flagNeedsAdditionalParsing = true;
          value = "NaN";
          break;

        case "noWis":
          fields = ["system.abilities.wis.base", "system.abilities.wis.value", "system.abilities.wis.total"];
          flagNeedsAdditionalParsing = true;
          value = "NaN";
          break;

        case "noCha":
          fields = ["system.abilities.cha.base", "system.abilities.cha.value", "system.abilities.cha.total"];
          flagNeedsAdditionalParsing = true;
          value = "NaN";
          break;

        default:
          break;
      }

      if (flagNeedsAdditionalParsing) {
        /** @type {sbcTypes.SBC} */
        const sbcInstance = window.SBC;
        let parser = new SimpleParser(sbcInstance.app, fields, supportedTypes);
        parsedFlags[flag] = await parser.parse(value, -1);
      }
    }
  }
}

/**
 * Migrates the item data and creates the item on the actor
 *
 * @param {Item} itemData               The item data to be created.
 * @returns {Promise<[boolean, Item]>}  Returns a boolean and the created item
 */
export async function createItem(itemData) {
  /** @type {sbcTypes.SBC} */
  const sbcInstance = window.SBC;

  try {
    const updateData = pf1.migrations.migrateItemData(itemData.toObject());
    if (!foundry.utils.isEmpty(updateData)) {
      await itemData.updateSource(updateData);
    }

    // Drop supplemental items. If they are required, they will be in the statblock and imported elsewhere
    await itemData.updateSource({ "system.links.supplements": [] });

    const item = await sbcInstance.actor.createEmbeddedDocuments("Item", [itemData.toObject()]);
    return [true, item[0]];
  } catch (err) {
    window.SBC.settings.getSetting("debug") && console.error(err);
    let errorMessage = `Failed to create embedded item ${itemData.name}.`;
    let error = new sbcError(sbcError.ERRORLEVELS.ERROR, "Parse", errorMessage);
    sbcInstance.app.processData.errors.push(error);
    sbcInstance.app.processData.input.prepared.success = false;
    return [false, null];
  }
}

/**
 * Looks through the actor for all items
 * and creates buffs where possible
 *
 * @returns {Promise<void>}
 */
export async function createBuffs() {
  /** @type {sbcTypes.SBC} */
  const sbcInstance = window.SBC;
  const compendiumOps = sbcInstance.compendiumSearch;
  try {
    window.SBC.settings.getSetting("debug").debug && console.groupCollapsed("sbc-pf1 | Finding Buffs");
    let items = window.SBC.actor.items.filter((i) => ["feat", "consumable", "spell"].includes(i.type));

    for (let buffableItem of items) {
      let name = buffableItem.name;
      let altName = name.replace(/(constant|weekly|monthly|yearly):/gi, "").trim();
      let altName2 = name.replace(/^(?:Potion|Wand) of\s+/gi, "").trim();

      console.log(`Checking ${name}`, buffableItem);

      let searchEntity = {
        search: new Set([name]),
        type: "buff",
        item: name,
      };

      const nameWords = name
        .split(" ")
        .slice(0, 2)
        .map((o) => o.toLowerCase());
      const metamagicFeat = Metamagic.find((m) => {
        const words = m.split(" ").map((o) => o.toLowerCase());
        return words.every((word) => nameWords.includes(word));
      });
      if (metamagicFeat) {
        searchEntity.search.add(altName.replace(new RegExp(`${metamagicFeat}`, "i"), "").trim());
        searchEntity.search.add(altName2.replace(new RegExp(`${metamagicFeat}`, "i"), "").trim());
      } else {
        searchEntity.search.add(altName);
        searchEntity.search.add(altName2);
      }

      let entity = await compendiumOps.findEntityInCompendia(searchEntity, {
        packs: ["pf1.commonbuffs"],
        itemType: "buff",
        classes: sbcInstance.app.processData.characterData.classes,
      });
      if (!entity) entity = game.items.getName(searchEntity.name);
      if (!entity) entity = game.items.getName(searchEntity.altName);

      if (entity) {
        console.log(`Found ${name}.`, entity);

        let casterLevel = 0;
        switch (buffableItem.type) {
          case "spell":
            {
              let spellBookKey = buffableItem.system.spellbook;
              console.log(`Spellbook key for ${name}: ${spellBookKey}`);
              let spellBook = pf1.utils.deepClone(buffableItem.spellbook);
              console.log(`Spellbook data for ${name}: `, spellBook, spellBook?.cl);
              let casterLevel = sbcInstance.actor.system.attributes.spells.spellbooks[spellBookKey]?.cl.total ?? -1;
              // let casterLevel = buffableItem.casterLevel ?? -1;
              // let casterLevel = buffableItem.getRollData().cl ?? -1;
              console.log(`Caster level for ${buffableItem.name}: ${casterLevel}`);
            }
            break;

          case "consumable":
            casterLevel = buffableItem.system.cl || 0;
            break;
        }

        if (casterLevel > 0) entity.updateSource({ "system.level": casterLevel });

        console.log(`Creating buff for ${name}.`);
        const [_result, buff] = await createItem(entity);
        // let buff = sbcInstance.actor.itemTypes.buff.find((i) => i.name === entity.name);

        if (/^Constant:/i.test(name)) {
          await buff.update({ "system.duration.value": "" });
          await buff.setActive(true);
        }
      }
    }
    window.SBC.settings.getSetting("debug") && console.groupEnd();
  } catch (err) {
    window.SBC.settings.getSetting("debug") && console.error(err);
  }
}

/**
 * Generates the notes section for the actor
 *
 * @param {sbcTypes.ProcessData} processData    The processed data to be prepared.
 * @returns {Promise<void>}
 */
export async function generateNotesSection(processData) {
  let preview = await renderTemplate("modules/pf1-statblock-converter/templates/sbcPreview.hbs", {
    actor: window.SBC.actor || {},
    notes: processData.notes,
    flags: processData.flags,
  });

  let d = new Date();

  let sbcInfo = await renderTemplate("modules/pf1-statblock-converter/templates/sbcNotes.hbs", {
    version: window.SBC.config.modData.version,
    date: d.toLocaleString(game.settings?.get("core", "language") || "en", {
      month: "long",
      day: "numeric",
      year: "numeric",
    }),
    preview: preview,
    rawInput: processData.input.text,
  });

  // WRITE EVERYTHING TO THE NOTES
  await window.SBC.actor?.update({
    "system.details.notes.value": sbcInfo,
  });
}

/**
 * Processes the miscellaneous notes
 *
 * @param {sbcTypes.ProcessData} processData  The processed data to be prepared.
 * @returns {Promise<void>}
 */
export async function processMiscNotes(processData) {
  let miscNotes = processData.miscNotes;
  let actor = window.SBC.actor;
  let contextNotes = [];
  let changes = [];
  let descData = `<p>This feature was created by <strong>SBC</strong> to collect any notes that don't have a source on the actor,
                such as saving throw details or a bonus to CMD from extra legs. It may also be used to record a natural armor bonus,
                spell resistance bonus, or spellcasting notes.</p>`;
  let miscNotesExist = false;

  if (miscNotes.acNotes.length > 0) {
    miscNotesExist = true;
    contextNotes.push({
      target: "ac",
      text: miscNotes.acNotes.join(", "),
    });

    descData += `<p><strong>Armor Class Notes:</strong></p><p>${miscNotes.acNotes.join(", ")}</p>`;
  }

  if (miscNotes.srNotes) {
    miscNotesExist = true;
    contextNotes.push({
      target: "sr",
      text: miscNotes.srNotes,
    });

    descData += `<p><strong>Spell Resistance Notes:</strong></p><p>${miscNotes.srNotes}</p>`;
  }

  if (miscNotes.cmbNotes) {
    miscNotesExist = true;
    contextNotes.push({
      target: "cmb",
      text: miscNotes.cmbNotes,
    });

    descData += `<p><strong>CMB Notes:</strong></p><p>${miscNotes.cmbNotes}</p>`;
  }

  if (miscNotes.cmdNotes) {
    miscNotesExist = true;
    contextNotes.push({
      target: "cmd",
      text: miscNotes.cmdNotes,
    });

    descData += `<p><strong>CMD Notes:</strong></p><p>${miscNotes.cmdNotes}</p>`;
  }

  if (miscNotes.saveNotes) {
    miscNotesExist = true;
    contextNotes.push({
      target: "allSavingThrows",
      text: miscNotes.saveNotes,
    });

    descData += `<p><strong>Saving Throw Notes:</strong></p><p>${miscNotes.saveNotes}</p>`;
  }

  if (miscNotes.sr > 0) {
    miscNotesExist = true;
    changes.push(
      await new pf1.components.ItemChange({
        formula: miscNotes.sr.toString(),
        type: "untyped",
        target: "spellResist",
        value: +miscNotes.sr,
      }).toObject()
    );
  }

  if (miscNotes.naturalAC > 0) {
    miscNotesExist = true;
    changes.push(
      (
        await new pf1.components.ItemChange({
          formula: miscNotes.naturalAC.toString(),
          type: "untyped",
          target: "nac",
          value: +miscNotes.naturalAC,
        })
      ).toObject()
    );
  }

  if (!miscNotesExist) return;

  await actor.createEmbeddedDocuments("Item", [
    {
      name: "Miscellaneous Notes",
      type: "feat",
      system: {
        changes: changes,
        contextNotes: contextNotes,
        description: { value: descData },
        subType: "misc",
        tag: "miscellaneousNotes",
      },
      img: "systems/pf1/icons/skills/yellow_36.jpg",
    },
  ]);
}
