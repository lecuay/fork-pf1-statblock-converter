const metamagic = [
  "Apocalyptic",
  "Aquatic",
  "Ascendant",
  "Authoritative",
  "Benthic",
  "Blissful",
  "Bouncing",
  "Brackish",
  "Brisk",
  "Burning",
  "Centered",
  "Cherry Blossom",
  "Coaxing",
  "Concussive",
  "Conditional",
  "Consecrate",
  "Contagious",
  "Contingent",
  "Crypt",
  "Dazing",
  "Delayed",
  "Disruptive",
  "Echoing",
  "Eclipsed",
  "Ectoplasmic",
  "Elemental",
  "Empower",
  "Empowered",
  "Encouraging",
  "Enlarge",
  "Enlarged",
  "Extend",
  "Extended",
  "Familiar",
  "Fearsome",
  "Flaring",
  "Fleeting",
  "Focused",
  "Furious",
  "Heighten",
  "Heightened",
  "Intensified",
  "Intuitive",
  "Jinxed",
  "Latent Curse",
  "Lingering",
  "Logical",
  "Maximize",
  "Maximized",
  "Merciful",
  "Murky",
  "Persistent",
  "Piercing",
  "Quicken",
  "Quickened",
  "Reach",
  "Rime",
  "Scarring",
  "Scouting Summons",
  "Seeking",
  "Selective",
  "Shadow Grasp",
  "Sickening",
  "Silent",
  "Snuffing",
  "Solar",
  "Solid Shadows",
  "Stable",
  "Steam",
  "Still",
  "Stilled",
  "Studied",
  "Stygian",
  "Stylized",
  "Tenacious",
  "Tenebrous",
  "Thanatopic",
  "Threatening Illusion",
  "Threnodic",
  "Thundering",
  "Toppling",
  "Toxic",
  "Traumatic",
  "Trick",
  "Tumultuous",
  "Umbral",
  "Ursurping",
  "Vast",
  "Verdant",
  "Widen",
  "Widened",
  "Yai-Mimic",
];
export default metamagic;
