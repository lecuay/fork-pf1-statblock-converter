const immunities = {
  construct: {
    damage: ["nonlethal"],
    conditions: [
      "mindAffecting",
      "bleed",
      "disease",
      "deathEffects",
      "paralyze",
      "poison",
      "sleep",
      "stun",
      "fatigue",
      "exhausted",
      "energyDrain",
    ],
    custom: [
      "Necromancy Effects",
      "Effects that require a Fort. save",
      "Ability Damage",
      "Ability Drain",
      "Massive Damage",
    ],
  },
  dragon: {
    conditions: ["paralyze", "sleep"],
  },
  ooze: {
    damage: ["precision"],
    condition: ["mindAffecting", "poison", "sleep", "paralyze", "polymorph", "stun"],
    custom: [
      "Gaze Attacks",
      "Visual Effects",
      "Illusions",
      "Attack forms that rely on sight",
      "Flanking",
      "Critical Hits",
    ],
  },
  plant: {
    condition: ["mindAffecting", "paralyze", "polymorph", "poison", "sleep", "stun"],
  },
  undead: {
    damage: ["nonlethal"],
    condition: [
      "mindAffecting",
      "bleed",
      "deathEffects",
      "disease",
      "paralyze",
      "poison",
      "sleep",
      "stun",
      "exhausted",
      "fatigue",
    ],
    custom: [
      "Ability Drain",
      "Energy Drain",
      "Damage to phys. Ability Scores",
      "Effects that require a Fort. save",
      "Massive Damage",
      "Raise Dead",
      "Reincarnate",
    ],
  },
  vermin: {
    condition: ["mindAffecting"],
  },
};
export default immunities;
