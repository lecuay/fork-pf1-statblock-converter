const itemModifications = [
  // Weapon modifications
  "Brutally weighted",
  "Dual-balanced",
  "Jagged hooks?",
  "Razor-Sharp",
  "Serrated Edge",
  "Tactically Adapted",
  "Verstaille Design",

  // Armor modifications
  "Burnished",
  "Deflecting",
  "Double-plated",
  "Jarring",
  "Nimble",
  "Razored",
  "Slumbering",
  "Throwing Shield",
  "Vitalguard",
  "With Armor Spikes?",

  // Other
  "Spiked(?! Chain(?:\b|$)| Gauntlet)",
];

export default itemModifications;
