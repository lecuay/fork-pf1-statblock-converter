const naturalAttacks = {
  hooves: {
    img: "systems/pf1/icons/items/inventory/monster-hoof.jpg",
    isPrimaryAttack: false,
    damageTypes: "B",
  },
  tail: {
    img: "systems/pf1/icons/items/inventory/monster-tail.jpg",
    isPrimaryAttack: false,
    damageTypes: "B",
  },
  other: {
    img: "systems/pf1/icons/items/inventory/monster-cat.jpg",
    isPrimaryAttack: false,
    damageTypes: "P, B or S",
  },
};
export default naturalAttacks;
