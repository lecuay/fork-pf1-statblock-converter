import { sbcUtils } from "../../sbcUtils.js";
import { sbcError } from "../../sbcError.js";
import { NaturalAttacks } from "../../Content/index.js";
import { AbstractParser } from "../abstract-parser.js";
import { createItem } from "../../sbcParser.js";
import {
  materialAddonsRegex,
  materialsBasicRegex,
  materialsNonBasicRegex,
  naturalAttacksRegex,
} from "../../sbcRegex.js";

// eslint-disable-next-line no-unused-vars
import * as sbcTypes from "../../sbcTypes.js";

// Parse Attacks
export default class AttackParser extends AbstractParser {
  /**
   * @inheritDoc
   */
  get parserGroup() {
    return "Parser/Offense";
  }

  /**
   * @inheritDoc
   */
  get parserType() {
    return "attack";
  }

  /**
   * Parses an attack string
   *
   * @param {string} value        The attack string to parse
   * @param {number} line         The line number
   * @param {string} type         The type of attack (mwak, rwak, etc.)
   * @returns {Promise<boolean>}  Always true, unless an error occurs
   */
  async _parse(value, line, type) {
    if (!value) return false;

    // The next task is to re-organize the code below to accomplish the following in order:
    // 1. Get the raw attack name without any "nonlethal", "melee", "range", or "touch"
    // 2. Check if there exists a weapon with the given name.
    // 3. If there is a weapon, check the attack bonus it uses to compare with statblock
    // 4. If there is a weapon, check the enhancement bonus to compare with statblock
    // 5. If there is a weapon, check the damage formula to compare with statblock
    // 6. If there is a weapon, check the damage bonus to compare with statblock
    // 7. If 3-6 are all matching, then check the system setting for whether to make the attack.
    // 8. If any of 3-6 are not matching, then create the attack and modify it to match the statblock
    // 9. If 2-8 don't happen, then proceed with creating the attack manually as normal.

    // [1] Sanitize the input
    let rawInput = value
      .replace(/, or/g, " or")
      .replace(/, \band\b /g, " and ")
      .replace(/\band\b (?![^(]*\)|\()/g, ",");

    // [2] Split it into attackGroups denoted by an "or"
    //     All attacks in an attackGroup can be part of a full attack action
    //     Loop over these attackGroups to handle each separately
    const inputAttackGroups = rawInput.split(/\bor\b/g);
    const attackGroupKeys = Object.keys(inputAttackGroups);
    const hasMultiAttack = window.SBC.actor.itemTypes.feat.find(
      (feat) => feat.name === "Multiattack" || feat.system.tag === "multiAttack"
    );
    const unClass = window.SBC.actor.itemTypes.class.find((classItem) => {
      return (
        classItem.system.tag === "monkUnchained" ||
        classItem.flags.core?.sourceId === "Compendium.pf1.classes.Item.5mQIOIw4hDhlREPo"
      );
    });
    const monkClass = window.SBC.actor.itemTypes.class.find((classItem) => {
      return (
        classItem.system.tag === "monk" ||
        classItem.flags.core?.sourceId === "Compendium.pf1.classes.Item.W8dxwZ1CZiwLKsqr"
      );
    });

    const isUnchainedMonk = unClass ? true : false;
    const flurryClass = unClass ? unClass : monkClass;
    console.log("FS| Unchained Monk", unClass, monkClass, isUnchainedMonk, flurryClass);
    for (let i = 0; i < attackGroupKeys.length; i++) {
      // [3] Split the attacks found in the attackGroup
      //     and loop over each attack separately
      const inputAttackGroup = this.fixSplitGroup(this.splitText(inputAttackGroups[i]));
      console.log("FS| Attack Group", inputAttackGroup, this.splitText(inputAttackGroups[i]));

      // Loop over all attacks in the attackGroup
      for (let index = 0; index < inputAttackGroup.length; index++) {
        console.log(inputAttackGroup[index], index, line, type);
        await this._processAttack(
          inputAttackGroup[index].trim(),
          index,
          line,
          type,
          hasMultiAttack,
          isUnchainedMonk,
          flurryClass
        );
      }
    }

    return true;
  }

  /**
   * Fix the split group, starting from the end of the array
   * - If the previous item starts with a +, add the current item to the previous item
   * - Otherwise, return the current item
   * - Filter out any empty items
   * - Reverse the array to the original order and return it
   *
   * @param {Array<string>} input The array of items to fix
   * @returns {Array<string>}     The fixed array of items
   */
  fixSplitGroup(input) {
    const reversedInput = input.reverse();
    return reversedInput
      .map((element, index) => {
        if (index > 0 && reversedInput[index - 1].startsWith("+")) return `${element} ${reversedInput[index - 1]}`;
        if (!/^[\d+-]+\//.test(element)) return element;
      })
      .filter((element) => !!element)
      .reverse();
  }

  /**
   * Process the attack string
   *
   * @param {string} inputAttack      The attack string to parse
   * @param {number} index            The index of the attack in the attack group
   * @param {number} line             The line number
   * @param {string} type             The type of attack (mwak, rwak, etc.)
   * @param {boolean} hasMultiAttack  True/False, does the creature have the multiattack feat?
   * @param {boolean} isUnchainedMonk True/False, is the creature an Unchained Monk?
   * @param {import("../../../pf1/module/documents/item/item-class.mjs").ItemClassPF} flurryClass The class to use for Flurry of Blows
   * @returns {Promise<boolean>}      Always true, unless an error occurs
   * @private
   */
  async _processAttack(inputAttack, index, line, type, hasMultiAttack, isUnchainedMonk, flurryClass) {
    console.log("FS| Processing Attack", inputAttack, index, line, type, hasMultiAttack, isUnchainedMonk, flurryClass);

    const actor = window.SBC.actor;

    // [4] Parse the attack and save the found data in two temporary constructs
    // attackData - Saves all parsed data related to the parent attack document.
    // actionData - Saves all parsed data related to the child action document(s).
    // weaponAttackDetails - Saves all parsed data related to the weapon attack item.
    // attackComparison - Saves all parsed data related to comparing the attack to the weapon attack item.
    // foundAddonTerms - Saves all found addon terms to be used later.
    // foundMaterialTerm - Saves the found material term to be used later.
    /** @type {sbcTypes.AttackData} */
    let attackData = {
      attackName: "",
      formattedAttackName: "",
      actions: [],
      img: "",
      subType: "weapon",
      attackNotes: "",
      effectNotes: [],
      specialEffects: "",
      isMasterwork: false,
      enhancementBonus: null,
      isPrimaryAttack: true,
      held: "normal",
      isTouch: false,
      isNonlethal: false,
      isNatural: false,
      isBroken: false,
      baseTypes: [],
      weaponGroups: {
        value: [],
        custom: "",
      },
      material: null,
      addons: {},
    };

    /** @type {sbcTypes.ActionData} */
    let actionData = {
      numberOfAttacks: 1,
      numberOfIterativeAttacks: 0,
      iterativeFormula: "",

      attackParts: [],
      formulaicAttacksCountFormula: "",
      formulaicAttacksBonusFormula: "",

      inputAttackModifier: 0,
      calculatedAttackBonus: 0,

      attackAbilityType: "",
      attackAbilityModifier: 0,
      defaultAttackAbility: "",

      damage: "",
      damageAbilityType: "",
      numberOfDamageDice: 0,
      damageDie: 0,
      damageBonus: 0,
      damageModifier: 0,

      damageParts: [],
      nonCritParts: [],

      defaultDamageType: "",
      damageTypes: new Set(),
      specialDamageType: "",
      hasSpecialDamageType: false,
      weaponSpecial: "-",
      critRange: 20,
      critMult: 2,
      damageMult: 1,

      attackRangeUnits: "",
      attackRangeIncrement: "",
      useOriginalDamage: false,
      featAttackBonus: 0,
      featDamageBonus: 0,
      featAttackBonusString: "",
      featDamageBonusString: "",

      hasMultiattack: hasMultiAttack,
      isUnchainedMonk: isUnchainedMonk,
      flurryClass: flurryClass,
      isFlurryOfBlows: false,
      flurryOfBlowsType: null,
    };

    let foundAddonTerms;
    let foundMaterialTerm;
    let foundBaseMaterialTerm;

    /** @type {sbcTypes.WeaponAttackDetails}*/
    let weaponAttackDetails = {
      attackName: "",
      weaponGroups: {
        value: [],
        custom: "",
      },
      isMasterwork: false,
      attackAbilityType: "",
      attackAbilityModifier: 0,
      damageAbilityType: "",
      damageBonus: 0,
      damageMult: 1,
      critRange: 20,
      critMult: 2,
      iterativeFormula: "",
      damageFormula: "",
      originalDamageFormula: "",
      featAttackBonus: 0,
      featDamageBonus: 0,
      featAttackBonusString: "",
      featDamageBonusString: "",
      baseMaterial: null,
      material: null,
      addons: [],
    };

    /** @type {sbcTypes.AttackComparison}*/
    let attackComparison = {
      statblockIterativeFormula: "",
      statblockDamageFormula: "",
      statblockDamageBonus: 0,
      statblockFullFormula: "",
      statblockEnhancementBonus: 0,

      itemExists: false,
      itemIterativeFormula: "",
      itemDamageFormula: "",
      itemDamageBonus: 0,
      itemFullFormula: "",
      itemEnhancementBonus: 0,
    };

    console.log("FS| Input Attack", inputAttack);
    // Preliminary check for Flurry of Blows, as this requires us to rename the attack
    inputAttack = this._processFlurryOfBlows(inputAttack, attackData);

    // [4.A] Parse attack related data, e.g. name, number, iterations and modifiers
    //       This is mainly everything not in parentheses in any given attack

    // Process attack details (touch, nonlethal, broken, etc.)
    inputAttack = this._processAttackProperties(inputAttack, attackData, index);

    // Process Enhancement Bonus and Masterwork
    this._processEnhancementBonus(inputAttack, attackData, attackComparison);

    // Process materials and addons
    [foundAddonTerms, foundMaterialTerm, foundBaseMaterialTerm] = this._processMaterialsAndAddons(
      inputAttack,
      attackData
    );
    if (!foundBaseMaterialTerm) {
      foundBaseMaterialTerm = "steel";
      attackData.baseMaterial = "steel";
    }

    // Process the attack name
    this._processAttackName(inputAttack, attackData, foundAddonTerms, foundMaterialTerm, foundBaseMaterialTerm, line);

    // Try to find an existing weapon from the Inventory tab for this attack
    let weapon = await this._acquireWeapon(attackData, foundAddonTerms);
    console.log("FS| Weapon", weapon);
    // If we found a weapon or natural attack, get the attack data from it
    // In the case of weapons, we have to create an attack item, even if the setting says not to
    /** @type {import("../../../pf1/module/documents/item/item-attack.mjs").ItemAttackPF} */
    let newAttack = null;
    if (weapon) {
      if (!attackData.isNatural) {
        let newAttackData = pf1.documents.item.ItemAttackPF.fromItem(weapon);
        newAttack = (await actor.createEmbeddedDocuments("Item", [newAttackData]))[0];
      } else {
        newAttack = (await createItem(weapon))[1];
      }

      attackData.attackName = weapon.name;
      attackData.formattedAttackName = weapon.name;
    }
    console.log("FS| New Attack", newAttack);

    let weaponEqualsAttack = attackData.isNatural;
    // If we have the attack data, we need to process it for later comparisons.
    if (newAttack) {
      type = await this._processWeaponDetails(
        attackComparison,
        attackData,
        weaponAttackDetails,
        weapon,
        newAttack,
        type
      );
    }
    console.log("FS| Weapon Attack Details", weaponAttackDetails);

    // The sections below (attackModifier, numberOfIterativeAttacks, damage and effect related data)
    // are processed from the statblocks inputAttack string. A lot of extra work, but necessary to be
    // able to compare the statblocks attack with the weapon attack item.
    // attackModifier
    this._processAttackModifier(inputAttack, actionData, attackData, line);

    // numberOfIterativeAttacks, when given in the statblock in the form of +X/+Y/+Z
    this._processNumberOfIterativeAttacks(inputAttack, actionData, attackData, attackComparison);

    // attackData.attackName = attackData.attackName.replace(/(^\+[1-5])|(\+[1-5]$)/g, "").trim();

    // [4.B] Parse damage and effect related data, e.g. number and type of damage dice
    //       This is mainly everything in parentheses in any given attack
    // If the attack has damage dice
    this._processAttackDamage(inputAttack, attackData, actionData, attackComparison);

    // [4.C] Compare the item data to the statblock data if it exists,
    //       then decide if we need to create a new attack
    if (attackComparison.itemExists) {
      sbcUtils.log(
        "FS| Comparing statblock data to attack item:\n" +
          `Iterative Formula: ${attackComparison.statblockIterativeFormula} vs ${attackComparison.itemIterativeFormula}\n` +
          `Damage Formula: ${attackComparison.statblockDamageFormula} vs ${attackComparison.itemDamageFormula}\n` +
          `Damage Bonus: ${attackComparison.statblockDamageBonus} vs ${attackComparison.itemDamageBonus}\n` +
          `Enhancement Bonus: ${attackComparison.statblockEnhancementBonus} vs ${attackComparison.itemEnhancementBonus}\n` +
          `Full Formula: ${attackComparison.statblockFullFormula} vs ${attackComparison.itemFullFormula}\n`
      );

      // If the attack is identical to the statblock, we may not need to make a new attack
      if (
        attackComparison.statblockIterativeFormula === attackComparison.itemIterativeFormula &&
        attackComparison.statblockDamageFormula === attackComparison.itemDamageFormula &&
        attackComparison.statblockDamageBonus === attackComparison.itemDamageBonus &&
        attackComparison.statblockEnhancementBonus === attackComparison.itemEnhancementBonus &&
        attackComparison.statblockFullFormula === attackComparison.itemFullFormula
      ) {
        weaponEqualsAttack = true;
      }
    }

    // Exit this attack early if the attack already exists and is identical to the statblock
    if (attackComparison.itemExists && weaponEqualsAttack && !attackData.isNatural) {
      if (!window.SBC.settings.getSetting("createAttacks")) {
        console.log("FS| Attack already exists and is identical to the statblock. Skipping creation.");
        // Clean up the attack if it was created and shouldn't be.
        await window.SBC.actor.deleteEmbeddedDocuments(
          "Item",
          [newAttack?.id].filter((x) => !!x)
        );
      }
    } else {
      // Bring in our data from earlier
      if (attackComparison.itemExists) {
        Object.assign(actionData, {
          attackAbilityType: weaponAttackDetails.attackAbilityType,
          attackAbilityModifier: weaponAttackDetails.attackAbilityModifier,
          damageAbilityType: weaponAttackDetails.damageAbilityType,
          featAttackBonus: weaponAttackDetails.featAttackBonus,
          featDamageBonus: weaponAttackDetails.featDamageBonus,
          featAttackBonusString: weaponAttackDetails.featAttackBonusString,
          featDamageBonusString: weaponAttackDetails.featDamageBonusString,
          useOriginalDamage: attackComparison.statblockDamageFormula === attackComparison.itemDamageFormula,
        });
      }

      // [4.D] Continue to parse the attack and action data and calculations
      // Handle melee attacks
      switch (type) {
        case "mwak":
          this._processMeleeAttackData(newAttack, actionData, attackData);
          break;

        case "rwak":
          this._processRangedAttackData(newAttack, actionData, attackData, inputAttack);
          break;

        case "twak":
          this._processThrownAttackData(newAttack, actionData, attackData, inputAttack);
          break;

        default:
          if (attackData.isNatural) {
            this._processNaturalAttackData(newAttack, actionData, attackData);
          }
          break;
      }

      // Handle swarm attacks, as these are neither melee nor ranged
      if (attackData.attackName.search(/\bSwarm\b/i) !== -1) {
        type = "other";
      }

      // Handle multiple attacks of the same type
      // Note: These are not iterative attacks!
      if (inputAttack.match(/^\d+/) !== null) {
        actionData.numberOfAttacks = parseInt(inputAttack.match(/(^\d+)/)[1]);
        attackData.attackNotes = actionData.numberOfAttacks + " " + attackData.attackNotes;
      }

      // Handle baseTypes
      this._processBaseTypes(newAttack, attackData, actionData);

      // Handle feat/feature bonuses
      if (!attackComparison.itemExists) {
        await this._processFeatBonuses(attackData, actionData);
      }

      // Calculate Attack and, if needed, compensate for differences between input attackModifier and system-derived attackModifier
      this._calculateAttackBonus(actionData, attackData);

      // Calculate Damage and, if needed, compensate for differences between
      // input damageModifier and system-derived damageModifier
      const damageDiceString = this._calculateDamageBonus(actionData, attackData, weaponAttackDetails);

      // If we have attack data from before, we can get damage types from it.
      // Otherwise, it's an unknown, so we default to untyped.
      this._processDamageTypes(newAttack, weapon, attackData, actionData);

      // ... then push the damagePart
      actionData.damageParts.push({
        formula: damageDiceString,
        types: actionData.damageTypes,
        // type: {
        //   values: actionData.damageTypes,
        //   custom: actionData.customDamageTypes,
        // },
      });

      // Push extra attacks from numberOfAttacks
      for (let i = 1; i < actionData.numberOfAttacks; i++) {
        let prefixAttackName = i + 1 + window.SBC.config.const.suffixMultiples[Math.min(3, i)];
        actionData.attackParts.push({
          formula: "",
          name: prefixAttackName + " " + attackData.attackName.replace(/e?s$/, "").capitalize(),
        });
      }

      // Push extra attacks from numberOfIterativeAttacks
      // WIP: This does not register or handle statblocks with errors in the iterations
      // if (actionData.numberOfIterativeAttacks > 0 || actionData.numberOfAttacks === 0) {
      //     actionData.formulaicAttacksCountFormula = "ceil(@attributes.bab.total/5)-1"
      // }

      // [5] Create an attack from attackData
      // If the attack already exists, update it instead
      let newAttackData = null;
      let attackUpdates = null;

      [newAttackData, attackUpdates] = this._prepareNewAttackData(
        newAttack,
        newAttackData,
        attackData,
        actionData,
        type,
        attackUpdates
      );
      console.log("FS| New Attack Data", newAttackData);
      console.log("FS| Attack Updates", attackUpdates);

      // [6] Create an action from actionData
      //     which in turn needs to be pushed to the in [5] created attack
      /** @type {import("../../../pf1/module/components/action.mjs").ItemAction} */
      let newActionData = null;
      newActionData = await this._prepareNewAction(newAttack, newActionData, actionData, attackData, type);
      console.log("FS| New Action Data", newActionData);

      // [7] Create the final document
      const attack = await this._createFinalAttack(
        newAttack,
        newAttackData,
        newActionData,
        attackData,
        attackUpdates,
        type
      );
      if (weapon && weapon.isOwned) {
        console.log(attack);
        weapon.createItemLink("children", attack);
      }
    }
  }

  /**
   * Process the Flurry of Blows attack properties
   *
   * @param {string} inputAttack              The attack string to parse
   * @param {sbcTypes.AttackData} attackData  The attack data to modify
   * @returns {string}                        The modified inputAttack
   * @private
   */
  _processFlurryOfBlows(inputAttack, attackData) {
    const flurryMatch = inputAttack.match(/flurry of blows\s*\((.*?)\)/i);
    if (flurryMatch) {
      console.log("FS| Flurry of Blows 0", inputAttack);
      inputAttack = inputAttack.replace(/flurry of blows\s*\((.*?)\)/i, flurryMatch[1]).trim();
      attackData.isFlurryOfBlows = true;
      //attackData.attackName = attackData.attackName.replace(flurryMatch[0], flurryMatch[1]).trim();

      console.log("FS| Flurry of Blows 1", flurryMatch, inputAttack);
    } else if (inputAttack.search(/flurry of blows/i) !== -1) {
      inputAttack = inputAttack.replace(/flurry of blows/i, "unarmed strike");
      attackData.isFlurryOfBlows = true;
    }

    return inputAttack;
  }

  /**
   * Process the attack properties
   * - Touch
   * - Nonlethal
   * - Broken
   *
   * @param {string} inputAttack              The attack string to parse
   * @param {sbcTypes.AttackData} attackData  The attack data to modify
   * @param {number} index                    The index of the attack in the attack group
   * @returns {void}
   * @private
   */
  _processAttackProperties(inputAttack, attackData, index) {
    // Search for Touch or Ranged Touch
    if (inputAttack.search(/\d+\s*.*((?:ranged|melee|\+\d+)?\s+touch)\b\s+\(/i) !== -1) {
      attackData.isTouch = true;

      // Remove the found data from the current input
      inputAttack = inputAttack.replace(/((?:ranged|melee|(\+\d+))?\s+touch)\b/i, "$2");

      //No valid name remaining
      if (!/[a-z](?![^(]*\))/i.test(inputAttack)) {
        attackData.attackName = "Attack " + (index + 1);
      }
    }

    // Search for Nonlethal flag
    if (inputAttack.search(/\d+\s*((?:(?:ranged|melee)\s*)?nonlethal)/i) !== -1) {
      attackData.isNonlethal = true;

      // Remove the found data from the current input
      inputAttack = inputAttack.replace(/((?:(?:ranged|melee)\s*)?nonlethal)/i, "");

      //No valid name remaining
      if (!/[a-z](?![^(]*\))/i.test(inputAttack)) {
        attackData.attackName = "Attack " + (index + 1);
      }
    }

    // Search for Broken flag
    if (inputAttack.search(/\bbroken\b/i) !== -1) {
      attackData.isBroken = true;

      // Remove the found data from the current input
      //inputAttack = inputAttack.replace(/(\bbroken\b)/i, "");

      //No valid name remaining
      if (!/[a-z](?![^(]*\))/i.test(inputAttack)) attackData.attackName = "Attack " + (index + 1);
    }

    return inputAttack;
  }

  /**
   * Process the enhancement bonus and masterwork properties
   *
   * @param {string} inputAttack                          The attack string to parse
   * @param {sbcTypes.AttackData} attackData              The attack data to modify
   * @param {sbcTypes.AttackComparison} attackComparison  The attack comparison data to modify
   * @returns {void}
   * @private
   */
  _processEnhancementBonus(inputAttack, attackData, attackComparison) {
    // enhancementBonus
    const enhBonusMatch = inputAttack.match(/(?:\W\+|^\+)(\d+)\s+(?!\(|(?:ranged |melee )?touch)/);
    if (enhBonusMatch) {
      attackData.enhancementBonus = parseInt(enhBonusMatch[1]);
      attackData.attackNotes = "+" + attackData.enhancementBonus + " " + attackData.attackNotes;

      attackComparison.statblockEnhancementBonus = +attackData.enhancementBonus;
    }

    // Masterwork
    if (inputAttack.match(/\bmwk\b/i) !== null) {
      attackData.isMasterwork = true;
      attackData.attackNotes = "mwk " + attackData.attackNotes;
    }
  }

  /**
   * Process the materials and addons
   *
   * @param {string} inputAttack              The attack string to parse
   * @param {sbcTypes.AttackData} attackData  The attack data to modify
   * @returns {[Array<string>, string, string]}       The modified foundAddonTerms, foundMaterialTerm, and foundBaseMaterialTerm
   */
  _processMaterialsAndAddons(inputAttack, attackData) {
    let foundAddonTerms = [];
    let foundMaterialTerm = "";
    let foundBaseMaterialTerm = "";

    // Handle base materials
    const baseMaterialMatch = inputAttack.match(materialsBasicRegex());
    if (baseMaterialMatch) {
      attackData.baseMaterial = baseMaterialMatch[0];
      foundBaseMaterialTerm = attackData.baseMaterial;
    }

    // Handle materials
    const materialMatch = inputAttack.match(materialsNonBasicRegex());
    if (materialMatch) {
      attackData.material = materialMatch[0];
      foundMaterialTerm = attackData.material;
      attackData.material =
        pf1.registry.materials.contents.find(
          (mt) => mt.name === attackData.material.capitalize() || mt.id === attackData.material
        )?.id ?? null;
      //altName = altName.replace(patternMaterialsNonBasics, "").trim();
    }

    // Handle addons
    const materialAddonMatch = inputAttack.match(materialAddonsRegex());
    if (materialAddonMatch) {
      foundAddonTerms = materialAddonMatch;
      foundAddonTerms.forEach((element, index) => {
        foundAddonTerms[index] =
          pf1.registry.materials.contents.find(
            (mt) => mt.name === element.capitalize() || mt.id === element.toLowerCase().replace(/\s/, "")
          )?.id ?? null;
      });
      foundAddonTerms = foundAddonTerms.filter((x) => !!x);
      attackData.addons = {};
      foundAddonTerms.forEach((element) => {
        element = element.toLowerCase().replace(/\s/, "");
        attackData.addons[element] = element;
      });
      //altName = altName.replace(patternAddons, "");
    }

    return [foundAddonTerms, foundMaterialTerm, foundBaseMaterialTerm];
  }

  /**
   * Process the attack name
   *
   * @param {string} inputAttack              The attack string to parse
   * @param {sbcTypes.AttackData} attackData  The attack data to modify
   * @param {Array<string>} foundAddonTerms   The found addon terms to modify
   * @param {string} foundMaterialTerm        The found material term to modify
   * @param {string} foundBaseMaterialTerm    The found base material term to modify
   * @param {number} _line                     The line number
   * @returns {void}
   * @private
   */
  _processAttackName(inputAttack, attackData, foundAddonTerms, foundMaterialTerm, foundBaseMaterialTerm, _line) {
    // attackName
    if (/((?:[+1-5a-zA-Z’']| (?=[+1-5a-zA-Z’'])|\*)+)[ +0-9(/]+\(*/.test(inputAttack) && !attackData.attackName) {
      console.log("FS| Attack Name 1", inputAttack);
      attackData.attackName = inputAttack
        .match(/((?:[+1-5a-zA-Z’'-]| (?=[+1-5a-zA-Z’'-])|\*)+)[ +0-9(/]+\(*/)[1]
        .replace(/^ | $|\bmwk\b |\*|\+$/gi, "")
        .replace(/\+[1-5]$/, "")
        .replace(/^\d+/, "")
        .trim()
        .replace(/(^\+[1-5])|(\+[1-5]+$)/g, "")
        .trim()
        .replace(new RegExp(`(?<!Magic\\s)${foundMaterialTerm}`, "i"), "")
        .replace(new RegExp(`(?<!Magic\\s)${foundBaseMaterialTerm}`, "i"), "")
        .replace(new RegExp(`\\b(${foundAddonTerms.join("|")})\\b`), "");

      if (
        (foundMaterialTerm.toLowerCase() === "stone" || foundBaseMaterialTerm.toLowerCase() === "stone") &&
        foundAddonTerms.includes("magic")
      ) {
        attackData.attackName = attackData.attackName.replace(/stone/i, "Magic Stone").trim();
      }

      attackData.attackNotes += attackData.attackName + " ";
    }

    attackData.attackName = attackData.attackName.trim();
    // Set the formattedAttackName to use later
    attackData.formattedAttackName = attackData.attackNotes.trim();
  }

  /**
   * Acquire the weapon from the inventory or compendium
   *
   * @param {sbcTypes.AttackData} attackData  The attack data to use
   * @param {Array<string>} foundAddonTerms   The found addon terms to use
   * @returns {Promise<import("../../../pf1/module/documents/item/item-weapon.mjs").ItemWeaponPF>} The weapon to use
   * @private
   * @async
   */
  async _acquireWeapon(attackData, foundAddonTerms) {
    /** @type {sbcTypes.SBC} */
    const sbcInstance = window.SBC;
    const compendiumOps = sbcInstance.compendiumSearch;

    let weapon = window.SBC.actor.itemTypes.weapon.find((weapon) => {
      const weaponName = weapon.name
        .replace(`+${weapon.system.enh}`, "")
        .replace(pf1.registry.materials.get(weapon.system.material.normal.value)?.name ?? "", "")
        .replace(/\s{2,}/g, " ")
        .replace("Masterwork ", "")
        .trim()
        .toLowerCase();

      const attackName = attackData.attackName.toLowerCase();

      // Check similarity between attack and weapon name. A similarity score of >=65% is probably good enough. Tweak as necessary
      let similarity = sbcUtils.stringSimilarity(attackName, weaponName);
      console.log("Comparing weapon:", weaponName, "item:", attackName, "similarity:", similarity);
      if (similarity < 0.65) {
        return false;
      }

      console.log("Comparing weapon properties...");
      if (attackData.isMasterwork && !weapon.system.masterwork) {
        console.log("Masterwork status did not match.");
        return false;
      }

      if (attackData.isBroken && !weapon.system.broken) {
        console.log("Broken status did not match.");
        return false;
      }

      if (attackData.enhancementBonus && +attackData.enhancementBonus !== weapon.system.enh) {
        console.log("Enhancement bonus did not match.");
        return false;
      }

      // Addon check
      if (foundAddonTerms.length > 0) {
        let weaponAddons = weapon.system.material.addon;
        for (let addon of foundAddonTerms) {
          if (!weaponAddons.includes(addon)) {
            console.log("Addon did not match.");
            return false;
          }
        }
      }

      // Composite *bow check
      // if(!result && /Composite/i.test(attackData.attackName))
      //     result = new RegExp(attackData.attackName, 'i').test(weaponLowerCase)

      return true;
    });

    if (!weapon) {
      // Weapon wasn't found in the inventory, so let's see if the attack is a natural attack
      // Handle natural attacks
      if (naturalAttacksRegex().test(attackData.attackName)) {
        attackData.isNatural = true;
        attackData.subType = "natural";

        // Clean up the plural name
        let tempNaturalAttackName = "";
        tempNaturalAttackName = attackData.attackName
          .match(naturalAttacksRegex())[0]
          .split(" ")
          .map((w) => w.capitalize())
          .join(" ");
        let tempNaturalAttackNameSingular = tempNaturalAttackName.replace(/s$/, "");

        // If the natural attack is from the system compendium, use that
        if (
          Object.values(window.SBC.config.naturalAttacks).find(
            (na) => na === tempNaturalAttackName || na === tempNaturalAttackNameSingular
          )
        ) {
          weapon = await compendiumOps.findEntityInCompendia(
            {
              name: tempNaturalAttackName,
              altName: tempNaturalAttackName.replace(/s$/, ""),
              item: tempNaturalAttackName,
            },
            {
              itemType: "attack",
            }
          );
        }
      }
    }

    return weapon;
  }

  /**
   * Process the weapon details
   *
   * @param {sbcTypes.AttackComparison} attackComparison          The attack comparison data to modify
   * @param {sbcTypes.AttackData} attackData                      The attack data to use
   * @param {sbcTypes.WeaponAttackDetails} weaponAttackDetails    The weapon attack details to modify
   * @param {import("../../../pf1/module/documents/item/item-weapon.mjs").ItemWeaponPF} weapon    The weapon to use
   * @param {import("../../../pf1/module/documents/item/item-attack.mjs").ItemAttackPF} newAttack The new attack to use
   * @param {string} type                                         The type of attack (mwak, rwak, etc.)
   * @returns {Promise<string>}                                   The modified type
   * @private
   * @async
   */
  async _processWeaponDetails(attackComparison, attackData, weaponAttackDetails, weapon, newAttack, type) {
    attackComparison.itemExists = true;
    weaponAttackDetails.attackName = newAttack.name;

    if (!attackData.isBroken && weapon.system.broken) {
      weaponAttackDetails.isBroken = true;
      weaponAttackDetails.weaponGroups = weapon.system.weaponGroups;
    }

    if (weapon.system.material) {
      weaponAttackDetails.material = weapon.system.material.normal.value;
      weaponAttackDetails.addons = weapon.system.material.addon;
    }

    // Process weapon groups vs feature bonuses
    for (let group of weaponAttackDetails.weaponGroups.value) {
      if (this.app.processData.characterData.weaponGroups[group]) {
        weaponAttackDetails.featAttackBonus = this.app.processData.characterData.weaponGroups[group];
        weaponAttackDetails.featDamageBonus = this.app.processData.characterData.weaponGroups[group];

        weaponAttackDetails.featAttackBonusString = `${weaponAttackDetails.featAttackBonus}[${sbcUtils.translate("feat.weaponTraining")}]`;
        weaponAttackDetails.featDamageBonusString = `${weaponAttackDetails.featDamageBonus}[${sbcUtils.translate("feat.weaponTraining")}]`;
      }
    }

    // Process weapon base type vs feat bonuses
    for (let baseType of weapon.system.baseTypes ?? []) {
      baseType = baseType.toLowerCase();
      const result1 = this._parseWeaponBonuses("attack", baseType);
      const result2 = this._parseWeaponBonuses("damage", baseType);

      if (result1[0]) {
        weaponAttackDetails.featAttackBonus += +result1[0];
        weaponAttackDetails.featAttackBonusString +=
          weaponAttackDetails.featAttackBonusString.length === 0 ? result1[1] : ` + ${result1[1]}`;
      }
      if (result2[0]) {
        weaponAttackDetails.featDamageBonus += +result2[0];
        weaponAttackDetails.featDamageBonusString +=
          weaponAttackDetails.featDamageBonusString.length === 0 ? result2[1] : ` + ${result2[1]}`;
      }
    }

    let newAttackIndex = newAttack.actionTypes.indexOf(type);
    if (newAttackIndex === -1 && type === "rwak") {
      newAttackIndex = newAttack.actionTypes.indexOf("twak");
      if (newAttackIndex !== -1) type = "twak";
      else newAttackIndex = 0;
    } else newAttackIndex = 0;

    /** @type {import("../../../pf1/module/components/action.mjs").ItemAction} */
    let newAttackAction = newAttack.actions.contents[newAttackIndex];

    // Default data
    weaponAttackDetails.attackAbilityType = newAttackAction.ability.attack;
    weaponAttackDetails.attackAbilityModifier = +sbcUtils.getModifier(
      this.app.processData.notes.statistics[weaponAttackDetails.attackAbilityType]
    );
    weaponAttackDetails.damageAbilityType = newAttackAction.ability.damage;
    weaponAttackDetails.damageBonus = +sbcUtils.getModifier(
      this.app.processData.notes.statistics[weaponAttackDetails.damageAbilityType]
    );
    weaponAttackDetails.damageMult = newAttackAction.ability.damageMult;
    weaponAttackDetails.critMult = newAttackAction.ability?.critMult;
    weaponAttackDetails.critRange = newAttackAction.ability?.critRange;

    // Build an iterative formula in the format of +X/+Y/+Z
    // let iterativeFormulaArray = newAttack.getAttackArray(newAttackAction.id);
    let iterativeFormulaArray = newAttackAction.getAttacks().map((a) => a.bonus);
    let iterativeFormula = "";
    iterativeFormulaArray.forEach((formula, index) => {
      // formula += +weaponAttackDetails.featAttackBonus;
      if (isNaN(formula)) formula = 0;

      if (weapon.system.masterwork) {
        formula++;
      }
      if (weapon.system.masterwork && weapon.system.enh) {
        formula--;
      }
      if (formula > -1) {
        iterativeFormula += "+";
      }
      iterativeFormula += formula;
      if (index !== iterativeFormulaArray.length - 1) {
        iterativeFormula += "/";
      }
    });
    weaponAttackDetails.iterativeFormula = iterativeFormula;
    attackComparison.itemIterativeFormula = iterativeFormula;

    // Build a damage formula (like 1d8 + 4/19-20/x4 plus 1d6 fire)
    let damageFormulaArray = newAttack.getAllDamageSources(newAttackAction.id);
    let damageBonus = Math.floor(weaponAttackDetails.damageBonus * weaponAttackDetails.damageMult); // + weaponAttackDetails.featDamageBonus;
    let damageFormula = "";
    if (weaponAttackDetails.critRange !== 20) {
      damageFormula += `/${weaponAttackDetails.critRange}-20`;
    }
    if (weaponAttackDetails.critMult !== 2) {
      damageFormula += `/x${weaponAttackDetails.critMult}`;
    }

    damageFormulaArray.forEach((source) => {
      if (source.modifier === "enh") {
        damageBonus += source.value;
        attackComparison.itemEnhancementBonus = source.value;
      } else if (source.flavor === "Broken") {
        damageBonus += source.value;
      } else {
        damageFormula += ` plus ${source.formula}`;
      }
    });
    attackComparison.itemDamageBonus = +damageBonus;

    // Process the damage formula for a simpler die formula
    // Rather than "sizeRoll(1, 6, @size)", process it for 1d8 as a Large for example
    let realFormula = newAttackAction.damage.parts[0]?.formula;
    let realDiceNum = null;
    let realDiceSize = null;
    let size = window.SBC.actor.system.traits.size.base;

    if (realFormula) {
      let results = realFormula.match(/sizeRoll\((?<num>\d+),\s?(?<size>\d+),?.*\)/);
      if (results) {
        realDiceNum = results.groups.num;
        realDiceSize = results.groups.size;
        size = Object.keys(pf1.config.sizeChart).indexOf(size);

        let processedRoll = pf1.utils.roll.sizeRoll(+realDiceNum, +realDiceSize, +size);
        let formula = Roll.fromTerms(processedRoll).formula;
        console.log(
          `Results for ${newAttack.name}: `,
          "Real Formula: ",
          realFormula,
          "Results: ",
          results,
          "Processed Roll: ",
          processedRoll,
          "Formula: ",
          formula
        );

        attackComparison.itemDamageFormula = formula;
        const sign = damageBonus > -1 ? "+" : "";
        damageFormula = `${formula} ${sign}${damageBonus}${damageFormula}`;

        attackComparison.itemFullFormula = damageFormula;
        weaponAttackDetails.originalDamageFormula = realFormula;
      }
    }

    delete newAttack.img;
    delete newAttack.subType;
    delete newAttack.held;

    return type;
  }

  /**
   * Process the attack damage dice and effects
   *
   * @param {string} inputAttack                          The attack string to parse
   * @param {sbcTypes.AttackData} attackData              The attack data to modify
   * @param {sbcTypes.ActionData} actionData              The action data to modify
   * @param {sbcTypes.AttackComparison} attackComparison  The attack comparison data to modify
   * @returns {void}
   * @private
   */
  _processAttackDamage(inputAttack, attackData, actionData, attackComparison) {
    const inputAttackMatch = inputAttack.match(/\(([^)]*)\)(?=[^(]*$)/);
    if (inputAttackMatch !== null) {
      let damageFormulaMatch = inputAttackMatch[1].match(/(\d+)d(\d+)/);
      if (damageFormulaMatch !== null) {
        // NumberOfDamageDice and DamageDie
        actionData.numberOfDamageDice = parseInt(damageFormulaMatch[1]);
        actionData.damageDie = parseInt(damageFormulaMatch[2]);
        attackData.attackNotes += " (" + actionData.numberOfDamageDice + "d" + actionData.damageDie;
        attackComparison.statblockDamageFormula = `${actionData.numberOfDamageDice}d${actionData.damageDie}`;
      } else if ((damageFormulaMatch = inputAttackMatch[1].match(/^(\d+)/)) !== null) {
        actionData.numberOfDamageDice = 1;
        actionData.damageDie = parseInt(damageFormulaMatch[1]);
        attackData.attackNotes += " (" + actionData.damageDie;
        attackComparison.statblockDamageFormula = `${actionData.damageDie}`;
      } else {
        actionData.numberOfDamageDice = 0;
        actionData.damageDie = 0;
      }
      // damageBonus
      const damageMatch = inputAttack.match(/d\d+(\+\d+|-\d+)/);
      if (damageMatch !== null) {
        actionData.damageBonus = parseInt(damageMatch[1]);
        attackData.attackNotes += `${actionData.damageBonus >= 0 ? "+" : "-"}${actionData.damageBonus}`;
        attackComparison.statblockDamageBonus = +actionData.damageBonus;
      } else actionData.damageAbilityType = "";

      // critRange
      const critRangeMatch = inputAttack.match(/\/(\d+)-\d+/);
      if (critRangeMatch !== null) {
        actionData.critRange = parseInt(critRangeMatch[1]);
        attackData.attackNotes += "/" + actionData.critRange + "-20";
      }
      // critMult
      const critMultMatch = inputAttack.match(/\/x(\d+)/);
      if (critMultMatch !== null) {
        actionData.critMult = parseInt(critMultMatch[1]);
        attackData.attackNotes += "/x" + actionData.critMult;
      }

      // effectNotes
      const effectNotesMatch = damageFormulaMatch
        ? inputAttackMatch[0].match(/\(\d+d\d+[+\d/\-x\s]*([^)\n]*)(?:$|\))/)
        : [null, inputAttackMatch[1]];
      if (effectNotesMatch !== null) {
        let specialEffects = effectNotesMatch[1];
        attackData.specialEffects = specialEffects;
        specialEffects = specialEffects
          .replace(/(\s+\b(?:and|plus)\b\s+)/gi, ",")
          .replace(/(^,|,$)/g, "")
          .split(",");

        if (specialEffects.length > 0) {
          for (let e = 0; e < specialEffects.length; e++) {
            let specialEffect = specialEffects[e];
            if (specialEffect !== "") {
              attackData.effectNotes.push(specialEffect);
            }
          }

          if (attackData.effectNotes.length > 0 && actionData.damageDie > 0) {
            attackData.attackNotes += (actionData.damageDie > 0 ? " plus " : "") + attackData.effectNotes.join(", ");
          }
        }
      }

      // add the closing parenthesis to the attack notes
      if (actionData.damageDie > 0) attackData.attackNotes += ")";

      let damageFormula = "";
      if (actionData.critRange !== 20) {
        damageFormula += `/${actionData.critRange}-20`;
      }
      if (actionData.critMult !== 2) {
        damageFormula += `/x${actionData.critMult}`;
      }
      if (attackData.specialEffects) {
        damageFormula += ` ${attackData.specialEffects}`;
      }

      const sign = attackComparison.statblockDamageBonus > -1 ? "+" : "";
      attackComparison.statblockFullFormula = `${attackComparison.statblockDamageFormula} ${sign}${attackComparison.statblockDamageBonus}${damageFormula}`;
    } else if (inputAttack.match(/\(([^)]*)\)/) !== null) {
      // If there is just a specialEffect in parentheses
      let specialEffect = inputAttack.replace(/\s+/g, " ").match(/\(([^)]*)\)/)[1];
      attackData.attackNotes += " (" + specialEffect + ")";
      attackData.effectNotes.push(specialEffect);
    } else {
      // If there are neither damage dice nor special effects in parentheses
      sbcUtils.log("Kind of embarrassing, but this should never happen.");
    }
  }

  /**
   * Process the number of iterative attacks
   *
   * @param {string} inputAttack                          The attack string to parse
   * @param {sbcTypes.ActionData} actionData              The action data to modify
   * @param {sbcTypes.AttackData} attackData              The attack data to modify
   * @param {sbcTypes.AttackComparison} attackComparison  The attack comparison data to modify
   * @returns {void}
   * @private
   */
  _processNumberOfIterativeAttacks(inputAttack, actionData, attackData, attackComparison) {
    const iterativeNumberMatch = inputAttack.match(/(\/\+\d+)/);
    if (iterativeNumberMatch !== null) {
      actionData.numberOfIterativeAttacks = iterativeNumberMatch.length;

      for (let i = actionData.numberOfIterativeAttacks; i > 1; i--) {
        let counter = +actionData.numberOfIterativeAttacks + 1 - i;
        let iterativeModifier =
          +actionData.inputAttackModifier - 5 * counter > -1
            ? `/+${+actionData.inputAttackModifier - 5 * counter}`
            : `/${+actionData.inputAttackModifier - 5 * counter}`;
        actionData.iterativeFormula += iterativeModifier;
        attackData.attackNotes += iterativeModifier;
      }
    }
    attackComparison.statblockIterativeFormula = actionData.iterativeFormula;
  }

  /**
   * Process the attack modifier
   *
   * @param {string} inputAttack              The attack string to parse
   * @param {sbcTypes.ActionData} actionData  The action data to modify
   * @param {sbcTypes.AttackData} attackData  The attack data to modify
   * @param {number} line                     The line number of the attack
   * @returns {void}
   * @private
   */
  _processAttackModifier(inputAttack, actionData, attackData, line) {
    if (inputAttack.match(/(\+\d+|-\d+)[+0-9/ ]*\(*/) !== null) {
      // Prefer matches that are not at the start and are followed by a parenthesis
      if (inputAttack.match(/(?!^)(\+\d+|-\d+)[+0-9/ ]*\(+/) !== null) {
        actionData.inputAttackModifier = parseInt(inputAttack.match(/(?!^)(\+\d+|-\d+)[+0-9/ ]*\(+/)[1]);
      } else if (inputAttack.match(/(?!^)(\+\d+|-\d+)[+0-9/ ]*/) !== null) {
        // Otherwise try to get just an attackModifier, e.g. for attacks without damage
        actionData.inputAttackModifier = parseInt(inputAttack.match(/(?!^)(\+\d+|-\d+)[+0-9/ ]*/)[1]);
      } else {
        // If nothing is found, fail gracefully
        let errorMessage = "Failed to find a usable attack modifier";
        let error = new sbcError(sbcError.ERRORLEVELS.ERROR, "Parse/Offense", errorMessage, line);
        this.app.processData.errors.push(error);
      }

      actionData.iterativeFormula = `${actionData.inputAttackModifier > 0 ? "+" : ""}` + actionData.inputAttackModifier;
      attackData.attackNotes += actionData.inputAttackModifier;
    }
  }

  /**
   * Process melee attack data
   *
   * @param {import("../../../pf1/module/documents/item/item-attack.mjs").ItemAttackPF} newAttack The new attack to use
   * @param {sbcTypes.ActionData} actionData  The action data to modify
   * @param {sbcTypes.AttackData} attackData  The attack data to modify
   * @returns {void}
   * @private
   */
  _processMeleeAttackData(newAttack, actionData, attackData) {
    if (!newAttack) {
      attackData.img = "systems/pf1/icons/items/weapons/elven-curve-blade.PNG";

      // set abilityTypes
      actionData.damageAbilityType = "str";
      actionData.attackRangeUnits = "melee";
    }

    // check for noStr-Flag
    if (!this.app.processData.flags.noStr) {
      actionData.attackAbilityModifier = +sbcUtils.getModifier(this.app.processData.notes.statistics.str);

      // set Str to hit
      if (!newAttack) {
        actionData.attackAbilityType = window.SBC.actor.system.attributes?.attack?.meleeAbility || "str";
      }
    } else if (!newAttack) {
      actionData.attackAbilityType = "dex";
      actionData.attackAbilityModifier = +sbcUtils.getModifier(this.app.processData.notes.statistics.dex);
    }

    // Check for WeaponFinesse-Flag
    if (this.app.processData.flags.hasWeaponFinesse) {
      actionData.attackAbilityModifier = +sbcUtils.getModifier(this.app.processData.notes.statistics.dex);

      // set Dex to hit
      actionData.attackAbilityType = "dex";
    }
  }

  /**
   * Process ranged attack data
   *
   * @param {import("../../../pf1/module/documents/item/item-attack.mjs").ItemAttackPF} newAttack The new attack to use
   * @param {sbcTypes.ActionData} actionData  The action data to modify
   * @param {sbcTypes.AttackData} attackData  The attack data to modify
   * @param {string} inputAttack              The attack string to parse
   * @returns {void}
   * @private
   */
  _processRangedAttackData(newAttack, actionData, attackData, inputAttack) {
    if (!newAttack) attackData.img = "systems/pf1/icons/items/weapons/thorn-bow.PNG";

    // check for noDex-Flag
    if (!this.app.processData.flags.noDex) {
      actionData.attackAbilityModifier = +sbcUtils.getModifier(this.app.processData.notes.statistics.dex);
      // set abilityTypes
      actionData.attackAbilityType = window.SBC.actor.system.attributes?.attack?.rangedAbility || "dex";

      if (!newAttack) {
        // Check if it's a normal bow or a crossbow, because these don't use "str" as the damage ability type
        if (inputAttack.search(/(bow\b)/i) !== -1 && inputAttack.search(/\bcomposite\b/i) === -1) {
          actionData.damageAbilityType = "";
        } else {
          // This seems wrong, as a bow would have a cap
          actionData.damageAbilityType = "str";
        }

        actionData.attackRangeUnits = "ft";
        actionData.attackRangeIncrement = "5"; // WIP: Should this really be 5?\
      }
    }
  }

  /**
   * Process thrown attack data
   *
   * @param {import("../../../pf1/module/documents/item/item-attack.mjs").ItemAttackPF} newAttack The new attack to use
   * @param {sbcTypes.ActionData} actionData  The action data to modify
   * @param {sbcTypes.AttackData} attackData  The attack data to modify
   * @param {string} inputAttack              The attack string to parse
   * @returns {void}
   * @private
   */
  _processThrownAttackData(newAttack, actionData, attackData, inputAttack) {
    if (!newAttack) attackData.img = "systems/pf1/icons/items/weapons/thorn-bow.PNG";

    // check for noDex-Flag
    if (!this.app.processData.flags.noDex) {
      actionData.attackAbilityModifier = +sbcUtils.getModifier(this.app.processData.notes.statistics.dex);
      // set abilityTypes
      actionData.attackAbilityType = window.SBC.actor.system.attributes?.attack?.rangedAbility || "dex";
      actionData.damageAbilityType = "str";
      actionData.damageMult = 1;

      if (!newAttack) {
        // Check if it's a normal bow or a crossbow, because these don't use "str" as the damage ability type
        if (inputAttack.search(/(bow\b)/i) !== -1 && inputAttack.search(/\bcomposite\b/i) === -1) {
          actionData.damageAbilityType = "";
        } else {
          // This seems wrong, as a bow would have a cap
          actionData.damageAbilityType = "str";
        }

        actionData.attackRangeUnits = "ft";
        actionData.attackRangeIncrement = "5"; // WIP: Should this really be 5?\
      }
    }
  }

  /**
   * Process natural attack data
   *
   * @param {import("../../../pf1/module/documents/item/item-attack.mjs").ItemAttackPF} newAttack The new attack to use
   * @param {sbcTypes.ActionData} actionData  The action data to modify
   * @param {sbcTypes.AttackData} attackData  The attack data to modify
   * @returns {void}
   * @private
   */
  _processNaturalAttackData(newAttack, actionData, attackData) {
    let tempNaturalAttackName = attackData.attackName.match(naturalAttacksRegex())[0];

    if (Object.keys(NaturalAttacks).find((na) => na === tempNaturalAttackName)) {
      const tempNaturalAttack = NaturalAttacks[tempNaturalAttackName.replace(/s$/, "").toLowerCase()];

      attackData.subType = "natural";
      attackData.isPrimaryAttack = tempNaturalAttack.isPrimaryAttack;
      attackData.img = tempNaturalAttack.img;
    }
  }

  /**
   * Process base types
   *
   * @param {import("../../../pf1/module/documents/item/item-attack.mjs").ItemAttackPF} newAttack The new attack to use
   * @param {sbcTypes.AttackData} attackData  The attack data to modify
   * @param {sbcTypes.ActionData} _actionData  The action data to modify
   * @returns {void}
   * @private
   */
  _processBaseTypes(newAttack, attackData, _actionData) {
    if (newAttack && newAttack.type !== "attack") {
      attackData.baseTypes = newAttack.system.baseTypes;
    } else {
      attackData.baseTypes = [attackData.formattedAttackName.replace(/(^\+[1-5])|(\+[1-5]$)/g, "").trim()];
    }
    if (attackData.isNatural) {
      attackData.baseTypes.push("natural");
    }
  }

  /**
   * Process the feat bonuses for a weapon
   *
   * @param {sbcTypes.AttackData} attackData  The attack data to modify
   * @param {sbcTypes.ActionData} actionData  The action data to modify
   * @returns {Promise<void>}
   * @private
   * @async
   */
  async _processFeatBonuses(attackData, actionData) {
    for (let baseType of attackData.baseTypes ?? []) {
      const result1 = this._parseWeaponBonuses("attack", baseType.toLowerCase());
      const result2 = this._parseWeaponBonuses("damage", baseType.toLowerCase());

      if (result1[0]) {
        actionData.featAttackBonus += result1[0];
        actionData.featAttackBonusString += result1[1];
      }
      if (result2[0]) {
        actionData.featDamageBonus += result2[0];
        actionData.featDamageBonusString += result2[1];
      }
    }
  }

  /**
   * Calculate the attack bonus
   *
   * @param {sbcTypes.ActionData} actionData  The action data to use
   * @param {sbcTypes.AttackData} attackData  The attack data to use
   * @returns {void}
   */
  _calculateAttackBonus(actionData, attackData) {
    let babModifier = +this.app.processData.characterData.conversionValidation.attributes.bab;

    // account for flurry of blows.
    if (attackData.isFlurryOfBlows) {
      babModifier += +actionData.flurryClass.system.level - +actionData.flurryClass.system.babBase;

      if (actionData.isUnchainedMonk) {
        actionData.flurryOfBlowsType = "unflurry";
      } else {
        actionData.flurryOfBlowsType = "flurry";
        babModifier -= 2;
      }
    }

    let calculatedAttackModifier =
      babModifier +
      +pf1.config.sizeMods[window.SBC.actor.system.traits.size.base] +
      +actionData.attackAbilityModifier +
      +actionData.featAttackBonus +
      (attackData.isBroken ? -2 : 0);

    // sbcUtils.log(`Calculated Attack Modifier: ${calculatedAttackModifier} =\n` +
    //     `Bab: ${this.app.processData.characterData.conversionValidation.attributes.bab}\n` +
    //     `Size Mod: ${pf1.config.sizeMods[window.SBC.actor.system.traits.size.base]}\n` +
    //     `Attack Ability Modifier: ${actionData.attackAbilityModifier}\n` +
    //     `Feat Attack Bonus: ${actionData.featAttackBonus}\n` +
    //     `Broken: ${attackData.isBroken ? -2 : 0}\n`);

    // account for Masterwork status
    if (attackData.isMasterwork) {
      calculatedAttackModifier++;
    }
    // account for enhancement bonus otherwise
    else if (attackData.enhancementBonus > 0) {
      calculatedAttackModifier += +attackData.enhancementBonus;
    }

    // verify attack's "primary attack" status
    const secondaryAttackPenalty = actionData.hasMultiattack ? 2 : 5;
    if (attackData.isNatural && calculatedAttackModifier - actionData.inputAttackModifier === secondaryAttackPenalty) {
      attackData.isPrimaryAttack = false;
    }

    // account for secondary attack penalty
    if (!attackData.isPrimaryAttack) {
      calculatedAttackModifier -= 5;
    }

    // Now compare our value with the statblocks
    // sbcUtils.log(`Calculated Attack Modifier: ${calculatedAttackModifier}\n` +
    //     `Input Attack Modifier: ${actionData.inputAttackModifier}\n` +
    //     `Calculated Attack Bonus: ${+actionData.inputAttackModifier - +calculatedAttackModifier}\n`);
    if (+calculatedAttackModifier !== +actionData.inputAttackModifier) {
      actionData.calculatedAttackBonus = +actionData.inputAttackModifier - +calculatedAttackModifier;
    }
  }

  /**
   * Calculate the damage bonus
   *
   * @param {sbcTypes.ActionData} actionData                      The action data to use
   * @param {sbcTypes.AttackData} attackData                      The attack data to use
   * @param {sbcTypes.WeaponAttackDetails} weaponAttackDetails    The weapon attack details to use
   * @returns {string}                                            The damage dice string
   * @private
   */
  _calculateDamageBonus(actionData, attackData, weaponAttackDetails) {
    let strDamageBonus = 0;
    if (actionData.damageAbilityType === "str") {
      // Use the value given in the statblock instead of the one currently in the actor
      strDamageBonus = +sbcUtils.getModifier(this.app.processData.notes.statistics.str);
    }

    // let calculatedDamageBonus = (attackData.isPrimaryAttack) ? +strDamageBonus + +attackData.enhancementBonus : strDamageBonus + +attackData.enhancementBonus - 5
    let calculatedDamageBonus =
      +strDamageBonus + +attackData.enhancementBonus + +actionData.featDamageBonus + (attackData.isBroken ? -2 : 0);
    let damageOffset = +actionData.damageBonus - +calculatedDamageBonus;
    // sbcUtils.log(`Damage Offset: ${damageOffset}\n` +
    //     `Damage Bonus: ${actionData.damageBonus}\n` +
    //     `Calculated Damage Bonus: ${calculatedDamageBonus}\n` +
    //     `Str Damage Bonus: ${strDamageBonus}\n` +
    //     `Enhancement Bonus: ${attackData.enhancementBonus}\n` +
    //     `Broken: ${attackData.isBroken ? -2 : 0}\n`);
    if (damageOffset === Math.floor(strDamageBonus / 2) && strDamageBonus > 0) {
      calculatedDamageBonus += Math.floor(strDamageBonus / 2);
      actionData.damageMult = 1.5;
      attackData.held = "2h";
    } else if (
      +actionData.damageBonus - +calculatedDamageBonus === -Math.ceil(strDamageBonus / 2) &&
      strDamageBonus > 0
    ) {
      calculatedDamageBonus -= Math.ceil(strDamageBonus / 2);
      actionData.damageMult = 0.5;
      attackData.held = "oh";
    }
    actionData.damageModifier = +actionData.damageBonus - +calculatedDamageBonus;

    // Create the string needed for the damagePart
    let damageDiceString;
    if (!actionData.useOriginalDamage) {
      damageDiceString = actionData.numberOfDamageDice + "d" + actionData.damageDie;
      if (actionData.featDamageBonusString && !window.SBC.settings.getSetting("rollBonusesIntegration")) {
        damageDiceString += " + " + actionData.featDamageBonusString;
      }
    } else {
      // damageDiceString = newAttack.actions.contents[0]?.damage.parts[0]?.formula + " + " + weaponAttackDetails.featDamageBonusString;
      damageDiceString = weaponAttackDetails.originalDamageFormula;
      if (weaponAttackDetails.featDamageBonus) {
        damageDiceString += " + " + weaponAttackDetails.featDamageBonusString;
      }
    }

    // ... and if there is a difference between the statblock and the calculation, add an adjustment modifier
    if (actionData.damageModifier !== 0) {
      if (actionData.damageModifier > 0) {
        damageDiceString += " + " + actionData.damageModifier + "[adjusted by sbc]";
      } else {
        damageDiceString += +actionData.damageModifier + "[adjusted by sbc]";
      }
    }
    damageDiceString = damageDiceString.replace(/\+ \+/g, "+ ").replace(/\s\s/, " ");

    return damageDiceString;
  }

  /**
   * Process damage types in the attack
   *
   * @param {import("../../../pf1/module/documents/item/item-attack.mjs").ItemAttackPF} newAttack The new attack
   * @param {import("../../../pf1/module/documents/item/item-weapon.mjs").ItemWeaponPF} weapon    The weapon
   * @param {sbcTypes.AttackData} attackData  The attack data to modify
   * @param {sbcTypes.ActionData} actionData  The action data to modify
   * @returns {void}
   * @private
   */
  _processDamageTypes(newAttack, weapon, attackData, actionData) {
    if (newAttack) {
      let damageType = newAttack.actions.contents[0]?.damage?.parts[0]?.types?.[0];
      if (damageType) {
        actionData.damageTypes.add(damageType);
      }
      let properties = Object.keys(weapon?.system.properties ?? {});
      properties.forEach((property) => {
        if (weapon?.system?.properties?.[property])
          attackData.attackNotes += `, ${pf1.config.weaponProperties[property]}`;
      });
    } else {
      if (attackData.isNonlethal) {
        actionData.damageTypes.add("nonlethal");
      }
      actionData.damageTypes.add("untyped");
    }

    // Check for specialDamageTypes
    // Check if the attackEffect denotes a valid damageType for the base damage,
    // and use this to override the default damage type
    for (let k = 0; k < attackData.effectNotes.length; k++) {
      let attackEffect = attackData.effectNotes[k].replaceAll(" energy", "").trim();

      let systemSupportedDamageTypes = Object.values(pf1.registry.damageTypes.getLabels()).map((x) => x.toLowerCase());
      let patternDamageTypes = new RegExp("\\b(" + systemSupportedDamageTypes.join("|") + ")\\b", "gi");

      // If the attackEffect has no additional damagePools XdY ...
      if (attackEffect.match(/\d+d\d+/) === null) {
        // ... and it matches any of the supported damageTypes ...
        const damageTypeMatch = attackEffect.match(patternDamageTypes);
        // if (attackEffect.search(patternDamageTypes) !== -1) {
        if (damageTypeMatch) {
          let specialDamageType = damageTypeMatch[0].replace("electricity", "electric").trim();

          // Remove the found attackEffect from the effectNotes array
          attackData.effectNotes.splice(k, 1);

          actionData.damageTypes = new Set(specialDamageType);
        }
      } else {
        // ... if the attackEffect has damagePools, create a new damageEntry for the attack
        let attackEffectDamage = attackEffect.match(/(\d+d\d+\s*\+*\s*\d*)/)[0];

        // Check if there is something left after removing the damage
        let attackEffectDamageType = attackEffect.replace(attackEffectDamage, "").replace("plus", "").trim();

        if (attackEffectDamageType !== "") {
          const attackDamageTypeMatch = attackEffect.match(patternDamageTypes);
          if (attackDamageTypeMatch) {
            attackEffectDamageType = attackDamageTypeMatch[0].replace("electricity", "electric").trim();
          }
        } else {
          attackEffectDamageType = "untyped";
        }

        // Push the damage values to the action
        actionData.nonCritParts.push({
          formula: attackEffectDamage,
          types: new Set([attackEffectDamageType]),
          // type: {
          //   values: [attackEffectDamageType],
          //   custom: attackEffectCustomDamageType,
          // },
        });

        // Remove the found attackEffect from the effectNotes array
        attackData.effectNotes.splice(k, 1);
      }
    }
  }

  /**
   * Prepare the data for a new attack
   *
   * @param {import("../../../pf1/module/documents/item/item-attack.mjs").ItemAttackPF} newAttack     The new attack
   * @param {import("../../../pf1/module/documents/item/item-attack.mjs").ItemAttackPF} newAttackData The new attack data
   * @param {sbcTypes.AttackData} attackData  The attack data to modify
   * @param {sbcTypes.ActionData} actionData  The action data to modify
   * @param {string} type                     The type of the attack
   * @param {object} attackUpdates            The attack updates
   * @returns {Array} The new attack data and the attack updates
   * @private
   */
  _prepareNewAttackData(newAttack, newAttackData, attackData, actionData, type, attackUpdates) {
    // Handle the flurry class association, if necessary
    /** @type {string} */
    const flurryClass = attackData.isFlurryOfBlows ? actionData.flurryClass.system.tag : null;

    if (!newAttack) {
      newAttackData = new Item.implementation({
        name: sbcUtils.capitalize(attackData.formattedAttackName) || "Undefined",
        type: "attack",
        img: attackData.img,
        system: {
          attackNotes: this.splitText(attackData.attackNotes).slice(1),
          effectNotes: attackData.effectNotes,
          masterwork: attackData.isMasterwork || (attackData.enhancementBonus ?? 0) > 0,
          broken: attackData.isBroken,
          enh: +attackData.enhancementBonus,
          proficient: true,
          held: attackData.held,
          showInQuickbar: true,
          subType: attackData.subType,
          identifiedName: sbcUtils.capitalize(attackData.formattedAttackName) || "Undefined",
          baseTypes: attackData.baseTypes,
          material: {
            base: {
              value: attackData.baseMaterial,
            },
            normal: {
              value: attackData.material,
            },
            addon: Object.keys(attackData.addons),
          },
          class: flurryClass,
        },
      });

      newAttackData.prepareData();
    } else {
      attackUpdates = {
        _id: newAttack.id,
        "system.enh": +attackData.enhancementBonus,
        "system.masterwork": attackData.isMasterwork || (attackData.enhancementBonus ?? 0) > 0,
        "system.held": attackData.held,
        "system.attackNotes": this.splitText(attackData.attackNotes).slice(1),
        "system.effectNotes": attackData.effectNotes,
        "system.broken": attackData.isBroken,
        name: sbcUtils.capitalize(attackData.formattedAttackName) || "Undefined",
        "system.material.base.value": attackData.baseMaterial,
        "system.material.normal.value": attackData.material,
        "system.material.addon": Object.keys(attackData.addons),
        "system.class": flurryClass,
      };
      newAttackData = newAttack;
    }

    return [newAttackData, attackUpdates];
  }

  /**
   * Prepare the data for a new action
   *
   * @param {import("../../../pf1/module/documents/item/item-attack.mjs").ItemAttackPF} newAttack     The new attack
   * @param {import("../../../pf1/module/components/action.mjs").ItemAction} newActionData The new action data
   * @param {sbcTypes.ActionData} actionData  The action data to modify
   * @param {sbcTypes.AttackData} attackData  The attack data to modify
   * @param {string} type                     The type of the attack
   * @returns {import("../../../pf1/module/components/action.mjs").ItemAction} The new action data
   * @private
   * @async
   */
  async _prepareNewAction(newAttack, newActionData, actionData, attackData, type) {
    // Process attack and damage numbers for additions to the Attack Bonus and Damage Formula fields
    let attackBonusString =
      actionData.calculatedAttackBonus !== 0
        ? attackData.isNatural && actionData.hasMultiattack && actionData.calculatedAttackBonus === 3
          ? null
          : actionData.calculatedAttackBonus.toString() + "[adjusted by sbc]"
        : null;

    if (actionData.featAttackBonusString && !window.SBC.settings.getSetting("rollBonusesIntegration")) {
      attackBonusString = attackBonusString
        ? `${attackBonusString} + ${actionData.featAttackBonusString}`
        : actionData.featAttackBonusString;
    }

    if (attackBonusString) {
      attackBonusString = attackBonusString
        .replace(/\+(\s+\+)+/g, "+ ")
        .replace(/\s+/g, " ")
        .replace(/^\+\s+/, "")
        .replace(/\s+\+$/, "");
    }

    // Update the extra attack types setting from PF1E v10.0
    let extraAttacksType = attackData.isFlurryOfBlows
      ? actionData.flurryOfBlowsType
      : actionData.attackParts.length > 0 && actionData.numberOfIterativeAttacks > 0
        ? "advanced"
        : actionData.numberOfIterativeAttacks > 0 && actionData.attackParts.length === 0
          ? "standard"
          : "custom";

    const babOverride = attackData.isFlurryOfBlows
      ? actionData.flurryOfBlowsType === "flurry"
        ? (
            +this.app.processData.characterData.conversionValidation.attributes.bab +
            (actionData.flurryClass.system.level - actionData.flurryClass.system.babBase)
          ).toString()
        : null
      : null;

    if (!newAttack) {
      newActionData = {
        ...pf1.components.ItemAction.defaultData,
        name: attackData.attackName.capitalize(),
        img: attackData.img,
        activation: {
          cost: 1,
          type: "attack",
        },
        bab: babOverride,
        unchainedAction: {
          activation: {
            cost: 1,
            type: "attack",
          },
        },
        range: {
          value: actionData.attackRangeIncrement,
          units: actionData.attackRangeUnits,
          maxIncrements: 1,
        },
        attackName: attackData.attackName.replace(/((?!Blows|blows)e?s)$/, "").capitalize(),
        actionType: type,
        attackBonus: attackBonusString,
        critConfirmBonus: "",
        damage: {
          critParts: [],
          nonCritParts: actionData.nonCritParts,
          parts: actionData.damageParts,
        },
        extraAttacks: {
          type: extraAttacksType,
          manual: actionData.attackParts,
        },
        formulaicAttacks: {
          count: {
            formula: actionData.formulaicAttacksCountFormula,
          },
          bonus: {
            formula: "@formulaicAttack * -5",
          },
        },
        ability: {
          attack: actionData.attackAbilityType,
          damage: actionData.damageAbilityType,
          damageMult: actionData.damageMult,
          critRange: actionData.critRange,
          critMult: actionData.critMult,
        },
        naturalAttack: {
          primaryAttack: attackData.isPrimaryAttack,
          ["secondary.attackBonus"]: actionData.hasMultiattack ? "-2" : "-5",
          ["secondary.damageMult"]: actionData.damageMult && attackData.isNatural ? actionData.damageMult : 0.5,
        },
        nonlethal: attackData.isNonlethal,
        touch: attackData.isTouch,
      };
    } else {
      let newActionIndex = newAttack.actionTypes.indexOf(type);
      if (newActionIndex === -1 && type === "rwak") {
        newActionIndex = newAttack.actionTypes.indexOf("twak");
        if (newActionIndex !== -1) type = "twak";
        else newActionIndex = 0;
      } else newActionIndex = 0;

      let action = newAttack.actions.get(newAttack.actions.contents[newActionIndex].id);
      let updates = {
        attackBonus: attackBonusString,
        damage: {
          critParts: [],
          nonCritParts: actionData.nonCritParts,
          parts: actionData.damageParts,
        },
        ["extraAttacks.type"]: extraAttacksType,
        ["extraAttacks.manual"]: actionData.attackParts,
        ["ability.attack"]: actionData.attackAbilityType,
        ["ability.critRange"]: actionData.critRange,
        ["ability.critMult"]: actionData.critMult,
        ["ability.damageMult"]: actionData.damageMult,
        nonlethal: attackData.isNonlethal,
        touch: attackData.isTouch,
        bab: babOverride,
        actionType: type,
      };

      if (newAttack.system.subType === "natural") {
        updates["naturalAttack.primaryAttack"] = attackData.isPrimaryAttack;
        updates["naturalAttack.secondary.attackBonus"] = actionData.hasMultiattack ? "-2" : "-5";
        updates["naturalAttack.secondary.damageMult"] =
          actionData.damageMult && attackData.isNatural ? actionData.damageMult : 0.5;
      }

      await action.update(updates);
    }

    return newActionData;
  }

  /**
   * Create the final attack
   *
   * @param {import("../../../pf1/module/documents/item/item-attack.mjs").ItemAttackPF} newAttack     The new attack
   * @param {import("../../../pf1/module/documents/item/item-attack.mjs").ItemAttackPF} newAttackData The new attack data
   * @param {import("../../../pf1/module/components/action.mjs").ItemAction} newActionData The new action data
   * @param {sbcTypes.AttackData} attackData  The attack data to modify
   * @param {object} attackUpdates            The attack updates
   * @param {string} type                     The type of the attack
   * @private
   * @async
   * @returns {Promise<ItemPF>} The new attack item
   */
  async _createFinalAttack(newAttack, newAttackData, newActionData, attackData, attackUpdates, type) {
    let existingName = (newAttack ? newAttack.name : newAttackData.name).toLowerCase();
    let existingId = newAttack ? newAttack.id : newAttackData.id;

    // Check if an attack with the same name already exists
    console.log(`Looking for existing attack with name ${existingName} and id ${existingId}`);
    let existingAttack = window.SBC.actor.itemTypes.attack.find(
      (ita) => ita.name.toLowerCase() === existingName && ita.id !== existingId
    );

    let attack = existingAttack;

    if (existingAttack) {
      let comparisonData = {
        enh: newAttack ? (attackUpdates["system.enh"] ?? newAttack.system.enh) : attackData.enhancementBonus,
        masterwork: newAttack
          ? (attackUpdates["system.masterwork"] ?? newAttack.system.masterwork)
          : attackData.isMasterwork,
        broken: newAttack ? (attackUpdates["system.broken"] ?? newAttack.system.broken) : attackData.isBroken,
      };
      if (!comparisonData.enh) comparisonData.enh = 0;

      if (
        existingAttack.system.enh === comparisonData.enh &&
        existingAttack.system.masterwork === comparisonData.masterwork &&
        existingAttack.system.broken === comparisonData.broken
      ) {
        // Import the current attack item's attack and effect notes over to the existing attack
        let newAttackNotes = new Set(
          existingAttack.system.attackNotes.concat(this.splitText(attackData.attackNotes).slice(1))
        );
        let newEffectNotes = new Set(existingAttack.system.effectNotes.concat(attackData.effectNotes));
        await window.SBC.actor.updateEmbeddedDocuments("Item", [
          {
            _id: existingAttack.id,
            "system.attackNotes": Array.from(newAttackNotes),
            "system.effectNotes": Array.from(newEffectNotes),
          },
        ]);

        let existingAttackIndex = existingAttack.actionTypes.indexOf(type);
        if (existingAttackIndex === -1 && type === "rwak") {
          existingAttackIndex = existingAttack.actionTypes.indexOf("twak");
          if (existingAttackIndex !== -1) type = "twak";
        }

        let actionData = newAttack ? newAttack.actions.contents[0] : newActionData;
        // Clone the action to make it unique, remove the id, and if necessary update the name
        let newAction = pf1.utils.deepClone(actionData);
        if (newAction) {
          if (attackData.isFlurryOfBlows) {
            newAction.name = "Flurry of Blows";
          }

          if (!attackData.isPrimaryAttack && attackData.isNatural) {
            newAction.name += " (Secondary)";
          } else if (newAction.name !== "Attack") {
            if (type === "mwak") newAction.name = "Melee";
            else if (type === "rwak") newAction.name = "Ranged";
            else if (type === "twak") newAction.name = "Thrown";
          } else {
            if (type === "mwak") newAction.name += " (Melee)";
            else if (type === "rwak") newAction.name += " (Ranged)";
            else if (type === "twak") newAction.name += " (Thrown)";
          }

          delete newAction._id;
        } else {
          if (attackData.isFlurryOfBlows) {
            newAction.name = "Flurry of Blows";
          }

          if (!attackData.isPrimaryAttack && attackData.isNatural) {
            newAction.name += " (Secondary)";
          } else if (newAction.name !== "Attack") {
            if (type === "mwak") newAction.name = "Melee";
            else if (type === "rwak") newAction.name = "Ranged";
            else if (type === "twak") newAction.name = "Thrown";
          } else {
            if (type === "mwak") newAction.name += " (Melee)";
            else if (type === "rwak") newAction.name += " (Ranged)";
            else if (type === "twak") newAction.name += " (Thrown)";
          }

          delete newAction._id;
        }

        // Add it to the existing attack, and erase the current attack
        await pf1.components.ItemAction.create([newAction], {
          parent: existingAttack,
        });

        /** @type {import("../../../pf1/module/components/action.mjs").ItemAction[] }*/
        const existingNewActions = [];
        let existingActions = existingAttack.toObject().system.actions;
        for (let i = 0; i < existingActions.length; i++) {
          if (i !== existingAttackIndex) existingNewActions.push(existingActions[i]);
        }
        await existingAttack.update({ "system.actions": existingNewActions });

        if (attackUpdates?.["system.class"]) {
          await existingAttack.update({ "system.class": attackUpdates["system.class"] });
        }
        if (newAttack) await window.SBC.actor.deleteEmbeddedDocuments("Item", [newAttack.id]);
      }
    } else {
      // Push it into this attack as well
      if (!newAttack) {
        await newAttackData.updateSource({
          "system.actions": [newActionData],
        });
        newAttackData.prepareData();

        // And lastly add the attack to the item stack
        attack = (await createItem(newAttackData))[1];
      } else {
        // Update the current attack
        await window.SBC.actor.updateEmbeddedDocuments("Item", [attackUpdates]);
        newAttackData.prepareData();
        attack = newAttackData;
      }
    }

    return attack;
  }

  /**
   * Parse the weapon bonuses for the attack and damage bonuses
   *
   * @param {string} type     The type of bonus to parse (attack or damage)
   * @param {string} value    The weapon type to check for bonuses
   * @returns {Array}         The bonus value and the bonus string for labelling
   */
  _parseWeaponBonuses(type, value) {
    let result = 0;
    let resultString = "";

    const characterData = this.app.processData.characterData;

    switch (type) {
      case "attack":
        if (characterData.weaponFocus.includes(value)) {
          result++;
          resultString += `+ 1[${sbcUtils.translate("feat.weaponFocus")}]`;
        }
        if (characterData.weaponFocusGreater.includes(value)) {
          result++;
          resultString += `+ 1[${sbcUtils.translate("feat.greaterWeaponFocus")}]`;
        }
        break;

      case "damage":
        if (characterData.weaponSpecialization.includes(value)) {
          result += 2;
          resultString += `+ 2[${sbcUtils.translate("feat.weaponSpecialization")}]`;
        }
        if (characterData.weaponSpecializationGreater.includes(value)) {
          result += 2;
          resultString += `+ 2[${sbcUtils.translate("feat.greaterWeaponSpecialization")}]`;
        }
        break;

      default:
        break;
    }

    // sbcUtils.log(`Weapon ${type} bonus for ${value}: ${result}, ${resultString}`);
    return [result, resultString];
  }
}
