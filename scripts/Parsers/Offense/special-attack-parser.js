import { sbcUtils } from "../../sbcUtils.js";
import { AbstractParser } from "../abstract-parser.js";
import { createItem } from "../../sbcParser.js";

// Parse Special Attacks
export default class SpecialAttackParser extends AbstractParser {
  /**
   * @inheritDoc
   */
  get parserGroup() {
    return "Parser/Offense";
  }

  /**
   * @inheritDoc
   */
  get parserType() {
    return "specialAttack";
  }

  /**
   * Parses a special attack string
   *
   * @param {string} value        The value to parse
   * @param {number} line         The line number
   * @param {string} _type        The type of value
   * @returns {Promise<boolean>}  Always true, unless an error occurs
   * @private
   */
  async _parse(value, line, _type = undefined) {
    const specialAttacks = this.fixSplitGroup(this.splitText(value, false));

    for (const specialAttackName of specialAttacks) {
      console.log(
        sbcUtils.translate("parser.specialAttack.parseInfo", {
          value: specialAttackName,
        })
      );

      const specialAttack = {
        name: sbcUtils.translate("parser.specialAttack.placeholderName", { value: specialAttackName }),
        type: "attack",
        desc: sbcUtils.translate("parser.specialAttack.placeholderDescription", { value: specialAttackName }),
      };

      const placeholder = await sbcUtils.generatePlaceholderEntity(specialAttack, line);

      await createItem(placeholder);
    }

    return true;
  }
}
