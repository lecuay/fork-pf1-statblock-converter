import { AdditionalSenses } from "../../Content/index.js";
import { AbstractParser } from "../abstract-parser.js";

const senseAttributeMap = {
  blindsight: "system.traits.senses.bs.value",
  blindsense: "system.traits.senses.bse.value",
  darkvision: "system.traits.senses.dv.value",
  tremorsense: "system.traits.senses.ts.value",
  scent: "system.traits.senses.sc.value",
  truesight: "system.traits.senses.tr.value",
  "true seeing": "system.traits.senses.tr.value",
  "see in darkness": "system.traits.senses.sid",
  "see invisibility": "system.traits.senses.si",
  "low-light": "system.traits.senses.ll.enabled",
};

const sensePhraseMap = {
  blindsight: "PF1.Sense.blindsight",
  blindsense: "PF1.Sense.blindsense",
  darkvision: "PF1.Sense.darkvision",
  tremorsense: "PF1.Sense.tremorsense",
  scent: "PF1.Sense.scent",
  "see in darkness": "PF1.Sense.seeInDark",
  truesight: "PF1.Sense.trueseeing",
  "true seeing": "PF1.Sense.trueseeing",
  "see invisibility": "PF1.Sense.seeInvis",
  "low-light": "PF1.Sense.lowlight",
};

// Parse Senses
export default class SensesParser extends AbstractParser {
  /**
   * @inheritDoc
   */
  get parserGroup() {
    return "Parser/Base";
  }

  /**
   * @inheritDoc
   */
  get parserType() {
    return "sense";
  }

  /**
   * Parses a sense string
   *
   * @param {string} value        The value to parse
   * @param {number} line         The line number
   * @param {string} _type        The type of value
   * @returns {Promise<boolean>}  Always true, unless an error occurs
   */
  async _parse(value, line, _type = undefined) {
    const systemSupportedSenses = Object.values(pf1.config.senses).map((x) => x.toLowerCase());
    const availableSenses = systemSupportedSenses.concat(AdditionalSenses);
    let customSenses = [];

    // Search for matches
    this.app.processData.notes.base.senses = [];
    for (const searchSense of availableSenses) {
      if (value.search(searchSense) === -1) {
        continue;
      }

      switch (searchSense) {
        // Range
        case "scent":
        case "truesight":
        case "true seeing":
        case "blindsight":
        case "blindsense":
        case "darkvision":
        case "tremorsense": {
          const rangeRegEx = new RegExp(searchSense + "\\s(\\d+)", "");
          const range = value.match(rangeRegEx)?.[1] ?? (["truesight", "true seeing"].includes(searchSense) ? 120 : 0);
          await window.SBC.actor.update({ [senseAttributeMap[searchSense]]: +range });
          const [distance, unit] = pf1.utils.convertDistance(+range);

          const phraseKey = sensePhraseMap[searchSense] || null;

          this.app.processData.notes.base.senses.push(
            (phraseKey ? game.i18n.localize(phraseKey) : searchSense.capitalize()) + ` ${distance} ${unit}`
          );
          break;
        }
        // Yes/No Toggle
        case "see in darkness":
        case "see invisibility":
        case "low-light": {
          await window.SBC.actor.update({ [senseAttributeMap[searchSense]]: true });
          const phraseKey = sensePhraseMap[searchSense] || null;
          this.app.processData.notes.base.senses.push(
            phraseKey ? game.i18n.localize(phraseKey) : searchSense.capitalize()
          );
          break;
        }

        // Custom Senses
        default:
          customSenses.push(searchSense.capitalize());
          this.app.processData.notes.base.senses.push(searchSense.capitalize());
          break;
      }
    }

    if (!this.app.processData.notes.base.senses.length) {
      delete this.app.processData.notes.base.senses;
    }

    // Set customSenses
    if (customSenses.length) {
      await window.SBC.actor.update({ "system.traits.senses.custom": customSenses.join(";") });
    }

    return true;
  }
}
