import { sbcUtils } from "../../sbcUtils.js";
import { AbstractParser } from "../abstract-parser.js";
import { createItem } from "../../sbcParser.js";

// Parse Tactics
export default class TacticsParser extends AbstractParser {
  /**
   * @inheritDoc
   */
  get parserGroup() {
    return "Parser/Misc";
  }

  /**
   * @inheritDoc
   */
  get parserType() {
    return "tactics";
  }

  /**
   * Parses a tactics
   *
   * @param {string} value        The value to parse
   * @param {number} line         The line number
   * @param {string} _type        The type of value
   * @returns {Promise<boolean>}  Always true, unless an error occurs
   */
  async parse(value, line, _type = undefined) {
    sbcUtils.log(sbcUtils.translate("parser.tactics.status", value));

    const tacticsDesc = `<div><strong>${value.name}</strong>: ${value.entry}</div>`;

    // Check, if there already is a tactics item
    /** @type {import("../../../pf1/module/action-use/action-use.mjs").ItemPF} */
    const tacticsItem = window.SBC.actor.items.contents.find((item) => item.name === "Tactics");

    // Update the existing item or create a new one
    if (tacticsItem) {
      const tempDesc = tacticsItem.system.description.value;
      await tacticsItem.update({
        "system.description.value": tempDesc + tacticsDesc,
      });
    } else {
      const tacticsEntry = {
        name: sbcUtils.translate("parser.tactics.placeholderName"),
        type: "misc",
        desc: tacticsDesc,
        img: "icons/skills/targeting/crosshair-pointed-orange.webp",
      };

      const placeholder = await sbcUtils.generatePlaceholderEntity(tacticsEntry, line);

      await createItem(placeholder);
    }

    return true;
  }
}
