import { sbcUtils } from "../sbcUtils.js";
import { sbcError } from "../sbcError.js";

// eslint-disable-next-line no-unused-vars
import * as sbcTypes from "../sbcTypes.js";
import { escapeForRegex, parensMergeRegex } from "../sbcRegex.js";

// Base class for specialized parsers
export class AbstractParser {
  /**
   * Create a new AbstractParser
   *
   * @param {sbcTypes.InputDialog} app  The SBC application
   */
  constructor(app) {
    this.app = app;
  }

  /**
   * The group that the parser belongs to
   *
   * @returns {string}  The parser group
   */
  get parserGroup() {
    return "Parser";
  }

  /**
   * The type of parser
   *
   * @returns {string}  The parser type
   */
  get parserType() {
    return "base";
  }

  /**
   * Parse a specific bit of content and process it onto an actor or item
   *
   * @param {string} _value       The value to parse
   * @param {number} _line        The line number
   * @param {string} _type        The type of value
   * @returns {Promise<boolean>}  Always true, unless an error occurs
   * @private
   */
  async _parse(_value, _line, _type) {
    return true;
  }

  throwError(err, phraseKey, value, line, ...data) {
    err && window.SBC.settings.getSetting("debug") && console.error(err);
    const errorMessage = sbcUtils.translate(phraseKey, {
      value: value,
      line: line,
      ...data,
    });
    const error = new sbcError(sbcError.ERRORLEVELS.ERROR, this.parserGroup, errorMessage, line, value);
    this.app.processData.errors.push(error);
  }

  logInfo(phraseKey, value, line, data) {
    const infoMessage = sbcUtils.translate(phraseKey, {
      value: value,
      line: line,
      ...data,
    });
    const info = new sbcError(sbcError.ERRORLEVELS.INFO, this.parserGroup, infoMessage, line, value);
    this.app.processData.errors.push(info);
  }

  /**
   * Throw a parser error
   *
   * @param {Error} err     The error that occurred
   * @param {string} value  The value that caused the error
   * @param {number} line   The line number
   * @param {string} type   The type of value
   * @internal
   */
  throwParseError(err, value, line, type) {
    this.throwError(err, `parser.${this.parserType}.error`, value, line, type);
  }

  /**
   * Parse a specific bit of content and process it onto an actor or item
   * This is a generic method that should be overridden by any subclass
   *
   * @param {string} value        The value to parse
   * @param {number} line         The line number
   * @param {string} type         The type of value
   * @returns {Promise<boolean>}  Always true, unless an error occurs
   */
  async parse(value, line, type = undefined) {
    try {
      sbcUtils.log(
        sbcUtils.translate(`parser.${this.parserType}.status`, {
          value: value,
          line: line,
          type: type,
        })
      );
      return this._parse(value, line, type);
    } catch (err) {
      this.throwParseError(err, value, line, type);
      return false;
    }
  }

  /**
   * Split a string into an array of items, using commas as the delimiter
   *  - Commas within parenthesis are ignored
   *  - If splitGroups is true, it will split items in parentheses into separate items
   *  - If splitGroups is false, it will keep items in parentheses together
   *
   * @param {string} input        The input string to split
   * @param {boolean} splitGroups Whether to split items in parentheses into separate items
   * @returns {Array<string>}     The array of items
   * @example
   * // Basic Usage
   * splitText("Fireball, Lightning Bolt (3d6), Cure Wounds") => ["Fireball", "Lightning Bolt (3d6)", "Cure Wounds"]
   *
   * @example
   * // Split class feature sub-choices into separate items
   * splitText("Sneak Attack +3d6, Rogue Talents (ambusher, trap spotter)") => ["Sneak Attack +3d6", "Rogue Talents (ambusher)", "Rogue Talents (trap spotter)"]
   *
   * @example
   * // Keep a spell's pieces together
   * splitText("Fireball (DC15), Lightning Bolt (8d6, DC15)", false) => ["Fireball (DC15)", "Lightning Bolt (8d6, DC15)"]
   */
  splitText(input, splitGroups = true) {
    // Split the input string at commas, excluding commas in parentheses, and return an array of the items
    let items = [];

    // Remove Commas in large Numbers, e.g. 3,000 GP --> 3000 GP
    let tempInput = input.replace(/(\d)(,)(\d)/g, "$1$3").trim();

    // Check if there are any commas or semicolons
    if (tempInput.search(/[,;]/g) === -1) {
      // When there is only one item, split at the first closing bracket and put it into the array
      items = tempInput.replace(/\)\s/, ");").split(/;/);
    }
    // Check if there are parenthesis including commas in the input
    else if (tempInput.match(/([^,;]+\([^(]+?[,;][^(]+?\))+?/gi) === null) {
      // If there are no parenthesis with commas, just split at commas/semicolons
      items = tempInput.split(/[,;]/g);
    } else {
      // Get the input with parenthesis and commas inside the parenthesis
      let itemsWithCommasInParenthesis = tempInput.match(/([^,;]+\([^(]+?[,;][^(]+?\)[^,]*)+?/gi);
      // let itemsWithCommasInParenthesis = tempInput.match(
      //   /([^,;]+\((?:[^[\]()]*|\[[^\]]*\])*?[,;](?:[^[\]()]*|\[[^\]]*\])*?\)[^,]*)+?/gi
      // );
      let itemsWithCommasInParenthesisKeys = Object.keys(itemsWithCommasInParenthesis);

      for (let i = 0; i < itemsWithCommasInParenthesisKeys.length; i++) {
        let tempKey = itemsWithCommasInParenthesisKeys[i];
        let tempItem = itemsWithCommasInParenthesis[tempKey].trim();

        if (splitGroups) {
          const re = /^(?<name>.*?)(?:\s\((?<parens>.*)\)(?<bonus>.*))?$/g.exec(tempItem);
          const { name, parens, bonus } = re.groups;

          if (
            ["change shape", "witch's familiar", "hex", "hexes", "bardic performances", "rogue talents"].includes(
              name.toLowerCase()
            )
          ) {
            items.push(tempItem);
          } else {
            let parensList = parens.split(", ");
            let matchCount = 0;
            for (let i = 1; i < parensList.length; i++) {
              let el = parensList[i].trim();
              if (!parensMergeRegex().test(el)) {
                matchCount = 0;
                continue;
              }

              const newEl = (parensList[i - ++matchCount] + `, ${el}`).replace(/,\s*,/g, ",");
              parensList[i - matchCount] = newEl;
              parensList[i] = "";
            }
            parensList = parensList.filter((el) => el !== "");

            parensList.forEach((subItem) => {
              subItem = subItem.trim();

              subItem = `${name} (${subItem})${bonus}`;

              items.push(subItem);
            });
          }
        } else {
          items.push(tempItem);
        }

        let patternTempItem = new RegExp(escapeForRegex(tempItem), "i");

        tempInput = tempInput.replace(patternTempItem, "").replace(/,\s*,/, ",").replace(/^,/, "").trim();
      }

      // Add any items without parenthesis back into the "items"-array
      let itemsWithoutParenthesis = [];

      if (tempInput !== "") {
        itemsWithoutParenthesis = tempInput
          .replace(/,\s*,/, ",")
          .replace(/[;,]\s*$/, "")
          .split(/[,;]/g);
      }

      if (itemsWithoutParenthesis.length > 0) {
        //items = items.concat(...itemsWithoutParenthesis)
        for (let i = 0; i < itemsWithoutParenthesis.length; i++) {
          itemsWithoutParenthesis[i] = itemsWithoutParenthesis[i].trim();
        }

        items = itemsWithoutParenthesis.concat(...items);
      }
    }

    return items.map((item) => item.trim());
  }

  /**
   * Fix the split group, starting from the end of the array
   * - If the previous item starts with a +, add the current item to the previous item
   * - If the current item does not start with a +, return the current item
   * - Filter out any empty items
   * - Reverse the array to the original order and return it
   *
   * @param {Array<string>} input The array of items to fix
   * @returns {Array<string>}     The fixed array of items
   */
  fixSplitGroup(input) {
    const reversedInput = input.reverse();
    return reversedInput
      .map((element, index) => {
        if (index > 0 && reversedInput[index - 1].startsWith("+")) return `${element} ${reversedInput[index - 1]}`;
        if (!element.startsWith("+")) return element;
      })
      .filter((element) => !!element)
      .reverse();
  }
}
