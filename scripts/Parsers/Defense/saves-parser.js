import { sbcUtils } from "../../sbcUtils.js";
import { AbstractParser } from "../abstract-parser.js";
import { savesRegex } from "../../sbcRegex.js";

// eslint-disable-next-line no-unused-vars
const SaveTypes = Object.freeze({
  Fort: Symbol("fort"),
  Ref: Symbol("ref"),
  Will: Symbol("will"),
});

// Parse Saves and Save Context Notes
export default class SavesParser extends AbstractParser {
  /**
   * @inheritDoc
   */
  get parserGroup() {
    return "Parser/Defense";
  }

  /**
   * @inheritDoc
   */
  get parserType() {
    return "save";
  }

  /**
   * Parses a save string
   *
   * @param {string} value        The value to parse
   * @param {number} line         The line number
   * @param {string} _type        The type of value
   * @returns {Promise<boolean>}  Always true, unless an error occurs
   */
  async _parse(value, line, _type = undefined) {
    let input = value;
    let saveContext = "";

    // Unparenthesized semicolon
    if (/;(?![^(]*\))/.test(input)) {
      [input, saveContext] = input.split(/;(?![^(]*\))/);
    }

    const saves = this.splitText(input).map(sbcUtils.parseSubtext);

    for (let k = 0; k < saves.length; k++) {
      const line = saves[k][0];
      let { type, value } = line.match(savesRegex()).groups;
      type = type.toLowerCase();

      if (!type) {
        this.throwError(undefined, "parser.save.error", saves[k].join(""), line);
        continue;
      }

      this.app.processData.characterData.conversionValidation.attributes[type] = value;
      this.app.processData.notes.defense[type + "Save"] = value;
      if (saves[k][1]) {
        saveContext += (saveContext ? "\n" : "") + type.capitalize() + ": " + saves[k][1];
      }
    }

    // Check if there are context notes for the saves
    if (saveContext) {
      this.app.processData.miscNotes.saveNotes = saveContext.trim();
      this.app.processData.notes.defense.saveNotes = saveContext.trim();
      // await window.SBC.actor.update({
      //   "system.attributes.saveNotes": saveContext.trim(),
      // });
    }

    return true;
  }
}
