import { AbstractParser } from "../abstract-parser.js";
import { sbcUtils } from "../../sbcUtils.js";
import { createItem } from "../../sbcParser.js";
import { conditionTypesRegex, damageTypesRegex } from "../../sbcRegex.js";

// eslint-disable-next-line no-unused-vars
import * as sbcTypes from "../../sbcTypes.js";

// Parse Resistances
export default class ResistanceParser extends AbstractParser {
  /**
   * @inheritDoc
   */
  get parserGroup() {
    return "Parser/Defense";
  }

  /**
   * @inheritDoc
   */
  get parserType() {
    return "resistance";
  }

  /**
   * Parses a resistance string
   *
   * @param {string} value        The value to parse
   * @param {number} _line         The line number
   * @param {string} _type        The type of value
   * @returns {Promise<boolean>}  Always true, unless an error occurs
   */
  async _parse(value, _line, _type = undefined) {
    let rawInput = value
      .replace(/(^[,;\s]*|[,;\s]*$)/g, "")
      .replace("electricity", "electric")
      .replaceAll(" energy", "");
    let input = this.splitText(rawInput);

    this.app.processData.notes.defense.resist = [];

    /** @type {sbcTypes.SBC} */
    const sbcInstance = window.SBC;
    const compendiumOps = sbcInstance.compendiumSearch;
    // Get the current DR data
    /** @type {Array<sbcTypes.DamageReductionEntry>} */
    let eresValues = sbcInstance.actor.system.traits.eres.value;
    /** @type {string} */
    let eresCustom = sbcInstance.actor.system.traits.eres.custom;
    /** @type {string} */
    let cresConditions = sbcInstance.actor.system.traits.cres;

    for (let i = 0; i < input.length; i++) {
      let resistance = input[i].replace(/Effects/gi, "").trim();

      // it's a condition resistance
      if (resistance.match(conditionTypesRegex())) {
        cresConditions += `${resistance.capitalize()};`;
        this.app.processData.notes.defense.resist.push(cresConditions);
      } else {
        // It's likely an energy resistance string, but could be an ability.
        // Execute a regex to parse the ER string into groups
        const re = /^(?<type1>\S+)(?:\s*(?<operator>or|and)\s*(?<type2>\S+))?\s*(?<amount>\d+)$/g.exec(resistance);
        if (!re) {
          // It's not a resistance, so it's likely an ability

          let searchEntity = {
            search: new Set(), // The search strings
            type: "feats",
            item: resistance,
          };
          searchEntity.search = sbcUtils.createSearchSet([sbcUtils.capitalize(resistance)], true);

          // If the input is found in one of the compendiums, generate an entity from that
          /** @type {import ("../../../pf1/module/documents/item/item-pf.mjs").ItemPF} */
          let entity = await compendiumOps.findEntityInCompendia(searchEntity, {
            itemTypes: ["feat"],
            itemSubTypes: ["classFeat", "misc"],
            classes: this.app.processData.characterData.classes,
          });

          if (!entity) {
            searchEntity.itemTypes = "misc";
            entity = await sbcUtils.generatePlaceholderEntity(searchEntity);
          }

          let foundItem = sbcUtils.checkForDuplicateItem(sbcInstance.actor, "feat", "classFeat", resistance, entity);

          if (!foundItem) {
            foundItem = sbcUtils.checkForDuplicateItem(sbcInstance.actor, "feat", null, resistance, entity);
          }

          if (!foundItem) {
            await createItem(entity);
          }
        } else {
          let { amount, type1, operator, type2 } = re.groups;
          console.log(
            sbcUtils.translate("parser.resistance.parseInfo"),
            amount,
            type1,
            operator,
            type2,
            type1?.search(damageTypesRegex()),
            type2?.search(damageTypesRegex())
          );

          // If we found one or two types and at least one of them is a valid damage type,
          // then we can add a new DR entry
          if ((type1 && type1.match(damageTypesRegex())) || (type2 && type2.match(damageTypesRegex()))) {
            type1 = type1?.match(damageTypesRegex())?.[0];
            type2 = type2?.match(damageTypesRegex())?.[0];
            const types = [type1, type2].filter((x) => !!x).map((x) => pf1.utils.createTag(x));

            eresValues.push({
              amount: amount,
              types: types,
              operator: operator === "or",
            });
            this.app.processData.notes.defense.resist.push(
              [
                this.getDamageTypePhrase(types[0]),
                ...(types[1] ? [" ", sbcUtils.translate(operator), " ", this.getDamageTypePhrase(types[1])] : []),
                " ",
                amount,
              ].join("")
            );
          }
          // It's a custom reduction, as there is no place for that, just put it into custom reductions
          else {
            eresCustom += `${resistance.capitalize()};`;
            this.app.processData.notes.defense.resist.push(resistance.capitalize());
          }
        }
      }
    }

    await sbcInstance.actor.update({
      "system.traits.cres": cresConditions.replace(/;$/, ""),
      "system.traits.eres.value": eresValues,
      "system.traits.eres.custom": eresCustom.replace(/;$/, ""),
    });

    return true;
  }

  /**
   * Get the corresponding damage type phrase for the damage type
   *
   * @param {string} damageType       The key of the damage type
   * @returns {string}                The phrase for the damage type
   */
  getDamageTypePhrase(damageType) {
    if (pf1.config.damageResistances[damageType]) {
      return pf1.config.damageResistances[damageType];
    }

    const materialTypeLabels = pf1.registry.materials.getLabels();
    if (materialTypeLabels[damageType]) {
      return materialTypeLabels[damageType];
    }

    const damageTypeLabels = pf1.registry.damageTypes.getLabels();
    if (damageTypeLabels[damageType]) {
      return damageTypeLabels[damageType];
    }

    return "";
  }
}
