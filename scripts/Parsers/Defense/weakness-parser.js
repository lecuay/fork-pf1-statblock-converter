import { AbstractParser } from "../abstract-parser.js";
import { damageTypesRegex } from "../../sbcRegex.js";

// eslint-disable-next-line no-unused-vars
import * as sbcTypes from "../../sbcTypes.js";

// Parse Weaknesses / Vulnerabilities
export default class WeaknessParser extends AbstractParser {
  /**
   * @inheritDoc
   */
  get parserGroup() {
    return "Parser/Defense";
  }

  /**
   * @inheritDoc
   */
  get parserType() {
    return "weakness";
  }

  /**
   * Parses a weakness string
   *
   * @param {string} value        The value to parse
   * @param {number} _line         The line number
   * @param {string} _type        The type of value
   * @returns {Promise<boolean>}  Always true, unless an error occurs
   */
  async _parse(value, _line, _type = undefined) {
    const rawInput = value.replace(/(^[,;\s]*|[,;\s]*$)/g, "");
    const input = this.splitText(rawInput);

    this.app.processData.notes.defense["weakness"] = rawInput;

    /** @type {sbcTypes.SBC} */
    const sbcInstance = window.SBC;
    /** @type {Array<string>} */
    let dvValues = sbcInstance.actor.system.traits.dv;

    console.log(input);
    for (let i = 0; i < input.length; i++) {
      let weakness = input[i].replace(/Effects/gi, "").trim();
      const matchedWeakness = weakness.match(damageTypesRegex());

      // it's a damage weakness / vulnerability
      if (matchedWeakness) dvValues.push(matchedWeakness[0]);
      else if (
        !sbcInstance.actor.itemTypes.feat.find(
          (item) => item.system.subType === "misc" && item.name.toLowerCase() === weakness.toLowerCase()
        )
      ) {
        // It's a custom weakness / vulnerability
        dvValues.push(weakness.capitalize());
      }
    }

    await window.SBC.actor.update({
      "system.traits.dv": dvValues,
    });

    return true;
  }
}
