import { sbcUtils } from "../../sbcUtils.js";
import { AbstractParser } from "../abstract-parser.js";

// Parse Spell Resistance
export default class SRParser extends AbstractParser {
  /**
   * @inheritDoc
   */
  get parserGroup() {
    return "Parser/Defense";
  }

  /**
   * @inheritDoc
   */
  get parserType() {
    return "spellResistance";
  }

  /**
   * Parses a spell resistance string
   *
   * @param {string} value        The value to parse
   * @param {number} _line         The line number
   * @param {string} _type        The type of value
   * @returns {Promise<boolean>}  Always true, unless an error occurs
   */
  async _parse(value, _line, _type = undefined) {
    const rawInput = value.replace(/(^[,;\s]*|[,;\s]*$)/g, "");
    const input = sbcUtils.parseSubtext(rawInput);

    let srTotal = input[0];
    let srContext = "";
    let srNotes = "";

    if (input[1]) {
      srContext = input[1];
      srNotes = srContext;
    } else if (/\s*([-+]?\d+)\s+(.*)/.test(srTotal)) {
      [srTotal, srContext] = srTotal.split(/\s*([-+]?\d+)\s+(.*)/).slice(1);
      srNotes = srContext;
    }

    this.app.processData.miscNotes.srNotes = srNotes;
    this.app.processData.miscNotes.sr = srTotal.toString();
    this.app.processData.notes.defense.srNotes = srNotes;
    this.app.processData.notes.defense.sr = srTotal.toString();
    // await window.SBC.actor.update({
    //   "system.attributes.sr.formula": srTotal.toString(),
    //   "system.attributes.srNotes": srNotes,
    // });

    return true;
  }
}
