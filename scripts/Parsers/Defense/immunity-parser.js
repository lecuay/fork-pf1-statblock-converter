import { sbcUtils } from "../../sbcUtils.js";
import { AbstractParser } from "../abstract-parser.js";
import {
  conditionRegistryTypesRegex,
  conditionTypesRegex,
  creatureTypeRegex,
  damageTypesRegex,
} from "../../sbcRegex.js";

// Parse Immunities
export default class ImmunityParser extends AbstractParser {
  /**
   * @inheritDoc
   */
  get parserGroup() {
    return "Parser/Defense";
  }

  /**
   * @inheritDoc
   */
  get parserType() {
    return "immunity";
  }

  /**
   * Parses an immunity string
   *
   * @param {string} value        The value to parse
   * @param {number} _line         The line number
   * @param {string} _type        The type of value
   * @returns {Promise<boolean>}  Always true, unless an error occurs
   */
  async _parse(value, _line, _type = undefined) {
    const rawInput = value.replace(/(^[,;\s]*|[,;\s]*$)/g, "");
    const input = this.splitText(rawInput);

    this.app.processData.notes.defense.immune = [];

    // Get the current immunities
    /**
     * @type {{ci: Array<string>, di: Array<string>, ciCustom: Array<string>}}
     */
    const immunities = {
      ci: window.SBC.actor.system.traits.ci.base,
      di: window.SBC.actor.system.traits.di.base,
    };

    // Loop through all the immunities in the input
    for (let immunity of input) {
      immunity = this.sanitizeInput(immunity.replace(/(?<!Death )Effects/gi, "").trim());

      // Check if it's one of the system's condition immunities, or
      // if it's `fatigue` due to a mismatch in system terminology
      const isFatigue = immunity.toLowerCase().includes("fatigue");
      if (isFatigue || immunity.search(conditionTypesRegex()) !== -1) {
        if (isFatigue) immunity = "fatigued";

        const immunityKey = sbcUtils.getKeyByValue(pf1.config.conditionTypes, immunity);
        immunities.ci.push(sbcUtils.camelize(immunityKey));
        this.app.processData.notes.defense.immune.push(pf1.config.conditionTypes[immunityKey]);
      }
      // Check if the immunity is a damage type
      else if (immunity.replace(/(\s*energy)/i, "").search(damageTypesRegex(true)) !== -1) {
        const immunityKey = sbcUtils.getKeyByValue(
          pf1.registry.damageTypes.getLabels(),
          immunity.replace(/(\s*energy)/i, "")
        );
        immunities.di.push(sbcUtils.camelize(immunityKey));
        this.app.processData.notes.defense.immune.push(
          pf1.registry.damageTypes.getLabels()[immunityKey] + (immunity.match(/ energy/) ? " Energy" : "")
        );
      }
      // Check if the immunity is in the condition registry
      else if (immunity.search(conditionRegistryTypesRegex()) !== -1) {
        const immunityKey = sbcUtils.getKeyByValue(pf1.registry.conditions.getLabels(), immunity);
        immunities.ci.push(sbcUtils.camelize(immunityKey));
        this.app.processData.notes.defense.immune.push(pf1.registry.conditions.getLabels()[immunityKey]);
      }
      // Check the immunity list for this creature type
      else if (immunity.search(creatureTypeRegex()) !== -1) {
        const creatureType = sbcUtils.haystackSearch(window.SBC.config.creatureTypes, immunity);
        const racialImmunities = window.SBC.config.immunities[creatureType] || {};
        if (racialImmunities.condition) {
          immunities.ci.push(...racialImmunities.condition);
        }
        if (racialImmunities.damage) {
          immunities.di.push(...racialImmunities.damage);
        }
        if (racialImmunities.custom) {
          immunities.ci.push(...racialImmunities.custom);
        }

        this.app.processData.notes.defense.immune.push(immunity.capitalize());
      }
      // Otherwise, assume it's a custom immunity
      else {
        immunities.ciCustom.push(immunity.capitalize());
        this.app.processData.notes.defense.immune.push(immunity.capitalize());
      }
    }

    await window.SBC.actor.update({
      "system.traits.ci": immunities.ci,
      "system.traits.di": immunities.di,
    });

    return true;
  }

  /**
   * Sanitize the immunities for cases where the statblock term
   * doesn't match what the system has.
   *
   * @param {string} input  The string to sanitize
   * @returns {string}      The sanitized string
   *
   * @example
   * If the input is "fatigue", the sanitized string is "fatigued"
   *
   * @example
   * If the input is "paralysis", the sanitized string is "paralyzed"
   */
  sanitizeInput(input) {
    const conditions = {
      fatigue: "fatigued",
      paralysis: "paralyzed",
      confusion: "confused",
      petrification: "petrified",
      stunning: "stunned",
    };

    for (const condition of Object.keys(conditions)) {
      input = input.replace(new RegExp(condition, "gi"), conditions[condition]);
    }
    return input.trim();
  }
}
