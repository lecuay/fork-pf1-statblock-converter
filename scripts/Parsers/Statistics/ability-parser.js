import { sbcUtils } from "../../sbcUtils.js";
import { parseValueToDocPath } from "../../sbcParser.js";
import { AbstractParser } from "../abstract-parser.js";

// Parse Ability Values and Mods
export default class AbilityParser extends AbstractParser {
  /**
   *
   * @param {sbcTypes.InputDialog} app          The SBC Parser application
   * @param {Array<string>} targetValueFields   The fields to update with the parsed value
   * @param {Array<string>} targetModFields     The fields to update with the parsed modifier
   * @param {string} supportedType              The supported type of the value
   */
  constructor(app, targetValueFields, targetModFields, supportedType) {
    super(app);
    this.targetValueFields = targetValueFields;
    this.targetModFields = targetModFields;
    this.supportedType = supportedType;
  }

  async parse(value, line) {
    sbcUtils.log(sbcUtils.translate("parser.ability.status", { value, fields: this.targetValueFields.join(", ") }));

    // Check if the given value is one of the supported ones
    if (typeof value === this.supportedType) {
      try {
        for (const valueField of this.targetValueFields) {
          await parseValueToDocPath(window.SBC.actor, valueField, value);
        }
        return true;
      } catch (err) {
        this.throwError(err, "parser.ability.error", value, line, {
          fields: this.targetValueFields.join(", "),
          modValue: sbcUtils.getModifier(value),
          modFields: this.targetModFields.join(", "),
        });
        return false;
      }
    } else {
      this.throwError(undefined, "parser.ability.typeError", value, line, { type: this.supportedType });
      return false;
    }
  }
}
