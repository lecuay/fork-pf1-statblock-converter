import { sbcUtils } from "../../sbcUtils.js";
import { AbstractParser } from "../abstract-parser.js";
import { racialModifiersRegex, skillsRegex } from "../../sbcRegex.js";

// Parse Skills
export default class SkillParser extends AbstractParser {
  /**
   * @inheritDoc
   */
  get parserGroup() {
    return "Parser/Statistics";
  }

  /**
   * @inheritDoc
   */
  get parserType() {
    return "skill";
  }

  /**
   * Parse a string as skills
   *
   * @param {string} value        The value to parse
   * @param {number} line         The line number
   * @param {string} _type        The type of value
   * @returns {Promise<boolean>}  Always true, unless an error occurs
   * @private
   */
  async _parse(value, line, _type = undefined) {
    // Reverse the skills so that we can handle Racial Modifiers first.
    let [skills, racialSkills] = value.split(/racial (?:modifiers?|bonus)/i);
    skills = this.splitText(skills);
    racialSkills = racialSkills ? this.splitText(racialSkills) : [];

    this.app.processData.notes.statistics.skills = [];

    const maxRanks = window.SBC.actor.system.attributes.hd.total;
    const classSkills = [];
    const racialModifiers = {};
    const skillChanges = {};
    const skillChangesAfter = {};

    // Check the current Items for classes and save, if a skill is a classSkill
    const currentItems = window.SBC.actor.items.contents;

    await sbcUtils.runParallelOps(currentItems, async (item) => {
      let skillKeys = Object.keys(item.system.classSkills ?? {});

      // Loop through the skills
      for (let j = 0; j < skillKeys.length; j++) {
        let currentSkill = skillKeys[j];
        if (item.system.classSkills[currentSkill] === true) {
          if (!classSkills.includes(currentSkill)) {
            classSkills.push(currentSkill);
          }
        }
      }

      // Loop through the item's changes, if any
      await this.updateSkillChanges(item, skillChanges);
    });

    // Setup Counters
    let countOfCustomSkills = 0;
    let countOfSubSkills = {
      art: 1,
      crf: 1,
      lor: 1,
      prf: 1,
      pro: 1,
    };

    // Replace common Skill shorthands and misswordings
    const formatSkill = (rawSkill) =>
      rawSkill
        .replace(/\bEnter Choice\b/gim, "any one")
        .replace(/Arcane/gim, "Arcana")
        .replace(/\bPer\./gim, "Perception")
        .replace(/S\. Motive/gim, "Sense Motive")
        .replace(/\bLing\./gim, "Linguistics");

    // Process skills for racial modifiers
    for (const racialSkill of racialSkills) {
      let rawSkill = formatSkill(racialSkill);

      let racialSkillMatch = rawSkill.match(racialModifiersRegex());
      if (!racialSkillMatch) {
        continue;
      }

      const parsedValue = parseInt(racialSkillMatch.groups.value);
      racialModifiers[racialSkillMatch.groups.name.toLowerCase()] = {
        name: racialSkillMatch.groups.name,
        number: parsedValue,
        prefixedValue: sbcUtils.prefixNumber(parsedValue),
        context: racialSkillMatch.groups.context ?? "",
      };
    }
    this.app.processData.notes.statistics.racialSkillModifiers = Object.keys(racialModifiers).length
      ? Object.values(racialModifiers)
      : null;

    // Now process the normal skills
    for (let i = 0; i < skills.length; i++) {
      if (!skills[i]) continue;
      let rawSkill = formatSkill(skills[i]);

      if (rawSkill.match(/[+-]?\s*\d+(?![^(]*\))/g)?.length > 1) {
        let missedCommas = rawSkill.split(/(?<=[-+]\d+) /);
        rawSkill = missedCommas.splice(0, 1)[0];
        skills.push(...missedCommas);
      }

      let skill = null;
      let knowledgeSubSkills = [
        "Arcana",
        "Dungeoneering",
        "Engineering",
        "Geography",
        "History",
        "Local",
        "Nature",
        "Nobility",
        "Planes",
        "Religion",
      ];

      // Check if there are multiple subskills
      if (
        rawSkill.search(/knowledge|perform|craft|profession|lore|artistry/i) !== -1 &&
        rawSkill.search(/,|\band\b|&/g) !== -1
      ) {
        // If there are, generate new skills and push them to the array of skills

        let tempSkill = sbcUtils.parseSubtext(rawSkill);

        let skillName = tempSkill[0];
        let skillModifier = tempSkill[2][0];

        let subSkills = tempSkill[1].split(/,|\band\b|&/g);

        for (let j = 0; j < subSkills.length; j++) {
          let subSkill = subSkills[j].trim();
          let newSkill = skillName + " (" + subSkill + ") " + skillModifier;
          skills.push(newSkill);
        }

        continue;
      }

      if (rawSkill.match(/\bknowledge\b/i) && rawSkill.match(/\bany\b/i)) {
        // If it's a knowledge skill with "any" as subskill
        // Find out how many, as most of the time its denoted as "any two"/"any three"

        let tempSkill = sbcUtils.parseSubtext(rawSkill);

        let skillName = tempSkill[0];
        let skillModifier = tempSkill[2][0];

        const stringOfKnowledgeSubskills = tempSkill[1].match(/\bany\b (.*)/i)[1];
        const numberOfKnowledgeSubskills =
          ["one", "two", "three", "four", "five", "six", "even", "eight", "nine"].indexOf(stringOfKnowledgeSubskills) +
          1;

        // Pick Subskills at random
        let alreadyPickedSubskills = "";

        for (let i = 0; i < numberOfKnowledgeSubskills; i++) {
          let randomSubSkill = Math.floor(Math.random() * 10);
          let searchString = new RegExp(knowledgeSubSkills[randomSubSkill], "i");

          if (alreadyPickedSubskills.search(searchString) === -1 && !skills.includes(searchString)) {
            let subSkill = knowledgeSubSkills[randomSubSkill];
            let newSkill = skillName + " (" + subSkill + ") " + skillModifier;
            skills.push(newSkill);
            alreadyPickedSubskills += newSkill;
          } else {
            i--;
          }
        }

        continue;
      }

      if (rawSkill.match(/\bknowledge\b/i) && rawSkill.match(/\ball\b/i)) {
        // If it's a knowledge skill with "all" as subskill

        const tempSkill = sbcUtils.parseSubtext(rawSkill);
        const skillName = tempSkill[0];
        const skillModifier = tempSkill[2][0];

        for (let j = 0; j < knowledgeSubSkills.length; j++) {
          const knowledgeSubSkill = knowledgeSubSkills[j].trim();
          const newSkill = skillName + " (" + knowledgeSubSkill + ") " + skillModifier;
          skills.push(newSkill);
        }

        continue;
      }

      skill = sbcUtils.parseSubtext(rawSkill);

      try {
        let skillName = skill[0].replace(/[+-]?\s*\d+/g, "").trim();
        let skillTotal = skill[0].replace(skillName, "").replace(/\s/g, "");
        let subSkill = "";
        let skillContext = "";

        // Check, if there is a subskill
        if (skill[1]) {
          if (skill[1].startsWith("+")) {
            skillContext = skill[1];
          } else {
            subSkill = skill[1];
          }
        }

        // Check, if there are restValues after separating subtext
        if (skill[2]) {
          // If the rest includes two values, the first one is the skillTotal and the second the context note
          if (skillTotal === "" && skill[2][1]) {
            skillContext = skill[2][1];
          }
          skillTotal = skill[2][0];
        }

        // Check if its one of the supported skills, otherwise try to parse it as a custom skill
        const searchSkillWithSubSkill = skillName + " (" + subSkill + ")";
        let skillKey = "";

        if (skillName.match(skillsRegex())) {
          // Supported Skills without Subskills
          skillKey = sbcUtils.getKeyByValue(pf1.config.skills, skillName);
        } else if (searchSkillWithSubSkill.match(skillsRegex())) {
          // Supported Skills with Subskills
          skillName = searchSkillWithSubSkill;
          skillKey = sbcUtils.getKeyByValue(pf1.config.skills, searchSkillWithSubSkill);
        } else {
          // Custom Skills not included in the system
          skillKey = "skill";
        }

        const size = window.SBC.actor.system.traits.size.base;
        let sizeMod = 0;

        // As long as it's not a custom skill ...
        if (skillKey !== "skill") {
          // Seems the temporary actors does not calculate the mod or if it's a classSkill beforehand, so we need to do that manually
          const skillAbility = window.SBC.actor.system.skills[skillKey].ability;
          const skillAbilityMod = window.SBC.actor.system.abilities[skillAbility].mod;

          switch (skillKey) {
            case "fly":
              sizeMod = pf1.config.sizeFlyMods[size];
              break;

            case "ste":
              sizeMod = pf1.config.sizeStealthMods[size];
              break;

            default:
              break;
          }

          const skillInfo = {
            name: skillName,
            key: skillKey,
            total: +skillTotal,
            abilityMod: +skillAbilityMod,
            classSkill: classSkills.includes(skillKey),
            sizeMod: +sizeMod,
            racialModifiers,
            skillChanges,
            acp: window.SBC.actor.system.skills[skillKey].acp,
            acpTotal: window.SBC.actor.system.attributes.acp.total,
            maxRanks,
          };

          const skillRanks = this.calculateSkillRanks(skillInfo);
          // Check if there are racial modifiers to add to the context
          const racialNote = racialModifiers[skillName.toLowerCase()]?.context.length
            ? `${skillContext ? "; " : ""}${racialModifiers[skillName.toLowerCase()].prefixedValue}${racialModifiers[skillName.toLowerCase()].context}`
            : "";
          if (skillKey.search(/(art|crf|lor|prf|pro)/) === -1) {
            // IF ITS NOT A SKILL WITH SUBSKILLS
            await window.SBC.actor.update({
              [`system.skills.${skillKey}.rank`]: skillRanks,
            });

            // Process things again as a double-check (and to account for updated formulas)
            skillChangesAfter[skillKey] = 0;
            await sbcUtils
              .runParallelOps(currentItems, async (item) => {
                // Loop through the item's changes, if any
                await this.updateSkillChanges(item, skillChangesAfter);
              })
              .then(async () => {
                const subSkillInfo = Object.assign({}, skillInfo, { skillChanges: skillChangesAfter });
                const skillRanks2 = this.calculateSkillRanks(subSkillInfo);

                if (skillRanks !== skillRanks2) {
                  await window.SBC.actor.update({
                    [`system.skills.${skillKey}.rank`]: skillRanks2,
                  });
                }
              });

            // Add Data to conversionValidation
            this.app.processData.characterData.conversionValidation.skills[skillKey] = {
              total: +skillTotal,
              context:
                skillContext.replace(/\+?(-?\d+)/g, (value) => sbcUtils.prefixNumber(+value - +skillTotal)) +
                racialNote,
            };

            this.app.processData.notes.statistics.skills.push({
              name: pf1.config.skills[skillKey] || skillName,
              prefixedValue: skillTotal >= 0 ? "+" + +skillTotal : skillTotal,
              context: skillContext,
            });
          } else {
            // IF ITS A SKILL WITH SUBSKILLS (e.g. Art, Craft, etc.)
            const subSkillKey = skillKey + +countOfSubSkills[skillKey];
            const subSkillInfo = Object.assign({}, skillInfo, { key: subSkillKey, skillChanges: skillChangesAfter });

            // WIP FIND A WAY TO APPEND INSTEAD OF OVERWRITE THE SUBSKILLS
            await window.SBC.actor.update({
              [`system.skills.${skillKey}.subSkills.${subSkillKey}`]: {
                ability: window.SBC.actor.system.skills[skillKey].ability,
                acp: window.SBC.actor.system.skills[skillKey].acp,
                cs: window.SBC.actor.system.skills[skillKey].cs,
                name: subSkill.capitalize(),
                rank: skillRanks,
                rt: window.SBC.actor.system.skills[skillKey].rt,
              },
            });

            // Process things again as a double-check (and to account for updated formulas)
            skillChangesAfter[skillKey] = 0;
            await sbcUtils
              .runParallelOps(currentItems, async (item) => {
                // Loop through the item's changes, if any
                await this.updateSkillChanges(item, skillChangesAfter);
              })
              .then(async () => {
                const skillRanks2 = this.calculateSkillRanks(subSkillInfo);

                if (skillRanks !== skillRanks2) {
                  await window.SBC.actor.update({
                    [`system.skills.${skillKey}.subSkills.${subSkillKey}`]: {
                      ability: window.SBC.actor.system.skills[skillKey].ability,
                      acp: window.SBC.actor.system.skills[skillKey].acp,
                      cs: window.SBC.actor.system.skills[skillKey].cs,
                      name: subSkill.capitalize(),
                      rank: skillRanks2,
                      rt: window.SBC.actor.system.skills[skillKey].rt,
                    },
                  });
                }
              });

            // Add Data to conversionValidation
            this.app.processData.characterData.conversionValidation.skills[subSkillKey] = {
              name: subSkill.capitalize(),
              total: +skillTotal,
              context: skillContext.replace(/\d+/g, (value) => +value - +skillTotal),
            };

            this.app.processData.notes.statistics.skills.push({
              name: pf1.config.skills[skillKey] + ` (${subSkill.capitalize()})`,
              prefixedValue: skillTotal >= 0 ? "+" + +skillTotal : skillTotal,
              context: skillContext,
            });

            countOfSubSkills[skillKey]++;
          }
        } else {
          // if it's a custom skill ...
          let customSkillKey = "skill";

          if (countOfCustomSkills > 0) {
            customSkillKey = "skill" + (+countOfCustomSkills + 1);
          }

          let defaultAbilityMod = window.SBC.actor.system.abilities["int"].mod;

          let skillRanks = +skillTotal - +defaultAbilityMod;
          if (skillChanges[customSkillKey] !== null && skillChanges[customSkillKey] !== undefined) {
            skillRanks -= skillChanges[customSkillKey];
          }

          await window.SBC.actor.update({
            [`system.skills.${customSkillKey}`]: {
              ability: "int",
              acp: false,
              background: false,
              cs: false,
              custom: true,
              name: skillName,
              rank: skillRanks,
              rt: false,
            },
          });

          // Add Data to conversionValidation
          this.app.processData.characterData.conversionValidation.skills[customSkillKey] = {
            name: skillName,
            total: +skillTotal,
            context: skillContext.replace(/\d+/g, (value) => +value - +skillTotal),
          };

          this.app.processData.notes.statistics.skills.push({
            name: skillName,
            prefixedValue: skillTotal >= 0 ? "+" + +skillTotal : skillTotal,
            context: skillContext,
          });

          countOfCustomSkills++;
        }

        if (skillName.toLowerCase() === "perception") {
          this.app.processData.notes.base.perception = skillTotal >= 0 ? "+" + +skillTotal : skillTotal;
        }
      } catch (err) {
        this.throwError(undefined, "parser.skill.failedToParse", {
          value: skill,
        });
        return false;
      }
    }

    // If all skills were parsed correctly, return true
    return true;
  }

  /**
   * Check the item for any skill-based changes
   *
   * @param {import("../../../pf1/module/documents/item/item-pf.mjs").ItemPF} item  The item to update the skill changes for
   * @param {{string: number}} skillChanges   A dictionary of skill changes
   * @returns {Promise<void>}                 A Promise that resolves when the skill changes have been updated
   * @private
   * @async
   */
  async updateSkillChanges(item, skillChanges) {
    if (!item.system.changes) return;

    await sbcUtils.runParallelOps(item.system.changes, async (change) => {
      let skillKey = "";
      if (/skill\.(...)\.subSkills/i.test(change.target)) {
        // We are a subskill, and custom
        skillKey = change.target.match(/skill\....\.subSkills\.(.*)/i)[1];
      } else if (/skill\./i.test(change.target)) {
        // We are a normal skill
        skillKey = change.target.match(/skill\.(.*)/i)[1];
      }

      if (skillKey !== "") {
        const processedFormula = await Roll.fromTerms(
          Roll.parse(change.formula, window.SBC.actor.getRollData())
        ).evaluate();
        const processedFormulaTotal = processedFormula?.total ?? +change.formula; // change.value;

        if (skillChanges[skillKey] !== null && skillChanges[skillKey] !== undefined) {
          skillChanges[skillKey] += processedFormulaTotal;
        } else skillChanges[skillKey] = processedFormulaTotal;
      }
    });
  }

  /**
   * Calculate the skill ranks
   *
   * @param {object} skillInfo                    The skill information
   * @param {string} skillInfo.name               The name of the skill
   * @param {string} skillInfo.key                The key of the skill
   * @param {number} [skillInfo.total]            The total skill value, which we need to achieve
   * @param {number} [skillInfo.abilityMod]       The applicable ability modifier for this skill
   * @param {boolean} [skillInfo.classSkill]      Whether this skill is a class skill
   * @param {number} [skillInfo.sizeMod]          The applicable size modifier for this skill
   * @param {object} [skillInfo.racialModifiers]  A list of racial modifiers this actor has
   * @param {object} [skillInfo.changes]          A list of changes pertaining to the skill
   * @param {boolean} [skillInfo.acp]             Does this skill used the Armor Check Penalty?
   * @param {number} [skillInfo.acpTotal]         The total Armor Check Penalty
   * @param {number} [skillInfo.maxRanks]         The maximum skill ranks the actor can possess
   * @returns {number}                            The calculated skill ranks
   * @private
   */
  calculateSkillRanks({
    name,
    key,
    total = 0,
    abilityMod = 0,
    classSkill = false,
    sizeMod = 0,
    racialModifiers = {},
    changes = {},
    acp = false,
    acpTotal = 0,
    maxRanks = 0,
  } = {}) {
    let skillRanks = +total - +abilityMod - (classSkill ? pf1.config.classSkillBonus : 0) - +sizeMod;

    // Factor ACP into the total, if it's applicable
    if (acp && acpTotal) {
      skillRanks += acpTotal;
    }

    if (racialModifiers[name.toLowerCase()] && racialModifiers[name.toLowerCase()].context.length === 0) {
      skillRanks -= racialModifiers[name.toLowerCase()].number;
    }

    if (changes[key] !== null && changes[key] !== undefined) {
      skillRanks -= changes[key];
    }

    sbcUtils.log(
      `Skill: ${name} | Total: ${total} | Ability: ${abilityMod} | Class: ${classSkill} | Size: ${sizeMod} | Racial: ${racialModifiers[name.toLowerCase()]?.number ?? 0} | Change: ${changes[key] ?? 0} | Ranks: ${skillRanks}`
    );

    return Math.clamp(skillRanks, 0, maxRanks);
  }
}
