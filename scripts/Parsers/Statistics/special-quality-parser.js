import { sbcUtils } from "../../sbcUtils.js";
import { AbstractParser } from "../abstract-parser.js";
import { createItem } from "../../sbcParser.js";

// Special Qualities Parser
export default class SpecialQualityParser extends AbstractParser {
  /**
   * @inheritDoc
   */
  get parserGroup() {
    return "Parser/Statistics";
  }

  /**
   * @inheritDoc
   */
  get parserType() {
    return "specialQuality";
  }

  /**
   * Parse a string as special quality
   *
   * @param {string} value        The value to parse
   * @param {number} line         The line number
   * @param {string} _type        The type of value
   * @returns {Promise<boolean>}  Always true, unless an error occurs
   */
  async _parse(value, line, _type = undefined) {
    const specialQualities = this.fixSplitGroup(this.splitText(value));
    const type = "misc";

    for (let i = 0; i < specialQualities.length; i++) {
      const specialQuality = {
        name: sbcUtils.translate("parser.specialQuality.placeholderName", { value: specialQualities[i] }),
        type: type,
        desc: sbcUtils.translate("parser.specialQuality.placeholderDescription"),
      };
      const placeholder = await sbcUtils.generatePlaceholderEntity(specialQuality, line);
      await createItem(placeholder);
    }

    return true;
  }
}
