import { sbcUtils } from "../../sbcUtils.js";
import { parseValueToPath } from "../../sbcParser.js";
import { AbstractParser } from "../abstract-parser.js";

/**
 * NotesParser is a class that writes a given value into all
 * fields defined in parserMapping
 *
 * @augments AbstractParser
 */
export default class NotesParser extends AbstractParser {
  /**
   * Parses values into a child of this.app.processData.notes, which gets read when creating the styled preview statblock
   *
   * @param {sbcTypes.InputDialog} app The InputDialog instance.
   * @param {Array.<string>} targetField Foundry Actor's field where this is pointing to.
   */
  constructor(app, targetField) {
    super(app);

    if (!targetField) {
      throw Error(sbcUtils.translate("parser.errors.missingArgument", { arg: "targetField" }));
    }
    this.targetField = targetField;
  }

  // noinspection JSCheckFunctionSignatures: This is an intentional override of the base signature
  /**
   * @param {string|number} value The value to parse.
   * @param {number} line The line where the parsing is occurring.
   * @returns {Promise.<boolean>} `true` if correctly parsed.
   */
  async parse(value, line) {
    if (value === undefined) {
      throw Error(sbcUtils.translate("parser.errors.missingArgument", { arg: "value" }));
    }
    if (line === undefined) {
      throw Error(sbcUtils.translate("parser.errors.missingArgument", { arg: "line" }));
    }

    sbcUtils.log(
      sbcUtils.translate("parser.notes.status", {
        value: value,
        targetField: this.targetField,
      })
    );
    this.app.processData.notes[value] = value;

    try {
      for (const field of this.targetField) {
        await parseValueToPath(this.app.processData.notes, field, value);
      }
      return true;
    } catch (err) {
      this.throwError(err, "parser.notes.error", value, line, { targetField: this.targetField });
      return false;
    }
  }
}
