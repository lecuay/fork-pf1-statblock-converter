import { sbcUtils } from "../../sbcUtils.js";
import { AbstractParser } from "../abstract-parser.js";
import { createItem } from "../../sbcParser.js";
import {
  abilityDamageRegex,
  abilityDCregex,
  abilityRangeRegex,
  escapeForRegex,
  frequencyWithPeriodRegex,
} from "../../sbcRegex.js";

// eslint-disable-next-line no-unused-vars
import * as sbcTypes from "../../sbcTypes.js";

/**
 * EntityParser is a class that parses entities of a given type
 * mainly used for feats and abilities
 *
 * @augments AbstractParser
 */
export default class EntityParser extends AbstractParser {
  // noinspection JSCheckFunctionSignatures
  /**
   * Parses the given value and creates entities for it
   *
   * @param {string} value              The value to parse
   * @param {number} line               The line number
   * @param {string} pack               The pack to search in
   * @param {string} type               The type of the entity
   * @param {string} subtype            The subtype of the entity
   * @param {boolean} processClusters   Whether to process clusters
   * @returns {Promise<[boolean, string[]]>}        Always true, unless an error occurs
   */
  async parse(value, line, pack, type = null, subtype = null, processClusters = true) {
    /** @type {sbcTypes.SBC} */
    const sbcInstance = window.SBC;
    const compendiumOps = sbcInstance.compendiumSearch;
    sbcUtils.log(sbcUtils.translate("parser.entity.status", { value, pack }));

    let logEntities = [];
    try {
      let compendium = [];
      if (typeof pack === "string") {
        compendium.push("pf1." + pack);
      } else {
        pack.forEach((element) => {
          compendium.push("pf1." + element);
        });
      }

      let array = this.splitText(value, processClusters);
      array = this.fixSplitGroup(array);
      this.processForChoices(array);

      for (let i = 0; i < array.length; i++) {
        let input = array[i].trim().replaceAll(/\*/g, "").trim();

        const frequencyMatch = frequencyWithPeriodRegex().exec(input);
        if (frequencyMatch) {
          input = input.replace(` ${frequencyMatch[0]}`, "");
          if (input.indexOf("(")) {
            input = input.replace("(", `(${frequencyMatch[0]}, `).replace(/,\s\)/g, ")");
          } else {
            input += ` (${frequencyMatch[0]})`;
          }
        }

        // Catch instances of a stray ')' at the end of the input
        if (/(?!\()\)$/.test(input)) {
          input = input.replace(/\)$/, "");
        }
        // Catch instances of a stray '(' in the input
        if (/\((?!\))/.test(input)) {
          input += ")";
        }
        input = input.replace(/\[/g, "(").replace(/]/g, ")").replaceAll(/,\s+,/g, ",");
        if (/sneak attack \+\d+d\d+ plus \d+ bleed/i.test(input)) {
          input = input.replace(/ plus \d+ bleed/i, "");
        }

        let inputSeparated = input.split(/\s/);
        console.log(`Looking at ${input}.`);

        if (/\+?\d+(d\d+)?/.test(inputSeparated[inputSeparated.length - 1])) {
          inputSeparated.splice(inputSeparated.length - 1, 1);
        }

        if (type === "feat") {
          input.replace(/B$/, "");
          if (input.endsWith("M")) {
            input = input.substring(0, input.length - 1);
            const mythicName = `${input} (Mythic)`;

            // Check if the mythic version is already in the array, otherwise add it
            if (!array.find((f) => f === mythicName)) array.push(mythicName);

            // If the non-mythic version has already been processed, skip this one
            if (array.slice(0, Math.max(0, i - 1)).find((f) => f === input)) continue;
          } else {
            // const mythicMatch = input.match(/(\w+)M\b\s*\(.*(?!Mythic)\)/gi);
            const mythicMatch = input.match(/(.*)M \((.*)\)/);
            if (mythicMatch) {
              input = `${mythicMatch[1]} (${mythicMatch[2]})`;
              const mythicName = `${mythicMatch[1]} (Mythic) (${mythicMatch[2]})`;

              // Check if the mythic version is already in the array, otherwise add it
              if (!array.find((f) => f === mythicName)) array.push(mythicName);

              // If the non-mythic version has already been processed, skip this one
              if (array.slice(0, Math.max(0, i - 1)).find((f) => f === mythicMatch[1])) continue;
            }
          }
        }

        let shortName = sbcUtils
          .parseSubtext(`${input.replace(/\+*\d+$/g, "").trim()}`)[0]
          .replace(/(\d+[ -]ft\.$)/i, "")
          .trim();
        let altName = `${inputSeparated.join(" ").trim()}`;
        let altName2 = `${input.match(/\((.*)\)/) ? input.match(/\((.*)\)/)[1] : ""}`;
        if (frequencyMatch) {
          altName = altName.replace(frequencyMatch[0], "").replace(/,\s*/, "").trim();
          altName2 = altName2.replace(frequencyMatch[0], "").replace(/,\s*/, "").trim();
        }
        let altName3 = altName2
          .replace(/(\+?\d+?)$/, "")
          .trim()
          .replace(/^(.*?action;)/i, "")
          .trim();

        if (altName2.match(/^Mythic\) \(/i)) {
          shortName += " (Mythic)";
        }

        let searchEntity = {
          search: new Set(), // The search strings
          type: pack,
          item: input,
        };
        searchEntity.search = sbcUtils.createSearchSet([altName3, altName2, altName, input, shortName], true);

        if (/channel\s*(positive|negative)\s*energy/i.test(input)) {
          searchEntity.search.add("channel energy");
        }

        // If the input is found in one of the compendiums, generate an entity from that
        /** @type {import ("../../../pf1/module/documents/item/item-pf.mjs").ItemPF} */
        let entity = null;
        if (typeof type !== "string") {
          entity = await compendiumOps.findEntityInCompendia(searchEntity, {
            itemTypes: type,
            itemSubTypes: Array.isArray(subtype) ? subtype : [subtype],
            classes: this.app.processData.characterData.classes,
            race: this.app.processData.characterData.race,
          });

          if (!entity) {
            entity = await compendiumOps.findEntityInCompendia(searchEntity, {
              itemTypes: type,
              itemSubTypes: ["classFeat", "misc"],
              classes: this.app.processData.characterData.classes,
              race: this.app.processData.characterData.race,
            });
          }
        } else {
          entity = await compendiumOps.findEntityInCompendia(searchEntity, {
            itemType: type,
            itemSubTypes: Array.isArray(subtype) ? subtype : [subtype],
            classes: this.app.processData.characterData.classes,
            race: this.app.processData.characterData.race,
          });
        }

        let name = (entity?.name || input).capitalize();
        let subText = sbcUtils.parseSubtext(input);
        let subBase = subText[0];
        let subInput = subText[1]?.replace(/^(.*?action;)/i, "").trim();
        let subInput2 = subInput?.replace(/(\+?\d+?)$/, "").trim();
        let quantity = subText[1]?.match(/(?:^|,)\s*(\d+)\s*(?:,|$)/);
        if (quantity) quantity = quantity[1];
        else quantity = 1;

        if (entity !== null) {
          if (!subInput) {
            await entity.updateSource({ name: sbcUtils.capitalize(input) });
          } else if (
            subInput &&
            new RegExp(`(^${escapeForRegex(subInput)}|^${escapeForRegex(subInput2)})`, "i").test(entity.name) === false
          ) {
            await entity.updateSource({ name: sbcUtils.capitalize(subBase + (subInput ? ` (${subInput})` : "")) });
          } else if (
            subInput &&
            entity.name !== sbcUtils.capitalize(subInput) &&
            entity.name !== sbcUtils.capitalize(subInput2)
          ) {
            await entity.updateSource({ name: sbcUtils.capitalize(input.trim()) });
          }

          if (/\*$/g.test(input)) {
            await entity.updateSource({ name: entity.name + "*" });
          }

          await sbcUtils.addAssociatedClass(entity, window.SBC.actor.itemTypes.class);
        } else {
          if (typeof pack === "object") {
            switch (pack[0]) {
              case "monster-abilities":
                Object.assign(searchEntity, {
                  name: sbcUtils.translate("parser.entity.placeholderSpecialAbilityName", { input }),
                  type: "misc",
                  desc: sbcUtils.translate("parser.entity.placeholderSpecialAbilityDescription"),
                });
                break;

              case "special-attacks":
                Object.assign(searchEntity, {
                  name: sbcUtils.translate("parser.entity.placeholderSpecialAttackName", { input }),
                  type: "attack",
                  desc: sbcUtils.translate("parser.entity.placeholderSpecialAttackDescription"),
                });
                break;
            }
          }

          if (searchEntity.type === "class-abilities" || searchEntity.type === "classFeat") searchEntity.type = "misc";
          entity = await sbcUtils.generatePlaceholderEntity(searchEntity, line);
        }

        if (quantity > 1) {
          let entityName = entity.name;
          entityName = entityName
            .replace(/(?<![+-]\s*\d*\s*|DC\s*)\b(\d+)\b(?!\s*d\s*\d*\s*[+-])/i, "")
            .replace(/\(\s*\)/, "")
            .trim();

          await entity.updateSource({ name: entityName });
        }

        let [_a, resultItem, foundName] = [null, null, name + (subInput ? ` (${subInput})` : "")];
        // Check if the entity is already in the actor
        // If so, update the name
        // If not, create it
        if (
          (typeof pack === "string" && (pack === "class-abilities" || pack === "monster-abilities")) ||
          (typeof pack === "object" && pack.length > 0)
        ) {
          let foundItem = sbcUtils.checkForDuplicateItem(window.SBC.actor, "feat", "classFeat", foundName, entity);

          if (!foundItem) {
            foundItem = sbcUtils.checkForDuplicateItem(window.SBC.actor, "feat", null, foundName, entity);
          }

          if (foundItem && !pack.includes("special-attacks")) {
            if (
              (subInput &&
                new RegExp(`(^${escapeForRegex(subInput)}|^${escapeForRegex(subInput2)})`, "i").test(foundItem.name) ===
                  false) ||
              (!subInput && new RegExp(`(^${escapeForRegex(subBase)})`, "i").test(foundItem.name) === false)
            ) {
              await foundItem.update({
                name: sbcUtils.capitalize(input),
              });
            } else if (subInput && subInput !== subInput2 && foundItem.name.toLowerCase() === subInput2.toLowerCase()) {
              await foundItem.update({
                name: sbcUtils.capitalize(subInput),
              });
            }
            resultItem = foundItem;
          } else {
            [_a, resultItem] = await createItem(entity);

            if (quantity > 1) {
              for (let i = 1; i < quantity; i++) {
                await createItem(entity);
              }
            }
          }
        } else {
          let foundItem = sbcUtils.checkForDuplicateItem(window.SBC.actor, type, subtype, foundName, entity);

          if (!foundItem) {
            [_a, resultItem] = await createItem(entity);

            if (quantity > 1) {
              for (let i = 1; i < quantity; i++) {
                await createItem(entity);
              }
            }
          } else resultItem = foundItem;
        }

        if (resultItem) {
          let subText = sbcUtils.parseSubtext(input);
          let nameRegex = new RegExp(`(^${escapeForRegex(resultItem.name)})`, "i");
          let nameMatch = nameRegex.test(input);
          if (nameMatch === false) {
            subText[0] = resultItem.name.replace(/^Special (.*?):/, "").trim();
            subText[1] = "";

            await resultItem.update({ name: subText[0] + (subText[1] ? ` (${subText[1]})` : "") });
          }

          if (quantity > 1) {
            let entityName = subText[1];
            entityName = entityName
              .replace(/(?<![+-]\s*\d*\s*|DC\s*)\b(\d+)\b(?!\s*d\s*\d*\s*[+-])[,)]?/i, "")
              .replace(/\(\s*\)/, "")
              .trim();

            subText[1] = entityName;
          }

          let subName = subText[1];
          let subParts = subName?.split(/[;,]\s?/);

          if (subParts) {
            let entityMods = {
              attack: null,
              damageFormula: null,
              damageType: null,
              damageEffect: [],
              saveDC: null,
              saveType: null,
              saveEffect: null,
              templateRange: null,
              templateType: null,
              uses: null,
              usesPeriod: null,
            };

            /** @type {RegExpMatchArray} */
            let regexMatch = null;
            subParts.forEach((part) => {
              // Check for damage
              if ((regexMatch = part.match(abilityDamageRegex())) !== null && entityMods.damageFormula === null) {
                entityMods.damageFormula = regexMatch.groups.formula;
                entityMods.damageType = regexMatch.groups.type?.replace(" damage", "").trim();
                entityMods.damageEffect.push(regexMatch.groups.effect);

                subName = subName.replace(regexMatch[0], "").trim();
              }
              // Check for attack bonus
              else if (/([+|-]\d+)/.test(part)) {
                entityMods.attack = part.match(/([+|-]\d+)/)[0].replace("+", "");
              }
              // Check for saving throw data
              else if ((regexMatch = part.match(abilityDCregex())) !== null && entityMods.saveDC === null) {
                entityMods.saveDC = regexMatch.groups.dc;
                entityMods.saveType = regexMatch.groups.type;
                entityMods.saveEffect = regexMatch.groups.effect;

                subName = subName.replace(regexMatch[0], "").trim();
              }
              // Check for frequency uses
              else if ((regexMatch = part.match(frequencyWithPeriodRegex())) !== null) {
                entityMods.uses = regexMatch.groups.uses;
                entityMods.usesPeriod = regexMatch.groups.period;

                subName = subName.replace(regexMatch[0], "").trim();
              }
              // Check for template range and type
              else if ((regexMatch = part.match(abilityRangeRegex())) !== null) {
                entityMods.templateRange = regexMatch.groups.range;
                entityMods.templateType = regexMatch.groups.type;

                subName = subName.replace(regexMatch[0], "").trim();
              }
              // Check for useable restrictions (like breath weapon)
              else if (part.match(/useable/i)) {
                // Skip these, as they're usually already included.
                entityMods.damageEffect.push(part);
                subName = subName.replace(part, "").trim();
              }
            });

            subName = subName.replace(/^\s*,|,\s*$/g, "").trim();
            subParts = subName?.split(/[;,]\s?/);

            if (subParts.length > 0 && subParts[0] !== "") subParts = subParts.map((word) => sbcUtils.capitalize(word));
            else subParts = null;

            const updatedActions = [];
            if (Object.keys(entityMods).length > 0 && resultItem.actions.size > 0) {
              await sbcUtils.runParallelOps(
                resultItem.actions.contents,
                async (content, _index, updatedActions) => {
                  let action = resultItem.actions.get(content.id);

                  let updates = {};
                  // Effect notes
                  if (entityMods.damageEffect.length > 0) {
                    /** @type {string[]} */
                    let effectNotes = action.notes.effect ?? [];
                    effectNotes = effectNotes.concat(entityMods.damageEffect);

                    updates["notes.effect"] = effectNotes;
                    subName = subName.replace(entityMods.damageEffect.join(""), "").trim();
                  }
                  // Damage formula and type
                  if (entityMods.damageFormula && action.damage.parts.length > 0) {
                    let damageType = action.damage.parts[0].types;
                    const isDamageType = pf1.registry.damageTypes.get(entityMods.damageType) !== undefined;
                    if (!isDamageType) {
                      damageType.values = [];
                      damageType.custom = entityMods.damageType ?? "";
                    } else {
                      damageType.values = [entityMods.damageType];
                      damageType.custom = "";
                    }

                    const comparisonFormula = (
                      await sbcUtils.processFormula(action.damage.parts[0].formula, window.SBC.actor, resultItem)
                    ).formula;
                    if (comparisonFormula !== entityMods.damageFormula) {
                      updates["damage.parts"] = [
                        {
                          formula: entityMods.damageFormula,
                          type: damageType,
                        },
                      ];
                    }
                  }
                  // Attack bonus
                  if (
                    entityMods.attack &&
                    ["spellsave", "save", "heal", "other"].includes(action.actionType) === false
                  ) {
                    updates["attackBonus"] = entityMods.attack;
                  }
                  // If the action has a saving throw, process our modifiers to save data.
                  if (action.save.dc) {
                    // Save DC
                    if (entityMods.saveDC) {
                      const comparisonFormula = (
                        await sbcUtils.processFormula(action.save.dc, window.SBC.actor, resultItem)
                      ).total;
                      if (comparisonFormula !== entityMods.saveDC) {
                        const difference = entityMods.saveDC - comparisonFormula;
                        if (+difference > 0) updates["save.dc"] = action.save.dc + ` + ${+difference}`;
                      }
                    }
                    // Save Type
                    if (entityMods.saveType) {
                      switch (entityMods.saveType) {
                        case "Fortitude":
                          updates["save.type"] = "fort";
                          break;
                        case "Reflex":
                          updates["save.type"] = "ref";
                          break;
                        case "Will":
                          updates["save.type"] = "will";
                          break;
                        default:
                          updates["save.type"] = null;
                          break;
                      }
                    }
                    // Save Description
                    if (entityMods.saveEffect) {
                      updates["save.description"] =
                        (entityMods.saveType ? `${entityMods.saveType} ` : "") + entityMods.saveEffect;
                    }
                  }
                  // Template range
                  if (entityMods.templateRange) {
                    updates["measureTemplate.size"] = entityMods.templateRange;
                  }
                  // Template type
                  if (entityMods.templateType) {
                    switch (entityMods.templateType) {
                      case "cone":
                        updates["measureTemplate.type"] = "cone";
                        break;
                      case "ray":
                        updates["measureTemplate.type"] = "ray";
                        break;
                      default: // Anything else should just be a circle
                        updates["measureTemplate.type"] = "circle";
                        break;
                    }
                  }

                  if (Object.keys(updates).length > 0) {
                    await action.update(updates);
                    updatedActions.push(action);
                  }
                },
                [updatedActions]
              );
            }
            await resultItem.update({ "system.actions": updatedActions });

            if (entityMods.uses) {
              const comparisonFormula = resultItem.system.uses.maxFormula
                ? (await sbcUtils.processFormula(resultItem.system.uses.maxFormula, window.SBC.actor, resultItem)).total
                : 0;
              if (comparisonFormula !== entityMods.uses) {
                const currentUses = resultItem.system.uses.maxFormula;
                const difference = (entityMods.uses - comparisonFormula).toString();
                let usesPeriod = resultItem.system.uses.per ? resultItem.system.uses.per : "charges";
                switch (usesPeriod) {
                  case "round":
                    usesPeriod = "round";
                    break;
                  case "hour":
                    usesPeriod = "hour";
                    break;
                  case "day":
                    usesPeriod = "day";
                    break;
                  case "week":
                    usesPeriod = "week";
                    break;
                  default:
                    break;
                }

                let updates = {
                  "system.uses.maxFormula": currentUses
                    ? +difference > 0
                      ? currentUses + ` + ${+difference}`
                      : currentUses
                    : difference,
                  "system.uses.per": usesPeriod,
                  "system.uses.value": resultItem.system.uses.value + +difference,
                };

                if (resultItem.actions.size === 0) {
                  let actionData = {
                    ...pf1.components.ItemAction.defaultData,
                    name: game.i18n.localize("PF1.Use"),
                  };

                  //let action = await resultItem.createEmbeddedEntity("OwnedItem", actionData);
                  updates["system.actions"] = [actionData];
                }

                await resultItem.update(updates);

                subName = subName
                  .replace(/(\d+)\/(day|week|month|year)/i, "")
                  .trim()
                  .replace(/^,|,$/g, "")
                  .trim();
                subParts = subName?.split(/[;,]\s?/) ?? [];

                if (subParts.length > 0 && subParts[0] !== "")
                  subParts = subParts.map((word) => sbcUtils.capitalize(word));
                else subParts = null;

                await resultItem.update({
                  name: sbcUtils.capitalize(subText[0]) + (subParts?.length > 0 ? ` (${subParts.join(", ")})` : ""),
                });
              }
            }
          }

          if (altName2.match(/^Mythic\) \(/i)) {
            const index = altName2.indexOf("(");
            const part = altName2.substring(index + 1).replace(")", "");
            subParts?.push(part);
          }

          if (quantity > 1) {
            if (subParts) subParts.unshift(quantity);
            else subParts = [quantity];
          }

          logEntities.push({
            name: subText[0],
            kind: resultItem.system.abilityType,
            description: resultItem.system.description.value,
            subParts: subParts,
          });
        }
        // Check for Weapon Finesse and set a flag accordingly
        if (input.toLowerCase() === "weapon finesse" && pack === "feats")
          this.app.processData.flags.hasWeaponFinesse = true;
      }

      // entities were created successfully
      return [true, logEntities];
    } catch (err) {
      this.throwError(err, "parser.entity.error", value, line, { type });
      return [false, []];
    }
  }

  /**
   * Processes the array of items for choices
   * These will be split up so that SBC will find them better.
   * Of note, things that can have choices, among many others:
   * - Bardic Performances
   * - Versatile Performances
   * - Rogue Talents
   * - Hexes
   * - Rage Powers
   * - Ki Powers
   *
   * @param {string[]} choices  The array of items to process
   */
  processForChoices(choices) {
    /** @type {string[]} */
    const options = [
      "bardic performance",
      "versatile performance",
      "rogue talent",
      "hexes",
      "rage power",
      "ki power",
      "discoveries",
      "exploit",
      "deed",
      "investigator talent",
      "infusion",
      "mesmerist trick",
      "ninja trick",
      "investigator talent",
      "magus arcana",
      "revelation",
      "mercies",
      "cruelties",
      "slayer talent",
      "vigilante trick",
    ];

    choices.forEach((choice) => {
      const choiceParts = [];
      let foundOption = null;
      if (
        options.find((o) => {
          if (choice.toLowerCase().startsWith(o)) {
            foundOption = o;
            return true;
          }
          return false;
        })
      ) {
        const subText = sbcUtils.parseSubtext(choice);
        let subTextFrequency = frequencyWithPeriodRegex().exec(subText[0])?.[0];
        if (subTextFrequency) {
          subText[0] = subText[0]
            .replace(subTextFrequency, "")
            .replace(/\s{2,}/, "")
            .trim();
        }

        if (subText[1]) {
          subText[1] = subText[1].replaceAll("[", "(").replaceAll("]", ")");
          const parts = this.splitText(subText[1], true);
          parts.forEach((part) => {
            if (["versatile performance"].includes(foundOption)) choiceParts.push(`${subText[0]} (${part})`);
            else choiceParts.push(`${part}`);
          });
        }

        if (choiceParts.length > 0) {
          if (choiceParts[0].toLowerCase().includes("action")) {
            const choiceText = choiceParts[0].replace(`${subText[0]} `, "").trim();
            if (subTextFrequency) subText[0] = `${subText[0]} ${subTextFrequency}`;
            subText[0] += ` (${choiceText})`;
            choiceParts.shift();
          }
          switch (foundOption) {
            case "hexes":
              subText[0] = subText[0].replace(/hexes/i, "Hex");
              break;
            case "discoveries":
              subText[0] = subText[0].replace(/discoveries/i, "Discovery");
              break;
            case "cruelties":
              subText[0] = subText[0].replace(/cruelties/i, "Cruelty");
              break;
            case "mercies":
              subText[0] = subText[0].replace(/mercies/i, "Mercy");
              break;
            default:
              break;
          }

          choices[choices.indexOf(choice)] = subText[0];

          if (choiceParts.length > 0) choices.push(...choiceParts);
        }
      }
    });
  }
}
