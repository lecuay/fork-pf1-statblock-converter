// eslint-disable-next-line no-unused-vars
import * as sbcTypes from "./sbcTypes.js";

export class sbcError {
  /**
   * @readonly
   * @enum {symbol}
   * @static
   */
  static ERRORLEVELS = Object.freeze({
    /** @type {symbol} 0: Fatal Error */
    FATAL: Symbol("0"),
    /** @type {symbol} 1: Normal Error */
    ERROR: Symbol("1"),
    /** @type {symbol} 2: Warning */
    WARNING: Symbol("2"),
    /** @type {symbol} 3: Info */
    INFO: Symbol("3"),
  });

  /**
   * A translation object for the error levels to their string representation
   *
   * @type {{number: string}}  A translation object for the error levels to their string representation
   * @readonly
   * @static
   */
  static levels = {
    [sbcError.ERRORLEVELS.FATAL]: "FATAL",
    [sbcError.ERRORLEVELS.ERROR]: "ERROR",
    [sbcError.ERRORLEVELS.WARNING]: "WARNING",
    [sbcError.ERRORLEVELS.INFO]: "INFO",
  };

  /**
   * @type {sbcTypes.ErrorData}
   */
  constructor(
    level = sbcError.ERRORLEVELS.FATAL,
    keyword = "Default Error Keyword",
    message = "Default Error Message",
    line = -1,
    value = undefined
  ) {
    this.level = level;
    this.keyword = keyword;
    this.message = message;
    this.line = line;
    this.value = value;
  }

  get errorLabel() {
    //console.log(this, sbcError.levels[this.level]);
    return sbcError.levels[this.level];
  }
}
